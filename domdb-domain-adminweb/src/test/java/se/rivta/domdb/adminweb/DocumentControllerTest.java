package se.rivta.domdb.adminweb;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import se.rivta.domdb.adminweb.model.viewmodel.DocumentViewModel;
import se.rivta.domdb.adminweb.repository.core.DocumentRepository;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.DescriptionDocumentType;
import se.rivta.domdb.domain.core.DomainVersion;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@Import(MyTestConfiguration.class)
@SpringBootTest
public class DocumentControllerTest {

  private static final Random random = new Random();

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  DomainVersionRepository domainVersionRepository;

  @Autowired
  DocumentRepository documentRepository;


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDocument_Get() throws Exception {
    Integer domainVersionId = 501;
    DomainVersion domainVersion = domainVersionRepository.findById(domainVersionId).orElseThrow(
        () -> new AssertionError("Test data is incorrect: domainVersion with id='501' not found")
    );

    mockMvc.perform(get("/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()
            + "/document/create"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("document"))
        .andExpect(model().attributeExists("documentTypes"))
        .andExpect(model().attributeExists("svnPath"))
        .andExpect(view().name("document/create"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDocument_Get_NotExist() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "fel";

    mockMvc.perform(get("/domain/" + domainName + "/" + versionDomainName + "/document/create"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @Transactional
  @WithMockUser(roles = "EDITOR")
  public void testCreateDocument_Post() throws Exception {
    int testDomainVersionId = 514;
    DomainVersion testDomainVersion = domainVersionRepository.findById(testDomainVersionId)
        .orElseThrow(
            () -> new AssertionError(
                "Test data is incorrect: DomainVersion with id='514' not found")
        );
    DescriptionDocumentType[] types = DescriptionDocumentType.values();
    Random random = new Random();
    DescriptionDocumentType randomType = types[random.nextInt(types.length)];
    String filename = "fileName";
    mockMvc.perform(post(
            "/domain/" + testDomainVersion.getDomainName() + "/" + testDomainVersion.getName()
                + "/document/create")
            .with(csrf())
            .param("fileName", filename)
            .param("documentType", randomType.toString()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + testDomainVersion.getDomainName() + "/" + testDomainVersion.getName()));

    Assertions.assertNotNull(
        documentRepository.getByDomainVersionIdAndDocumentType(testDomainVersionId,
            randomType));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDocument_Post_FailVersionDomain() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "trunk1";

    mockMvc.perform(post("/domain/" + domainName + "/" + versionDomainName + "/document/create")
            .with(csrf())
            .param("fileName", "")
            .param("documentType", DescriptionDocumentType.AB.toString())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDocument_Post_AlreadyExists() throws Exception {
    String domainName = "clinicalprocess:activity:actions";
    String versionDomainName = "trunk";

    mockMvc.perform(post("/domain/" + domainName + "/" + versionDomainName + "/document/create")
            .with(csrf())
            .param("fileName", "TKB_clinicalprocess_activity_actions.docx")
            .param("documentType", DescriptionDocumentType.TKB.toString())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testEditDocument_Get() throws Exception {
    int documentId = 501;
    DescriptionDocument document = documentRepository.findById(documentId)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DescriptionDocument id = '501' not found"));

    mockMvc.perform(
            get("/domain/" + document.getDomainVersion().getDomainName() + "/"
                + document.getDomainVersion()
                .getName() + "/document/edit/" + documentId))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("svnPath"))
        .andExpect(model().attributeExists("document"))
        .andExpect(model().attributeExists("documentTypes"))
        .andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          DocumentViewModel documentViewModel = (DocumentViewModel) mav.getModel().get("document");
          Assertions.assertEquals(document.getDocumentType(),
              documentViewModel.getDocumentType(),
              "Document type does not match!");
          Assertions.assertEquals(document.getFileName(),
              documentViewModel.getFileName(),
              "Document file name does not match!");
        })
        .andExpect(view().name("document/edit"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testEditDocument_Get_NotExist() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "trunk";
    int documentId = 1;

    mockMvc.perform(
            get("/domain/" + domainName + "/" + versionDomainName + "/document/edit/" + documentId))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  @Transactional
  public void testEditDocument_Post() throws Exception {
    int domainVersionId = 501;

    DomainVersion domainVersion = domainVersionRepository.findById(domainVersionId).orElseThrow(
        () -> new AssertionError("Test data is incorrect: domainVersion with id='503' not found")
    );

    List<DescriptionDocument> documents = documentRepository.getByDomainVersionId(domainVersionId);
    if (documents.isEmpty()) {
      throw new AssertionError(
          "Test data is incorrect: 'documents' for domainVersionId='501' not found");
    }
    DescriptionDocument document = documents.get(random.nextInt(documents.size()));
    String fileName = "fileName" + random.nextInt(1000);
    DescriptionDocumentType[] types = DescriptionDocumentType.values();
    Random random = new Random();
    DescriptionDocumentType randomType = types[random.nextInt(types.length)];

    mockMvc.perform(
            post("/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()
                + "/document/edit/"
                + document.getId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .with(csrf())
                .param("fileName", fileName)
                .param("documentType", randomType.toString()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()));

    DescriptionDocument newDescriptionDocument = documentRepository.findById(document.getId())
        .orElseThrow(
            () -> new AssertionError(
                "Test data is incorrect: 'document' with id='501' not found")
        );
    Assertions.assertEquals(newDescriptionDocument.getFileName(), fileName);
    Assertions.assertEquals(newDescriptionDocument.getDocumentType(), randomType);
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  @Transactional
  public void testDeleteDocument() throws Exception {
    int documentId = 501;
    DescriptionDocument document = documentRepository.findById(documentId).orElseThrow(() ->
        new AssertionError("Test data is incorrect: document with id='501' not found"));
    String testDomainName = document.getDomainVersion().getDomainName();
    String testDomainVersionName = document.getDomainVersion().getName();

    mockMvc.perform(delete("/document/" + documentId + "/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + testDomainName + "/" + testDomainVersionName));

    Assertions.assertTrue(documentRepository.findById(documentId).isEmpty());
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDeleteDocument_NotExist() throws Exception {
    mockMvc.perform(delete("/document/1/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }
}

