package se.rivta.domdb.adminweb;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import se.rivta.domdb.adminweb.model.viewmodel.DomainVersionViewModel;
import se.rivta.domdb.adminweb.model.viewmodel.DomainViewModel;
import se.rivta.domdb.adminweb.repository.core.DomainRepository;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.DomainVersion;


@ActiveProfiles("test")
@AutoConfigureMockMvc
@Import(MyTestConfiguration.class)
@SpringBootTest
public class DomainControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private DomainRepository domainRepository;

  @Test
  public void testIndex() throws Exception {
    mockMvc.perform(get("/"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domains"));
  }

  @Test
  public void testDomains() throws Exception {
    List<Domain> domainList = domainRepository.findAllByOrderByName();

    mockMvc.perform(get("/domains"))
        .andExpect(status().isOk())
        .andExpect(view().name("domain/list"))
        .andExpect(model().attribute("domainList", new DomainListMatcher(domainList)));
  }

  @Test
  public void testShowDomain() throws Exception {
    Domain domain = domainRepository.findDomainByNameAndFetchVersions(
            "clinicalprocess:activityprescription:actoutcome")
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: Domain 'clinicalprocess:activityprescription:actoutcome' not found"));

    mockMvc.perform(get("/domain/clinicalprocess:activityprescription:actoutcome"))
        .andExpect(status().isOk())
        .andExpect(view().name("domain/domain"))
        .andExpect(model().attributeExists("domain"))

        .andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          DomainViewModel domainViewModel = (DomainViewModel) mav.getModel().get("domain");
          Assertions.assertEquals(domain.getName(), domainViewModel.getDomainName(),
              "Domain name does not match!");
          Assertions.assertEquals(domain.getDescription(), domainViewModel.getDescription(),
              "Description does not match!");
          Assertions.assertEquals(domain.getSwedishLong(), domainViewModel.getSwedishLong(),
              "SwedishLong does not match!");
          Assertions.assertEquals(domain.getSwedishShort(), domainViewModel.getSwedishShort(),
              "SwedishShort does not match!");
          Assertions.assertEquals(domain.getOwner(), domainViewModel.getOwner(),
              "Owner does not match!");
          Assertions.assertEquals(domain.isHidden(), domainViewModel.isHiddenDomain(),
              "Hidden flag does not match!");
          Assertions.assertEquals(domain.getIssueTrackerUrl(), domainViewModel.getIssueTrackerUrl(),
              "IssueTrackerUrl does not match!");
          Assertions.assertEquals(domain.getSourceCodeUrl(), domainViewModel.getSourceCodeUrl(),
              "SourceCodeUrl does not match!");
          Assertions.assertEquals(domain.getInfoPageUrl(), domainViewModel.getInfoPageUrl(),
              "InfoPageUrl does not match!");
          Assertions.assertTrue(
              isCollectionsDataEqual(domain.getVersions(), domainViewModel.getVersions()));
          if (domain.getDomainType() != null && domainViewModel.getDomainType() != null) {
            Assertions.assertEquals(domain.getDomainType().getName(),
                domainViewModel.getDomainType().getName(), "DomainType name does not match!");
          } else {
            Assertions.assertNull(domain.getDomainType(), "DomainType should be null!");
            Assertions.assertNull(domainViewModel.getDomainType(),
                "DomainViewModel DomainType should be null!");
          }
        });
  }

  private boolean isCollectionsDataEqual(Set<DomainVersion> versions,
      List<DomainVersionViewModel> domainVersion) {
    if (versions.size() != domainVersion.size()) {
      return false;
    }

    for (DomainVersion item1 : versions) {
      boolean match = false;
      for (DomainVersionViewModel item2 : domainVersion) {
        if (item1.getName().equals(item2.getDomainVersionName())
            && item1.getId() == (item2.getId())) {
          match = true;
          break;
        }
      }
      if (!match) {
        return false;
      }
    }
    return true;
  }

  @Test
  public void testDomainNotFound() throws Exception {
    mockMvc.perform(get("/domain/test"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDomain_Get() throws Exception {
    mockMvc.perform(get("/domain/create"))
        .andExpect(status().isOk())
        .andExpect(view().name("domain/create"))
        .andExpect(model().attributeExists("domainTypes"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateDomain_Post() throws Exception {
    Random random = new Random();
    String randomName = "testName" + random.nextInt(1000);
    String randomShort = "testShort" + random.nextInt(1000);
    String randomDescription = "testDescription" + random.nextInt(1000);
    String randomSwedishLong = "testSwedishLong" + random.nextInt(1000);
    String randomOwner = "testOwner" + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();
    String randomIssueTrackerUrl = "testIssueTrackerUrl" + random.nextInt(1000);
    String randomSourceCodeUrl = "testSourceCodeUrl" + random.nextInt(1000);
    String randomInfoPageUrl = "testInfoPageUrl" + random.nextInt(1000);
    int domainType = 501;

    mockMvc.perform(post("/domain/create")
            .param("domainName", randomName)
            .param("description", randomDescription)
            .param("swedishLong", randomSwedishLong)
            .param("swedishShort", randomShort)
            .param("owner", randomOwner)
            .param("hiddenDomain", String.valueOf(randomHidden))
            .param("issueTrackerUrl", randomIssueTrackerUrl)
            .param("sourceCodeUrl", randomSourceCodeUrl)
            .param("infoPageUrl", randomInfoPageUrl)
            .param("domainType.id", String.valueOf(domainType))
            .with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + randomName));

    Domain savedDomain = domainRepository.findDomainByNameAndFetchVersions(randomName).orElseThrow(
        () -> new AssertionError("Domain was not created!")
    );
    Assertions.assertEquals(randomName, savedDomain.getName());
    Assertions.assertEquals(randomShort, savedDomain.getSwedishShort());
    Assertions.assertEquals(randomDescription, savedDomain.getDescription());
    Assertions.assertEquals(randomSwedishLong, savedDomain.getSwedishLong());
    Assertions.assertEquals(randomOwner, savedDomain.getOwner());
    Assertions.assertEquals(randomHidden, savedDomain.isHidden());
    Assertions.assertEquals(randomIssueTrackerUrl, savedDomain.getIssueTrackerUrl());
    Assertions.assertEquals(randomSourceCodeUrl, savedDomain.getSourceCodeUrl());
    Assertions.assertEquals(randomInfoPageUrl, savedDomain.getInfoPageUrl());
    Assertions.assertEquals(domainType, savedDomain.getDomainType().getId());
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testEditDomain_Get() throws Exception {
    Domain domain = domainRepository.findDomainByNameAndFetchVersions(
            "clinicalprocess:activityprescription:actoutcome")
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: Domain 'clinicalprocess:activityprescription:actoutcome' not found"));

    mockMvc.perform(get("/domain/" + domain.getName() + "/edit"))
        .andExpect(status().isOk())
        .andExpect(view().name("domain/edit"))
        .andExpect(model().attributeExists("domain"))
        .andExpect(model().attributeExists("domainTypes"))
        .andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          DomainViewModel domainViewModel = (DomainViewModel) mav.getModel().get("domain");
          Assertions.assertEquals(domain.getName(), domainViewModel.getDomainName(),
              "Domain name does not match!");
          Assertions.assertEquals(domain.getDescription(), domainViewModel.getDescription(),
              "Description does not match!");
          Assertions.assertEquals(domain.getSwedishLong(), domainViewModel.getSwedishLong(),
              "SwedishLong does not match!");
          Assertions.assertEquals(domain.getSwedishShort(), domainViewModel.getSwedishShort(),
              "SwedishShort does not match!");
          Assertions.assertEquals(domain.getOwner(), domainViewModel.getOwner(),
              "Owner does not match!");
          Assertions.assertEquals(domain.isHidden(), domainViewModel.isHiddenDomain(),
              "Hidden flag does not match!");
          Assertions.assertEquals(domain.getIssueTrackerUrl(), domainViewModel.getIssueTrackerUrl(),
              "IssueTrackerUrl does not match!");
          Assertions.assertEquals(domain.getSourceCodeUrl(), domainViewModel.getSourceCodeUrl(),
              "SourceCodeUrl does not match!");
          Assertions.assertEquals(domain.getInfoPageUrl(), domainViewModel.getInfoPageUrl(),
              "InfoPageUrl does not match!");

          if (domain.getDomainType() != null && domainViewModel.getDomainType() != null) {
            Assertions.assertEquals(domain.getDomainType().getName(),
                domainViewModel.getDomainType().getName(), "DomainType name does not match!");
          } else {
            Assertions.assertNull(domain.getDomainType(), "DomainType should be null!");
            Assertions.assertNull(domainViewModel.getDomainType(),
                "DomainViewModel DomainType should be null!");
          }
        });
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  @Transactional
  public void testEditDomain_Post() throws Exception {
    Domain domain = domainRepository.findDomainByNameAndFetchVersions(
            "clinicalprocess:activityprescription:actoutcome")
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: Domain 'clinicalprocess:activityprescription:actoutcome' not found"));

    Random random = new Random();
    String randomName = domain.getName() + random.nextInt(1000);
    String randomShort = domain.getSwedishShort() + random.nextInt(1000);
    String randomDescription = domain.getDescription() + random.nextInt(1000);
    String randomSwedishLong = domain.getSwedishLong() + random.nextInt(1000);
    String randomOwner = domain.getOwner() + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();
    String randomIssueTrackerUrl = domain.getIssueTrackerUrl() + random.nextInt(1000);
    String randomSourceCodeUrl = domain.getSourceCodeUrl() + random.nextInt(1000);
    String randomInfoPageUrl = domain.getInfoPageUrl() + random.nextInt(1000);
    int domainType = 501;

    mockMvc.perform(post("/domain/" + domain.getName() + "/edit")
            .param("domainName", randomName)
            .param("description", randomDescription)
            .param("swedishLong", randomSwedishLong)
            .param("swedishShort", randomShort)
            .param("owner", randomOwner)
            .param("hiddenDomain", String.valueOf(randomHidden))
            .param("issueTrackerUrl", randomIssueTrackerUrl)
            .param("sourceCodeUrl", randomSourceCodeUrl)
            .param("infoPageUrl", randomInfoPageUrl)
            .param("domainType.id", String.valueOf(domainType))
            .with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + randomName));

    Domain savedDomain = domainRepository.findDomainByNameAndFetchVersions(randomName).orElseThrow(
        () -> new AssertionError(
            "Domain 'clinicalprocess:activityprescription:actoutcome' not changed!")
    );
    Assertions.assertEquals(randomName, savedDomain.getName());
    Assertions.assertEquals(randomShort, savedDomain.getSwedishShort());
    Assertions.assertEquals(randomDescription, savedDomain.getDescription());
    Assertions.assertEquals(randomSwedishLong, savedDomain.getSwedishLong());
    Assertions.assertEquals(randomOwner, savedDomain.getOwner());
    Assertions.assertEquals(randomHidden, savedDomain.isHidden());
    Assertions.assertEquals(randomIssueTrackerUrl, savedDomain.getIssueTrackerUrl());
    Assertions.assertEquals(randomSourceCodeUrl, savedDomain.getSourceCodeUrl());
    Assertions.assertEquals(randomInfoPageUrl, savedDomain.getInfoPageUrl());
    Assertions.assertEquals(domainType, savedDomain.getDomainType().getId());
  }

  @Test
  @Transactional
  @WithMockUser(roles = "EDITOR")
  public void testDeleteDomain() throws Exception {
    int domainId = 501;

    mockMvc.perform(delete("/domain/" + domainId + "/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domains"));

    Optional<Domain> deletedDomain = domainRepository.findById(domainId);
    Assertions.assertFalse(deletedDomain.isPresent(), "Domain was not deleted");
  }
}

