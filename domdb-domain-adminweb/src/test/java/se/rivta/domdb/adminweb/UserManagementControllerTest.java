package se.rivta.domdb.adminweb;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import se.rivta.domdb.adminweb.model.auth.User;
import se.rivta.domdb.adminweb.repository.auth.UserRepository;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@Import(MyTestConfiguration.class)
@SpringBootTest

public class UserManagementControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private UserRepository userRepository;

  @Test
  @WithMockUser(roles = "ADMIN")
  void givenAdminUser_WhenGetUsers_ThenReturnUsersList() throws Exception {
    mockMvc.perform(get("/users"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("userList"))
        .andExpect(view().name("manageUsers/list"));
  }

  @Test
  @WithMockUser(roles = "ADMIN")
  public void givenAdminUser_WhenEditExistingUser_ThenReturnEditView() throws Exception {
    mockMvc.perform(get("/user/edit/editor"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("user"))
        .andExpect(view().name("manageUsers/edit"));
  }

  @Test
  @WithMockUser(roles = "ADMIN")
  public void givenAdminUser_WhenEditNonExistingUser_ThenRedirectWithError() throws Exception {
    mockMvc.perform(get("/user/edit/nonExistingUser"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "ADMIN")
  @Transactional
  public void givenAdminUser_WhenDeleteUser_ThenUserIsDeleted() throws Exception {
    mockMvc.perform(delete("/user/delete/admin").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/users"));

    Optional<User> user = userRepository.findByUsername("admin");
    Assertions.assertFalse(user.isPresent(), "User was not deleted");
  }

  @Test
  @WithMockUser(roles = "ADMIN")
  public void givenAdminUser_WhenDeleteNonExistingUser_ThenRedirectWithError() throws Exception {
    mockMvc.perform(delete("/user/delete/nonExistingUser").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }
}
