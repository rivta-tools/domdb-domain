package se.rivta.domdb.adminweb;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Optional;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import se.rivta.domdb.adminweb.model.viewmodel.DomainVersionViewModel;
import se.rivta.domdb.adminweb.model.viewmodel.DomainViewModel;
import se.rivta.domdb.adminweb.repository.core.DomainRepository;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.DomainVersion;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@Import(MyTestConfiguration.class)
@SpringBootTest
public class DomainVersionControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private DomainVersionRepository versionRepository;

  @Autowired
  private DomainRepository domainRepository;

  @Test
  public void testDomainVersion() throws Exception {
    DomainVersion domainVersion = versionRepository.findById(501)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DomainVersion id='501' not found"));

    mockMvc.perform(
            get("/domain/" + domainVersion.getDomain().getName() + "/" + domainVersion.getName()))
        .andExpect(status().isOk())
        .andExpect(view().name("domainVersion/domainVersion"))
        .andExpect(model().attributeExists("domainVersion")).
        andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          DomainVersionViewModel domainVersionViewModel = (DomainVersionViewModel) mav.getModel()
              .get("domainVersion");
          Assertions.assertEquals(domainVersion.getName(),
              domainVersionViewModel.getDomainVersionName(),
              "DomainVersion name does not match!");
          Assertions.assertEquals(domainVersion.getSourceControlPath(),
              domainVersionViewModel.getSourceControlPath(),
              "SourceControlPath does not match!");
          Assertions.assertEquals(domainVersion.getDocumentsFolder(),
              domainVersionViewModel.getDocumentsFolder(),
              "DocumentsFolder does not match!");
          Assertions.assertEquals(domainVersion.getInteractionsFolder(),
              domainVersionViewModel.getInteractionsFolder(),
              "InteractionsFolder does not match!");
          Assertions.assertEquals(domainVersion.getZipUrl(), domainVersionViewModel.getZipUrl(),
              "ZipUrl does not match!");
          Assertions.assertEquals(domainVersion.isHidden(),
              domainVersionViewModel.isHiddenVersion(),
              "Hidden flag does not match!");
        });
  }

  @Test
  public void testDomainVersionNotFound() throws Exception {
    mockMvc.perform(get("/domain/clinicalprocess:activity:actions/test"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionCreate_Get() throws Exception {
    mockMvc.perform(get("/domain/clinicalprocess:activity:actions/create"))
        .andExpect(status().isOk())
        .andExpect(view().name("domainVersion/create"))
        .andExpect(model().attributeExists("domainVersion"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionCreate_DomainNotFound_Get() throws Exception {
    mockMvc.perform(get("/domain/nonExistentDomain/create"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionCreate_Post() throws Exception {
    String domainName = "clinicalprocess:activity:actions";

    Random random = new Random();
    String randomVersionName = "domainVersionName" + random.nextInt(1000);
    String sourceControlPath = "sourceControlPath" + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();

    mockMvc.perform(post("/domain/" + domainName + "/create")
            .with(csrf())
            .param("domainVersionName", randomVersionName)
            .param("sourceControlPath", sourceControlPath)
            .param("hiddenVersion", String.valueOf(randomHidden)))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + domainName + "/" + randomVersionName));

    DomainVersion savedDomainVersion = versionRepository.findDomainVersion(domainName,
            randomVersionName)
        .orElseThrow(() -> new AssertionError("DomainVersion was not created!")
        );

    Assertions.assertEquals(randomVersionName, savedDomainVersion.getName());
    Assertions.assertEquals(sourceControlPath, savedDomainVersion.getSourceControlPath());
    Assertions.assertEquals(randomHidden, savedDomainVersion.isHidden());
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionCreate_DomainNotFound_Post() throws Exception {
    mockMvc.perform(post("/domain/nonExistentDomain/create")
            .with(csrf())
            .param("domainVersionName", "")
            .param("sourceControlPath", "")
            .param("hiddenVersion", "true"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_Get() throws Exception {
    DomainVersion domainVersion = versionRepository.findById(501)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DomainVersion id='501' not found"));

    mockMvc.perform(
            get("/domain/" + domainVersion.getDomain().getName() +
                "/" + domainVersion.getName() + "/edit"))
        .andExpect(status().isOk())
        .andExpect(view().name("domainVersion/edit"))
        .andExpect(model().attributeExists("domain"))
        .andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          DomainViewModel domainViewModel = (DomainViewModel) mav.getModel().get("domain");
          Assertions.assertEquals(domainVersion.getDomain().getName(),
              domainViewModel.getDomainName(),
              "Domain name does not match!");
          Assertions.assertEquals(domainVersion.getName(),
              domainViewModel.getDomainVersion().getDomainVersionName(),
              "Domain verdion name does not match!");
        });
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_DomainNotFound_Get() throws Exception {
    mockMvc.perform(get("/domain/nonExistentDomain/nonExistentDomainVersion/edit"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_Post() throws Exception {
    Domain newDomain = domainRepository.findById(502)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: Domain id='502' not found"));


    DomainVersion domainVersion = versionRepository.findById(501)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DomainVersion id='501' not found"));

    Random random = new Random();
    String randomVersionName = "domainVersionName" + random.nextInt(1000);
    String sourceControlPath = "sourceControlPath" + random.nextInt(1000);
    String documentsFolder = "documentsFolder" + random.nextInt(1000);
    String interactionsFolder = "interactionsFolder" + random.nextInt(1000);
    String zipUrl = "zipUrl" + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();

    mockMvc.perform(
            post("/domain/" + domainVersion.getDomain().getName() +
                "/" + domainVersion.getName() + "/edit")
                .with(csrf())
                .param("domainName", newDomain.getName())
                .param("domainVersion.domainVersionName", randomVersionName)
                .param("domainVersion.sourceControlPath", sourceControlPath)
                .param("domainVersion.documentsFolder", documentsFolder)
                .param("domainVersion.interactionsFolder", interactionsFolder)
                .param("domainVersion.zipUrl", zipUrl)
                .param("domainVersion.hiddenVersion", String.valueOf(randomHidden)))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + newDomain.getName() + "/" + randomVersionName));

    DomainVersion savedDomainVersion = versionRepository.findDomainVersion(
            newDomain.getName(),
            randomVersionName)
        .orElseThrow(() -> new AssertionError("DomainVersion was not changed!")
        );

    Assertions.assertEquals(randomVersionName, savedDomainVersion.getName());
    Assertions.assertEquals(sourceControlPath, savedDomainVersion.getSourceControlPath());
    Assertions.assertEquals(interactionsFolder, savedDomainVersion.getInteractionsFolder());
    Assertions.assertEquals(documentsFolder, savedDomainVersion.getDocumentsFolder());
    Assertions.assertEquals(zipUrl, savedDomainVersion.getZipUrl());
    Assertions.assertEquals(randomHidden, savedDomainVersion.isHidden());
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_withDomainChange_Post() throws Exception {
    DomainVersion domainVersion = versionRepository.findById(501)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DomainVersion id='501' not found"));

    Random random = new Random();
    String randomVersionName = "domainVersionName" + random.nextInt(1000);
    String sourceControlPath = "sourceControlPath" + random.nextInt(1000);
    String documentsFolder = "documentsFolder" + random.nextInt(1000);
    String interactionsFolder = "interactionsFolder" + random.nextInt(1000);
    String zipUrl = "zipUrl" + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();

    mockMvc.perform(
            post("/domain/" + domainVersion.getDomain().getName() +
                "/" + domainVersion.getName() + "/edit")
                .with(csrf())
                .param("domainName", domainVersion.getDomainName())
                .param("domainVersion.domainVersionName", randomVersionName)
                .param("domainVersion.sourceControlPath", sourceControlPath)
                .param("domainVersion.documentsFolder", documentsFolder)
                .param("domainVersion.interactionsFolder", interactionsFolder)
                .param("domainVersion.zipUrl", zipUrl)
                .param("domainVersion.hiddenVersion", String.valueOf(randomHidden)))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + domainVersion.getDomainName() + "/" + randomVersionName));

    DomainVersion savedDomainVersion = versionRepository.findDomainVersion(
            domainVersion.getDomainName(),
            randomVersionName)
        .orElseThrow(() -> new AssertionError("DomainVersion was not changed!")
        );

    Assertions.assertEquals(randomVersionName, savedDomainVersion.getName());
    Assertions.assertEquals(sourceControlPath, savedDomainVersion.getSourceControlPath());
    Assertions.assertEquals(interactionsFolder, savedDomainVersion.getInteractionsFolder());
    Assertions.assertEquals(documentsFolder, savedDomainVersion.getDocumentsFolder());
    Assertions.assertEquals(zipUrl, savedDomainVersion.getZipUrl());
    Assertions.assertEquals(randomHidden, savedDomainVersion.isHidden());
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_DomainNotFound_Post() throws Exception {
    mockMvc.perform(post("/domain/nonExistentDomain/nonExistentDomainVersion/edit")
            .with(csrf())
            .param("domainVersionName", "")
            .param("sourceControlPath", "")
            .param("hiddenVersion", "true"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDomainVersionEdit_NewDomainNotFound_Post() throws Exception {
    DomainVersion domainVersion = versionRepository.findById(501)
            .orElseThrow(() -> new AssertionError(
                "Test data is incorrect: DomainVersion id='501' not found"));

    Random random = new Random();
    String randomVersionName = "domainVersionName" + random.nextInt(1000);
    String sourceControlPath = "sourceControlPath" + random.nextInt(1000);
    String documentsFolder = "documentsFolder" + random.nextInt(1000);
    String interactionsFolder = "interactionsFolder" + random.nextInt(1000);
    String zipUrl = "zipUrl" + random.nextInt(1000);
    boolean randomHidden = random.nextBoolean();

    mockMvc.perform(
            post("/domain/" + domainVersion.getDomain().getName() +
                "/" + domainVersion.getName() + "/edit")
                .with(csrf())
                .param("domainName", "nonExistentName")
                .param("domainVersion.domainVersionName", randomVersionName)
                .param("domainVersion.sourceControlPath", sourceControlPath)
                .param("domainVersion.documentsFolder", documentsFolder)
                .param("domainVersion.interactionsFolder", interactionsFolder)
                .param("domainVersion.zipUrl", zipUrl)
                .param("domainVersion.hiddenVersion", String.valueOf(randomHidden)))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));

  }

  @Test
  @Transactional
  @WithMockUser(roles = "EDITOR")
  public void testDeleteDomainVersion() throws Exception {
    int domainVersionId = 501;

    DomainVersion domainVersion = versionRepository.findById(domainVersionId)
        .orElseThrow(() -> new AssertionError(
            "Test data is incorrect: DomainVersion id='501' not found"));

    mockMvc.perform(delete("/domainVersion/" + domainVersionId + "/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + domainVersion.getDomainName()));

    Optional<DomainVersion> deletedDomainOpt = versionRepository.findById(domainVersionId);
    Assertions.assertFalse(deletedDomainOpt.isPresent(), "DomainVersion was not deleted");
  }

  @Test
  @Transactional
  @WithMockUser(roles = "EDITOR")
  public void testDeleteDomainVersion_notFound() throws Exception {
    mockMvc.perform(delete("/domainVersion/256/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

}
