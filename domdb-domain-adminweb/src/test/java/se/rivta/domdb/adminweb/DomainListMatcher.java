package se.rivta.domdb.adminweb;

import java.util.List;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import se.rivta.domdb.domain.core.Domain;

public class DomainListMatcher extends TypeSafeMatcher<List<Domain>> {

  private final List<Domain> expectedList;

  public DomainListMatcher(List<Domain> expectedList) {
    this.expectedList = expectedList;
  }

  @Override
  protected boolean matchesSafely(List<Domain> actualList) {
    if (expectedList.size() != actualList.size()) {
      return false;
    }

    for (int i = 0; i < expectedList.size(); i++) {
      Domain expected = expectedList.get(i);
      Domain actual = actualList.get(i);

      if (!expected.getName().equals(actual.getName())) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void describeTo(Description description) {
    description.appendText("lists of domains to be deeply equal");
  }
}
