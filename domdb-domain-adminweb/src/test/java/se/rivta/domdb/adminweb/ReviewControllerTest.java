package se.rivta.domdb.adminweb;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import se.rivta.domdb.adminweb.model.viewmodel.ReviewViewModel;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewOutcomeRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewProtocolRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewRepository;
import se.rivta.domdb.domain.core.DomainVersion;
import se.rivta.domdb.domain.core.Review;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@Import(MyTestConfiguration.class)
@SpringBootTest
public class ReviewControllerTest {

  private static final Random random = new Random();

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  ReviewProtocolRepository reviewProtocolRepository;

  @Autowired
  ReviewOutcomeRepository reviewOutcomeRepository;

  @Autowired
  ReviewRepository reviewRepository;

  @Autowired
  DomainVersionRepository domainVersionRepository;

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateReview_Get() throws Exception {
    Integer domainVersionId = 501;
    DomainVersion domainVersion = domainVersionRepository.findById(domainVersionId).orElseThrow(
        () -> new AssertionError("Test data is incorrect: domainVersion with id='501' not found")
    );

    mockMvc.perform(get("/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()
            + "/review/create"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("review"))
        .andExpect(model().attributeExists("reviewProtocols"))
        .andExpect(model().attributeExists("reviewOutcomes"))
        .andExpect(view().name("review/create"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateReview_Get_NotExist() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "fel";

    mockMvc.perform(get("/domain/" + domainName + "/" + versionDomainName + "/review/create"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @Transactional
  @WithMockUser(roles = "EDITOR")
  public void testCreateReview_Post() throws Exception {
    int testDomainVersionId = 514;

    DomainVersion testDomainVersion = domainVersionRepository.findById(testDomainVersionId)
        .orElseThrow(
            () -> new AssertionError(
                "Test data is incorrect: DomainVersion with id='514' not found")
        );

    List<String> reviewProtocolNames = reviewProtocolRepository.findAllReviewNames();
    if (reviewProtocolNames.isEmpty()) {
      throw new AssertionError("Test data is incorrect: 'reviewProtocols' not found");
    }
    String randomProtocol = reviewProtocolNames.get(random.nextInt(reviewProtocolNames.size()));

    List<String> reviewOutcomeNames = reviewOutcomeRepository.findAllReviewOutcomeNames();
    if (reviewOutcomeNames.isEmpty()) {
      throw new AssertionError("Test data is incorrect: 'reviewOutcomes' not found");
    }
    String randomOutcome = reviewOutcomeNames.get(random.nextInt(reviewOutcomeNames.size()));

    mockMvc.perform(post(
            "/domain/" + testDomainVersion.getDomainName() + "/" + testDomainVersion.getName()
                + "/review/create")
            .with(csrf())
            .param("reviewProtocol", randomProtocol)
            .param("reviewOutcome", randomOutcome)
            .param("reportUrl", "someValue"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + testDomainVersion.getDomainName() + "/" + testDomainVersion.getName()));

    Assertions.assertNotNull(
        reviewRepository.getByDomainVersionIdAndReviewProtocolName(testDomainVersionId,
            randomProtocol));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateReview_Post_FailVersionDomain() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "trunk1";

    mockMvc.perform(post("/domain/" + domainName + "/" + versionDomainName + "/review/create")
            .with(csrf())
            .param("reviewProtocol", "")
            .param("reviewOutcome", "")
            .param("reportUrl", "someValue")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testCreateReview_Post_AlreadyExists() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "trunk";

    mockMvc.perform(post("/domain/" + domainName + "/" + versionDomainName + "/review/create")
            .with(csrf())
            .param("reviewProtocol", "Arkitektur & Regelverk: Säkerhet")
            .param("reviewOutcome", "Godkänd")
            .param("reportUrl", "someValue")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testEditReview_Get() throws Exception {
    int reviewId = 501;
    Review review = reviewRepository.findById(reviewId).orElseThrow(() -> new AssertionError(
        "Test data is incorrect: Review id = '501' not found"));

    mockMvc.perform(
            get("/domain/" + review.getDomainVersion().getDomainName() + "/" + review.getDomainVersion()
                .getName() + "/review/edit/" + reviewId))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("review"))
        .andExpect(model().attributeExists("reviewProtocols"))
        .andExpect(model().attributeExists("reviewOutcomes"))
        .andExpect(result -> {
          ModelAndView mav = result.getModelAndView();
          ReviewViewModel reviewViewModel = (ReviewViewModel) mav.getModel().get("review");
          Assertions.assertEquals(review.getReviewProtocol().getName(),
              reviewViewModel.getReviewProtocol(),
              "Review protocol does not match!");
          Assertions.assertEquals(review.getReviewOutcome().getName(),
              reviewViewModel.getReviewOutcome(),
              "Review outcome does not match!");
          Assertions.assertEquals(review.getReportUrl(), reviewViewModel.getReportUrl(),
              "Review report url does not match!");

        })
        .andExpect(view().name("review/edit"));
  }

  @Test
  @WithMockUser(roles = "EDITOR")
  public void testEditReview_Get_NotExist() throws Exception {
    String domainName = "clinicalprocess:activityprescription:actoutcome";
    String versionDomainName = "trunk";
    int reviewId = 1;

    mockMvc.perform(
            get("/domain/" + domainName + "/" + versionDomainName + "/review/edit/" + reviewId))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  @Transactional
  public void testEditReview_Post() throws Exception {
    int domainVersionId = 501;

    DomainVersion domainVersion = domainVersionRepository.findById(domainVersionId).orElseThrow(
        () -> new AssertionError("Test data is incorrect: domainVersion with id='503' not found")
    );
    List<Review> reviews = reviewRepository.getByDomainVersionId(domainVersionId);
    if (reviews.isEmpty()) {
      throw new AssertionError(
          "Test data is incorrect: 'reviewProtocols' for domainVersionId='501' not found");
    }
    Review review = reviews.get(random.nextInt(reviews.size()));

    List<String> reviewOutcomeNames = reviewOutcomeRepository.findAllReviewOutcomeNames();
    if (reviewOutcomeNames.isEmpty()) {
      throw new AssertionError("Test data is incorrect: 'reviewOutcomes' not found");
    }
    String newRandomOutcome = reviewOutcomeNames.get(random.nextInt(reviewOutcomeNames.size()));

    String randomUrl = "randomUrl" + random.nextInt(1000);

    mockMvc.perform(
            post("/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()
                + "/review/edit/"
                + review.getId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .with(csrf())
                .param("reviewProtocol", review.getReviewProtocol().getName())
                .param("reviewOutcome", newRandomOutcome)
                .param("reportUrl", randomUrl))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl(
            "/domain/" + domainVersion.getDomainName() + "/" + domainVersion.getName()));

    Review updatedReview = reviewRepository.findById(review.getId()).orElseThrow(
        () -> new AssertionError(
            "Test data is incorrect: 'review' with id='501' not found")
    );
    Assertions.assertEquals(newRandomOutcome, updatedReview.getReviewOutcome().getName());
    Assertions.assertEquals(randomUrl, updatedReview.getReportUrl());
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  @Transactional
  public void testDeleteReview() throws Exception {
    int reviewId = 501;
    Review review = reviewRepository.findById(reviewId).orElseThrow(() ->
        new AssertionError("Test data is incorrect: review with id='501' not found"));
    String testDomainName = review.getDomainVersion().getDomainName();
    String testDomainVersionName = review.getDomainVersion().getName();

    mockMvc.perform(delete("/review/" + reviewId + "/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/domain/" + testDomainName + "/" + testDomainVersionName));

    Assertions.assertTrue(reviewRepository.findById(reviewId).isEmpty());
  }


  @Test
  @WithMockUser(roles = "EDITOR")
  public void testDeleteReview_NotExist() throws Exception {
    mockMvc.perform(delete("/review/1/delete").with(csrf()))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrlPattern("/error*"));
  }

}
