package se.rivta.domdb.adminweb;

import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import static org.mockito.Mockito.*;

@TestConfiguration
public class MyTestConfiguration {

  @Bean
  public BuildProperties buildProperties() {
    return mock(BuildProperties.class);
  }
}
