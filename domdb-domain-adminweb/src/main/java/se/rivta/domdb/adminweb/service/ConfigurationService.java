package se.rivta.domdb.adminweb.service;

import jakarta.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

@Service
public class ConfigurationService implements ServletContextAware {

  ServletContext context;

  @Autowired
  BuildProperties buildProperties;

  @Override
  public void setServletContext(ServletContext servletContext) {
    this.context = servletContext;
  }

  public String getAppVersion() {
    return buildProperties.getVersion();
  }
}
