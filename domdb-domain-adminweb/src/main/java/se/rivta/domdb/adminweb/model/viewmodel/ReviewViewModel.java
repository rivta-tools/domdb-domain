package se.rivta.domdb.adminweb.model.viewmodel;

import se.rivta.domdb.domain.core.Review;


public class ReviewViewModel implements Comparable<ReviewViewModel> {
  private Integer id;
  private String reviewProtocol;
  private String reportUrl;
  private String reviewOutcome;


  public ReviewViewModel() {

  }
  public ReviewViewModel(Review review){
    this.id = review.getId();
    this.reviewProtocol = review.getReviewProtocol().getName();
    this.reviewOutcome = review.getReviewOutcome().getName();
    this.reportUrl = review.getReportUrl();
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public String getReviewProtocol() {
    return reviewProtocol;
  }

  public void setReviewProtocol(String reviewProtocol) {
    this.reviewProtocol = reviewProtocol;
  }

  public String getReportUrl() {
    return reportUrl;
  }

  public void setReportUrl(String reportUrl) {
    this.reportUrl = reportUrl;
  }

  public String getReviewOutcome() {
    return reviewOutcome;
  }

  public void setReviewOutcome(String reviewOutcome) {
    this.reviewOutcome = reviewOutcome;
  }

  @Override
  public int compareTo(ReviewViewModel o) {
    return this.id.compareTo(o.id);
  }
}
