package se.rivta.domdb.adminweb.repository.core;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.ReviewOutcome;

@Repository
public interface ReviewOutcomeRepository extends JpaRepository<ReviewOutcome, Integer> {

  @Query("SELECT name FROM ReviewOutcome")
  List<String> findAllReviewOutcomeNames();

  @Query("SELECT r FROM ReviewOutcome r WHERE r.name = :name")
  ReviewOutcome findReviewOutcomeByName(@Param("name") String name);
}