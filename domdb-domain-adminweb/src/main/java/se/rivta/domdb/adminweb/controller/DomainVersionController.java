package se.rivta.domdb.adminweb.controller;


import groovy.lang.Closure;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import se.rivta.domdb.adminweb.model.ftp.FtpFileListPayload;
import se.rivta.domdb.adminweb.model.viewmodel.DomainVersionViewModel;
import se.rivta.domdb.adminweb.model.viewmodel.DomainViewModel;
import se.rivta.domdb.adminweb.repository.core.DocumentRepository;
import se.rivta.domdb.adminweb.repository.core.DomainRepository;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.adminweb.repository.core.InteractionDescriptionRepository;
import se.rivta.domdb.adminweb.repository.core.InteractionRepository;
import se.rivta.domdb.adminweb.service.FtpService;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.DomainVersion;
import se.rivta.domdb.domain.core.InteractionDescription;
import se.rivta.domdb.domain.integration.BitbucketReaderFactory;
import se.rivta.domdb.domain.integration.DocumentUpdater;
import se.rivta.domdb.domain.integration.InteractionUpdater;
import se.rivta.domdb.domain.integration.SourceDocumentReader;
import se.rivta.domdb.domain.integration.SourceInteractionReader;

@SuppressWarnings("unused")
@Controller
public class DomainVersionController {

  private static final Logger log = LoggerFactory.getLogger(DomainVersionController.class);

  @Autowired
  MessageSource messageSource;

  @Autowired
  DomainRepository domainRepository;

  @Autowired
  DomainVersionRepository domainVersionRepository;

  @Autowired
  DocumentRepository documentRepository;

  @Autowired
  InteractionDescriptionRepository interactionDescriptionRepository;

  @Autowired
  InteractionRepository interactionRepository;

  @Autowired
  FtpService ftpService;

  // CONVENIENT CONSTANTS.
  private static final String REDIRECT_ERROR_ERROR_MESSAGE = "redirect:/error?errorMessage=";
  private static final String REDIRECT_DOMAIN = "redirect:/domain/";
  private static final String ERROR_CANNOT_FIND_DOMAIN_WITH_NAME = "error.cannot.find.domain.with.name";

  ////////////////////
  // PAGES AND ENDPOINTS
  // DOMAIN_VERSION VIEW
  ////////////////////

  // GET DomainVersion VIEW - Page Render.
  @GetMapping(value = "/domain/{domainName}/{versionName}")
  public String domainVersion(Model model,
      @PathVariable("domainName") String domainName,
      @PathVariable("versionName") String versionName) {
    log.info("DomainVersion View - GET page render: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(domainName, versionName).orElse(null);
    if (domainVersion == null) {
      log.info("DomainVersion View Get: User encountered a problem: Desired DomainVersion could not be found.");
      String errorMessage =
          messageSource.getMessage("error.cannot.find.domain.version.with.domain.and.name",
              new String[]{domainName, versionName}, Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
    }

    DomainVersionViewModel versionViewModel = new DomainVersionViewModel(domainVersion);
    versionViewModel.setDescriptionDocuments(domainVersion.getDescriptionDocuments());
    versionViewModel.setInteractionDescriptions(domainVersion.getInteractionDescriptions());
    versionViewModel.setReviews(domainVersion.getReviews());
    model.addAttribute("domainVersion", versionViewModel);

    log.debug("DomainVersion View - GET page render: Finished preprocessing.");
    return "domainVersion/domainVersion";
  }

  ////////////////////
  // DOMAIN VERSION CREATE
  ////////////////////

  // GET DomainVersion CREATE - Page Render.
  @GetMapping(value = "/domain/{domainName}/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String create(Model model, @PathVariable("domainName") String domainName) {
    log.info("DomainVersion Create - GET page render: Incoming Request.");

    if (!domainRepository.existsDomainByName(domainName)) {
      log.info("DomainVersion Create Get: User encountered a problem: Desired Domain could not be found.");
      String message =
          messageSource.getMessage(ERROR_CANNOT_FIND_DOMAIN_WITH_NAME, new String[]{domainName},
              Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + message;
    }

    model.addAttribute("domainVersion", new DomainVersionViewModel());

    log.debug("DomainVersion Create - GET page render: Finished preprocessing.");
    return "domainVersion/create";
  }

  // POST DomainVersion CREATE - Process and ReRoute to view page.
  @Transactional
  @PostMapping(value = "/domain/{domainName}/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String create(@PathVariable("domainName") String domainName,
      @ModelAttribute DomainVersionViewModel domainVersionViewModel) {
    log.info("DomainVersion Create - POST endpoint: Incoming Request.");

    Domain domain = domainRepository.findDomainByNameAndFetchVersions(domainName).orElse(null);
    if (domain == null) {
      log.info("DomainVersion Create Post: User encountered a problem: Desired Domain could not be found.");
      String message =
          messageSource.getMessage(ERROR_CANNOT_FIND_DOMAIN_WITH_NAME, new String[]{domainName},
              Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + message;
    }

    DomainVersion domainVersion = new DomainVersion();
    domainVersion.setDomain(domain);
    domainVersion.setName(domainVersionViewModel.getDomainVersionName());
    domainVersion.setSourceControlPath(domainVersionViewModel.getSourceControlPath());
    domainVersion.setHidden(domainVersionViewModel.isHiddenVersion());
    domainVersion.setDocumentsFolder("docs"); // default value for new item.
    domainVersion.setInteractionsFolder("schemas/interactions"); // default value for new item.

    log.debug("DomainVersion Creation POST: Saving DomainVersion");
    domainVersionRepository.save(domainVersion);

    log.info("DomainVersion Create - DomainVersion with id '{}' and name '{}' was created and saved to storage.",
        domainVersion.getId(), domainVersion.getName());

    return REDIRECT_DOMAIN + domainName + "/" + domainVersion.getName();
  }

  ////////////////////
  // DOMAIN VERSION EDIT
  ////////////////////

  // GET DomainVersion EDIT - Page Render.
  @GetMapping(value = "/domain/{domainName}/{versionName}/edit")
  @PreAuthorize("hasRole('EDITOR')")
  public String edit(Model model,
      @PathVariable("domainName") String domainName,
      @PathVariable("versionName") String versionName) throws IOException {
    log.info("DomainVersion Edit - GET page render: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(domainName, versionName).orElse(null);

    if (domainVersion == null) {
      log.info("DomainVersion Edit Get: User encountered a problem: Desired DomainVersion could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.version",
              new String[]{domainName, versionName},
              Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + message;
    }

    DomainViewModel domainModel = new DomainViewModel(domainVersion.getDomain());
    domainModel.setDomainVersion(new DomainVersionViewModel(domainVersion));
    model.addAttribute("domain", domainModel);

    // Retrieve any files associated with the DomVer.
    log.debug("DomainVersion Edit - Attempting to scan for FTP files associated with relevant DomainVersion.");
    FtpFileListPayload payload = ftpService.getListOfDirectoryContent(domainName, versionName);
    model.addAttribute("ftp_fileScanResults", payload);

    log.debug("DomainVersion Edit - GET page render: Finished preprocessing.");
    return "domainVersion/edit";
  }

  // POST DomainVersion EDIT - Process and ReRoute to view page.
  @Transactional
  @PreAuthorize("hasRole('EDITOR')")
  @PostMapping(value = "/domain/{domainName}/{versionName}/edit")
  public String edit(Model model,
      @PathVariable("domainName") String oldDomainName,
      @PathVariable("versionName") String versionName,
      @ModelAttribute DomainViewModel domainModel) {
    log.info("DomainVersion Edit - POST endpoint: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(oldDomainName, versionName).orElse(null);
    if (domainVersion == null) {
      log.info("DomainVersion Edit Post: User encountered a problem: Desired DomainVersion could not be found.");
      String errorMessage =
          messageSource.getMessage("error.cannot.find.domain.version.with.domain.and.name",
              new String[]{oldDomainName, versionName}, Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
    }

    String newDomainName = domainModel.getDomainName();
    if (!oldDomainName.equals(newDomainName)) {
      Domain newDomain = domainRepository.findDomainByName(domainModel.getDomainName())
          .orElse(null);

      if (newDomain == null) {
        log.info("DomainVersion Edit Post: User encountered a problem: Desired Domain could not be found.");
        String errorMessage =
            messageSource.getMessage(ERROR_CANNOT_FIND_DOMAIN_WITH_NAME,
                new String[]{newDomainName},
                Locale.getDefault());
        return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
      }
      domainVersion.setDomain(newDomain);
    }

    DomainVersionViewModel domainVersionModel = domainModel.getDomainVersion();
    domainVersion.setName(domainVersionModel.getDomainVersionName());
    domainVersion.setHidden(domainVersionModel.isHiddenVersion());
    domainVersion.setSourceControlPath(domainVersionModel.getSourceControlPath());
    domainVersion.setDocumentsFolder(domainVersionModel.getDocumentsFolder());
    domainVersion.setInteractionsFolder(domainVersionModel.getInteractionsFolder());
    domainVersion.setZipUrl(domainVersionModel.getZipUrl());

    log.debug("DomainVersion Edit POST: Saving DomainVersion");
    domainVersionRepository.save(domainVersion);

    log.info("DomainVersion with id '{}' and name '{}' was edited and saved to storage.",
        domainVersion.getId(), domainVersion.getName());

    return REDIRECT_DOMAIN + newDomainName + "/" + domainVersion.getName();
  }

  ////////////////////
  // DOMAIN VERSION DELETE
  ////////////////////

  // DELETE DomainVersion DELETE - Process and ReRoute to Parent Domain view page.
  @DeleteMapping("/domainVersion/{versionId}/delete")
  @PreAuthorize("hasRole('EDITOR')")
  public String deleteDomainVersion(@PathVariable("versionId") Integer versionId) {
    log.info("DomainVersion Deletion - DELETE endpoint: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.findById(versionId).orElse(null);
    if (domainVersion == null) {
      log.info("DomainVersion Delete: User encountered a problem: Desired DomainVersion could not be found.");
      String errorMessage =
          messageSource.getMessage("error.cannot.find.domain.version.with.id",
              new Integer[]{versionId},
              Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
    }

    log.debug("DomainVersion Delete: Attempting deletion.");
    domainVersionRepository.deleteById(versionId);

    log.info("DomainVersion with id '{}' and name '{}' was deleted and removed from storage.",
        domainVersion.getId(), domainVersion.getName());
    return REDIRECT_DOMAIN + domainVersion.getDomain().getName();
  }

  ////////////////////
  // DOMAIN VERSION UPDATE-FROM-SOURCE
  ////////////////////

  // GET DomainVersion READ_SOURCE - Trigger Processing and Reroute to View Page.
  @Transactional
  @GetMapping(value = "/domain/{domainName}/{versionName}/refreshFromSource")
  @PreAuthorize("hasRole('EDITOR')")
  public String refreshFromSource(
      @PathVariable("domainName") String domainName,
      @PathVariable("versionName") String versionName) {
    log.info("DomainVersion Refresh-from-Source - GET trigger endpoint: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.findDomainVersionWithDetails(
        domainName, versionName).orElse(null);
    if (domainVersion == null) {
      log.info("DomainVersion Refresh-from-Source: User encountered a problem: Desired DomainVersion could not be found.");
      String errorMessage =
          messageSource.getMessage("error.cannot.find.domain.version.with.name",
              new String[]{versionName}, Locale.getDefault());
      return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
    }

    log.debug("DomainVersion Refresh-from-Source - Processing...");
    try {
      // This needs to be here and not as a try-with-resource, because that fails for some reason.
      StandardFileSystemManager apacheVfs = new StandardFileSystemManager();
      apacheVfs.init();

      log.debug("DomainVersion Refresh-from-Source - Connecting Bitbucket Reader Factory...");
      BitbucketReaderFactory readerFactory = new BitbucketReaderFactory(domainVersion, apacheVfs);

      log.debug("DomainVersion Refresh-from-Source - Interactions folder set to {}...", domainVersion.getInteractionsFolder());
      if (domainVersion.getInteractionsFolder() != null &&
          !domainVersion.getInteractionsFolder().isBlank()) {
        log.debug("Processing interactions...");
        updateInteractions(domainVersion, readerFactory);
      }

      log.debug("DomainVersion Refresh-from-Source - Documents folder set to {}...", domainVersion.getDocumentsFolder());
      if (domainVersion.getDocumentsFolder() != null
          && !domainVersion.getDocumentsFolder().isBlank()) {
        log.debug("Processing documents...");
        updateDocuments(domainVersion, readerFactory);
      }
    }
    catch (Exception exc) {

      String errorMessage = "Encountered an Exception of Type: " + exc.getClass().getSimpleName() +
          ", with Internal Error Message: " + exc.getMessage();

      log.warn("Exception occurred during Refresh-from-Source. Can be user error.\n" +
          "ERROR: {}.\n" +
          "Stack Trace below:\n{}",
          errorMessage, Arrays.toString(exc.getStackTrace()));

      return REDIRECT_ERROR_ERROR_MESSAGE + errorMessage;
    }

    log.info("DomainVersion Refresh-from-Source - DomainVersion with id '{}' and name '{}' has been updated from source.",
        domainVersion.getId(), domainVersion.getName());

    return REDIRECT_DOMAIN + domainName + "/" + versionName;
  }


  ////////////////////
  // PRIVATE HELPERS AND PROCESSORS
  ////////////////////

  private void updateDocuments(DomainVersion domainVersion, BitbucketReaderFactory readerFactory) {
    SourceDocumentReader documentReader = readerFactory.createDocumentReader();
    DocumentUpdater documentUpdater = new DocumentUpdater(
        new Closure<DescriptionDocument>(this) {
          private static final long serialVersionUID = 1L;
          DescriptionDocument doCall(DescriptionDocument entity) {
            documentRepository.delete(entity);
            return entity;
          }
        });
    documentUpdater.updateDocuments(domainVersion, documentReader);
    log.debug("DomainVersion Refresh-from-Source - Attempting Save of Documents...");
    documentRepository.saveAll(domainVersion.getDescriptionDocuments());
  }

  private void updateInteractions(DomainVersion domainVersion,
      BitbucketReaderFactory readerFactory) {
    SourceInteractionReader interactionReader = readerFactory.createInteractionReader();

    InteractionUpdater interactionUpdater = new InteractionUpdater(
        new Closure<InteractionDescription>(this) {
          private static final long serialVersionUID = 1L;
          InteractionDescription doCall(InteractionDescription entity) {
            interactionDescriptionRepository.delete(entity);
            return entity;
          }
        });
    interactionUpdater.updateInteractions(domainVersion, interactionReader, true);

    for (InteractionDescription description : domainVersion.getInteractionDescriptions()) {
      log.debug("DomainVersion Refresh-from-Source - Attempting Save of Interaction...");
      interactionRepository.save(description.getInteraction());
      interactionDescriptionRepository.save(description);
    }
    domainVersionRepository.save(domainVersion);
  }
}
