package se.rivta.domdb.adminweb.util;

public class RoleConstants {
  public static final String ROLE_EDITOR = "ROLE_EDITOR";
  public static final String ROLE_USER = "ROLE_USER";
  public static final String ROLE_ADMIN = "ROLE_ADMIN";

  private RoleConstants() {
    throw new IllegalStateException("Utility class");
  }
}
