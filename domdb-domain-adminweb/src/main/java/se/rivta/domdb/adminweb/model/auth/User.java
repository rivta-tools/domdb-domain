package se.rivta.domdb.adminweb.model.auth;

import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "user")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long version;

  @Column(nullable = false, unique = true)
  private String username;

  @Column(nullable = false)
  private String password;

  private boolean enabled = true;

  @Column(name = "account_expired")
  private boolean accountExpired;
  @Column(name = "account_locked")
  private boolean accountLocked;
  @Column(name = "password_expired")
  private boolean passwordExpired;

  ////
  // EQUAL AND HASH
  ////

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    User otherUser = (User) other;
    return enabled == otherUser.enabled
        && accountExpired == otherUser.accountExpired
        && accountLocked == otherUser.accountLocked
        && passwordExpired == otherUser.passwordExpired
        && id.equals(otherUser.id)
        && username.equals(otherUser.username)
        && password.equals(otherUser.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, username, password, enabled, accountExpired, accountLocked,
        passwordExpired);
  }

  ////
  // GETTERS AND SETTERS
  ////

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() { return this.version;}

  public void setVersion(Long version) { this.version = version; }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isAccountExpired() {
    return accountExpired;
  }

  public void setAccountExpired(boolean accountExpired) {
    this.accountExpired = accountExpired;
  }

  public boolean isAccountLocked() {
    return accountLocked;
  }

  public void setAccountLocked(boolean accountLocked) {
    this.accountLocked = accountLocked;
  }

  public boolean isPasswordExpired() {
    return passwordExpired;
  }

  public void setPasswordExpired(boolean passwordExpired) {
    this.passwordExpired = passwordExpired;
  }
}
