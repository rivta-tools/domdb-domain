package se.rivta.domdb.adminweb.model.auth;

import jakarta.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "oauthid")
public class OAuthID implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  private String provider;

  @Column(nullable = false, unique = true)
  private String accessToken;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public OAuthID() {}

  public OAuthID(String provider, String accessToken, User user) {
    this.provider = provider;
    this.accessToken = accessToken;
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

}

