package se.rivta.domdb.adminweb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController {

    private static final Logger log = LoggerFactory.getLogger(ErrorController.class);

    @GetMapping("/error-page")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView errorPage(@RequestParam("message") String errorMessage) {
        log.debug("Error Page - GET page render: Incoming Request.");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error-page");
        modelAndView.addObject("errorMessage", errorMessage);
        return modelAndView;
    }
}
