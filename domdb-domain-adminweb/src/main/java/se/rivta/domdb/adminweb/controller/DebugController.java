package se.rivta.domdb.adminweb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import se.rivta.domdb.adminweb.model.ftp.FtpFileListPayload;
import se.rivta.domdb.adminweb.service.FtpService;

import java.io.IOException;

@Controller
public class DebugController {

  private static final Logger log = LoggerFactory.getLogger(DebugController.class);

  @Autowired
  FtpService ftpService;

  /**
   * Renders the debugging page.
   * @param model Page data model.
   * @return Thymeleaf Template to render, by folder/name.
   */
  @GetMapping(value = "/debug")
  public String renderDebugPage(Model model) {
    log.warn("DEBUG Page render Get-requested.");

    try {
      log.info("Trying FTP Approach 1 with FTPClient");

      String domainName = "testA.testB.testC";
      String versionName = "versionX_RCY";

      model.addAttribute("domainName", domainName); // Set the vars into the model scope, to simulate how they're used on a page.
      model.addAttribute("versionName", versionName);
      model.addAttribute("selectedFilePath", "placeholder");

      FtpFileListPayload payload = ftpService.getListOfDirectoryContent(domainName, versionName);
      model.addAttribute("ftp_fileScanResults", payload);

      log.debug("FTP Retrieval performed.");
    } catch (IOException e) {
      log.error("FTP Retrieval has exploded: " + e.getMessage());
      model.addAttribute("ftp_success", false);
    }

    model.addAttribute("dynamicContent", "Standard content");
    model.addAttribute("selectedFilePath", "placeholder");

    return "debug/debugPage";
  }

  @GetMapping("/update-subsection")
  public String updateSubSection(Model model) {
    log.info("DEBUG: update of subsection Get-requested.");
    // Update the dynamic content and return the "subSection" template
    model.addAttribute("dynamicContent", "Updated content");
    return "fragments/ftp_fileSegment :: ftpFiles_subSectionContent";
  }

}
