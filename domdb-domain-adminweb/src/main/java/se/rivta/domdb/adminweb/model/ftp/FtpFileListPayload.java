package se.rivta.domdb.adminweb.model.ftp;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class FtpFileListPayload {
  private List<FtpFile> ftpFiles;

  private String domainName;
  private String versionName;
  private String desiredSubPath;
  private String fullDirectoryPath;
  private String publicDownloadUrlPrefix;

  // Retrieval Meta
  private Boolean connected = false;
  private Boolean directoryFound = false;
  private Boolean success = false;
  private String errorMessage;
}


