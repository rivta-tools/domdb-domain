package se.rivta.domdb.adminweb.controller;

import org.slf4j.Logger;
import org.apache.commons.vfs2.FileSystemException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import se.rivta.domdb.adminweb.model.ftp.FtpFileListPayload;
import se.rivta.domdb.adminweb.service.FtpService;

import java.io.IOException;
import se.rivta.domdb.adminweb.util.RoleConstants;

@Controller
public class FtpController {

  @Autowired
  FtpService ftpService;

  private static final Logger log = LoggerFactory.getLogger(FtpController.class);
  private static final String REDIRECT_DEBUG_PAGE = "redirect:/debug";

  @GetMapping("/file_search")
  public String findFiles(Model model,
                          @RequestParam String domainName,
                          @RequestParam String versionName) throws IOException {

    log.info("GET Request received to find files, using domain name {} and version name {}", domainName, versionName);


    FtpFileListPayload payload = ftpService.getListOfDirectoryContent(domainName, versionName);

    // Update the dynamic content and return the "subSection" template
    model.addAttribute("ftp_filesRetrieved", true);
    model.addAttribute("ftp_filePayload", payload);
    return "fragments/ftp :: fileTable";
  }

  @PostMapping("/file_upload")
  @Secured(RoleConstants.ROLE_EDITOR)
  public String create(@RequestParam String domainName,
                       @RequestParam String versionName,
                       @RequestParam("file") MultipartFile requestFile,
                       @RequestHeader(value = HttpHeaders.REFERER, required = false) final String referrer) throws IOException {

    if (requestFile == null || requestFile.isEmpty()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provided file parameter is either empty or null.");
    }

    log.info("POST Request received to store and persist a file, using Domain Name: \"{}\";" +
            " Version Name: \"{}\"; File Name: \"{}\"; File Size: {} Bytes.",
        domainName, versionName, requestFile.getOriginalFilename(), requestFile.getSize());

    ftpService.uploadFile(domainName, versionName, requestFile.getOriginalFilename(), requestFile.getBytes());

    if (referrer != null && !referrer.isBlank()) {
      // Try redirecting to page that held the FTP File Selector...
      return "redirect:" + referrer;
    }
    // ... or fallback to the main overview page for the specific DomainVersion.
    return "redirect:/domain/" + domainName + "/" + versionName;
  }

  @DeleteMapping("/file_delete")
  @Secured(RoleConstants.ROLE_EDITOR)
  public String delete(@RequestParam String domainName,
                       @RequestParam String versionName,
                       @RequestParam String fileName,
                       @RequestHeader(value = HttpHeaders.REFERER, required = false) final String referrer) throws FileSystemException {
    log.info("DELETE Request received to remove File: \"{}\", using Domain Name: \"{}\", Version Name \"{}\"",
        domainName, versionName, fileName);


    ftpService.deleteFile(domainName, versionName, fileName);

    if (referrer != null && !referrer.isBlank()) {
      // Try redirecting to page that held the FTP File Selector...
      return "redirect:" + referrer;
    }
    // ... or fallback to the main overview page for the specific DomainVersion.
    return "redirect:/domain/" + domainName + "/" + versionName;
  }

  @PostMapping("/processSelection")
  public String processSelection(@RequestParam("selectedFilePath") String selectedFilePath) {
    log.info("FTP PROCESSING SELECTION: filePath: {}", selectedFilePath);
    return REDIRECT_DEBUG_PAGE;
  }
}
