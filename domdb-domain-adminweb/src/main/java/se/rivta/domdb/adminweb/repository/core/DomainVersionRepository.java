package se.rivta.domdb.adminweb.repository.core;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.DomainVersion;

@Repository
public interface DomainVersionRepository extends JpaRepository<DomainVersion, Integer> {

  @Query("SELECT dv FROM DomainVersion dv WHERE dv.domain.name = :domainName AND dv.name = :versionName")
  Optional<DomainVersion> findDomainVersion(@Param("domainName")String domainName, @Param("versionName")String versionName);

  @Query("SELECT dv FROM DomainVersion dv " +
      "LEFT JOIN FETCH dv.descriptionDocuments " +
      "LEFT JOIN FETCH dv.reviews " +
      "LEFT JOIN FETCH dv.interactionDescriptions " +
      "WHERE dv.domain.name = :domainName " + "AND dv.name = :versionName"
  )
  Optional<DomainVersion> findDomainVersionWithDetails(@Param("domainName") String domainName, @Param("versionName") String versionName);

  @Query("SELECT CASE WHEN COUNT(dv) > 0 THEN true ELSE false END FROM DomainVersion dv JOIN dv.domain d WHERE dv.name = :name AND d.name = :domainName")
  boolean existsByNameAndDomainName(@Param("name") String name, @Param("domainName") String domainName);

}
