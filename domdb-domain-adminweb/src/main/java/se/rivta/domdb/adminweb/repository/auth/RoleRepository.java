package se.rivta.domdb.adminweb.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import se.rivta.domdb.adminweb.model.auth.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

  Role findRoleByAuthority(String authority);
}
