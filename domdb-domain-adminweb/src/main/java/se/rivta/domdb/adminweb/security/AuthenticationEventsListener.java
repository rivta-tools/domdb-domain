package se.rivta.domdb.adminweb.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.authentication.event.LogoutSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationEventsListener implements ApplicationListener<ApplicationEvent> {

  private static final Logger logger = LoggerFactory.getLogger(AuthenticationEventsListener.class);

  @Override
  public void onApplicationEvent(ApplicationEvent event) {
    if (event instanceof InteractiveAuthenticationSuccessEvent) {
      String userName = getUserName(
          ((InteractiveAuthenticationSuccessEvent) event).getAuthentication());
      logger.info("Användaren {} loggades in.", userName);
    } else if (event instanceof AbstractAuthenticationFailureEvent) {
      Exception exception = ((AbstractAuthenticationFailureEvent) event).getException();
      logger.info("Det gick inte att autentisera användaren. Fel: {}", exception.getMessage());
    } else if (event instanceof LogoutSuccessEvent) {
      String userName = getUserName(
          ((LogoutSuccessEvent) event).getAuthentication());
      logger.info("Användaren {} loggades ut.", userName);
    }
  }

  private String getUserName(Authentication authentication) {
    if (authentication != null && authentication.getPrincipal() instanceof DefaultOAuth2User) {
      return ((DefaultOAuth2User) authentication.getPrincipal()).getName();
    }
    return null;
  }
}