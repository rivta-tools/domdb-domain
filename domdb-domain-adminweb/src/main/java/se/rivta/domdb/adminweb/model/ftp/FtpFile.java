package se.rivta.domdb.adminweb.model.ftp;

import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

@Getter
public class FtpFile {
  private final String name;
  private final long size;
  private final Date date;
  private final String prettySize;
  private final String prettyDate;
  private final String fullPath;

  private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss XXX");

  public FtpFile(String name, long size, Date date, String fullPath) {
    this.name = name;
    this.size = size;
    this.date = date;
    this.fullPath = fullPath;

    this.prettySize = FileUtils.byteCountToDisplaySize(this.size);

    sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Europe/Stockholm")));
    this.prettyDate = sdf.format(date);
  }
}
