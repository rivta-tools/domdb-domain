package se.rivta.domdb.adminweb.controller;

import java.util.Locale;
import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import se.rivta.domdb.adminweb.repository.auth.RoleRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRoleRepository;

@Controller
public class AuthController {

  @Autowired
  UserRoleRepository userRoleRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  MessageSource messageSource;

  private static final String ERROR_MESSAGE_ATTRIBUTE = "errorMessage";
  private static final String INFO_MESSAGE_ATTRIBUTE = "infoMessage";
  private static final Logger log = LoggerFactory.getLogger(AuthController.class);

  @GetMapping("/login")
  public String login(HttpServletRequest request, Model model) {
    log.info("Login Page render Get-requested.");

    String errorMessage = (String) request.getSession().getAttribute(ERROR_MESSAGE_ATTRIBUTE);

    if (errorMessage != null) {
      model.addAttribute(ERROR_MESSAGE_ATTRIBUTE, errorMessage);
      request.getSession().removeAttribute(ERROR_MESSAGE_ATTRIBUTE);
    }

    String infoMessage = (String) request.getSession().getAttribute(INFO_MESSAGE_ATTRIBUTE);
    if (infoMessage != null) {
      model.addAttribute(INFO_MESSAGE_ATTRIBUTE, infoMessage);
      request.getSession().removeAttribute(INFO_MESSAGE_ATTRIBUTE);
    }

    return "auth/login";
  }

  @GetMapping("/login?error")
  public String loginError(Model model) {
    log.info("Login Error handling Get-requested");
    model.addAttribute(ERROR_MESSAGE_ATTRIBUTE,
        messageSource.getMessage("error.authentication", null, Locale.getDefault()));
    return "auth/login";
  }
}
