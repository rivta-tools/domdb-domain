package se.rivta.domdb.adminweb.security;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import se.rivta.domdb.adminweb.model.auth.User;
import se.rivta.domdb.adminweb.model.auth.UserRole;
import se.rivta.domdb.adminweb.repository.auth.RoleRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRoleRepository;
import se.rivta.domdb.adminweb.util.RoleConstants;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

  private final UserRepository userRepository;
  private final UserRoleRepository userRoleRepository;
  private final RoleRepository roleRepository;
  public static final String USERNAME = "username";

  @Autowired
  public CustomOAuth2UserService(UserRepository userRepository, UserRoleRepository userRoleRepository, RoleRepository roleRepository) {
    this.userRepository = userRepository;
    this.userRoleRepository = userRoleRepository;
    this.roleRepository = roleRepository;
  }

  @Override
  public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
    Map<String, Object> attributes = getUserAttributes(userRequest);
    String userName = (String) attributes.get(USERNAME);
    String password = (String) attributes.get("uuid");

    User user = userRepository.findByUsername(userName).orElseGet(() -> createNewUser(userName, password));

    if (user.isAccountLocked()) {
      throw new LockedException("Användarkontot är låst.");
    }
    if (!user.isEnabled()) {
      throw new DisabledException("Användarkontot är inaktiverat.");
    }

    List<SimpleGrantedAuthority> userAuthorities = getSimpleGrantedAuthorities(user);
    return new DefaultOAuth2User(userAuthorities, attributes, USERNAME);
  }

  private List<SimpleGrantedAuthority> getSimpleGrantedAuthorities(User user) {
    return userRoleRepository.findByUser(user).stream()
        .map(userRole -> new SimpleGrantedAuthority(userRole.getRole().getAuthority()))
        .collect(Collectors.toList());
  }


  private User createNewUser(String userName, String password) {
    User newUser = new User();
    newUser.setUsername(userName);
    newUser.setPassword(password);
    newUser.setVersion(0L);
    userRepository.save(newUser);

    UserRole userRole = new UserRole();
    userRole.setUser(newUser);
    userRole.setRole(roleRepository.findRoleByAuthority(RoleConstants.ROLE_USER));
    userRoleRepository.save(userRole);

    return newUser;
  }

  public Map<String, Object> getUserAttributes(OAuth2UserRequest userRequest) {
    OAuth2User oAuth2User = super.loadUser(userRequest);
    return oAuth2User.getAttributes();
  }
}
