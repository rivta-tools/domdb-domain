package se.rivta.domdb.adminweb.model.viewmodel;

import java.util.Date;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.DescriptionDocumentType;

public class DocumentViewModel implements Comparable<DocumentViewModel> {

  private Integer id;
  private DescriptionDocumentType documentType;
  private String fullPath;
  private String fileName;
  private Date lastChangedDate;

  public DocumentViewModel() {
  }

  public DocumentViewModel(DescriptionDocument descriptionDocument) {
    this.id = descriptionDocument.getId();
    this.documentType = descriptionDocument.getDocumentType();
    this.fullPath = descriptionDocument.getFullPath();
    this.fileName = descriptionDocument.getFileName();
    this.lastChangedDate = descriptionDocument.getLastChangedDate();
  }

  public DescriptionDocumentType getDocumentType() {
    return documentType;
  }

  public void setDocumentType(DescriptionDocumentType documentType) {
    this.documentType = documentType;
  }

  public String getFullPath() {
    return fullPath;
  }

  public void setFullPath(String fullPath) {
    this.fullPath = fullPath;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Date getLastChangedDate() {
    return lastChangedDate;
  }

  public void setLastChangedDate(Date lastChangedDate) {
    this.lastChangedDate = lastChangedDate;
  }

  public Integer getId() {
    return id;
  }

  @Override
  public int compareTo(DocumentViewModel o) {
    return this.id.compareTo(o.id);
  }
}
