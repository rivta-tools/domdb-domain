package se.rivta.domdb.adminweb.repository.core;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.Domain;

import java.util.Optional;

@Repository
public interface DomainRepository extends JpaRepository<Domain, Integer> {

    List<Domain> findAllByOrderByName();

    boolean existsDomainByName(String name);

    @Query("SELECT d FROM Domain d WHERE d.name = :name")
    Optional<Domain> findDomainByName(@Param("name") String name);

    @Query("SELECT d FROM Domain d LEFT JOIN FETCH d.versions WHERE d.name = :name")
    Optional<Domain> findDomainByNameAndFetchVersions(@Param("name") String name);
}
