package se.rivta.domdb.adminweb.repository.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.DomainType;

@Repository
public interface DomainTypeRepository extends JpaRepository<DomainType, Integer>  {
}
