package se.rivta.domdb.adminweb.model.viewmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.DomainVersion;
import se.rivta.domdb.domain.core.InteractionDescription;
import se.rivta.domdb.domain.core.Review;

public class DomainVersionViewModel implements Comparable<DomainVersionViewModel> {

  private Integer id;
  private String domainVersionName;
  private boolean hiddenVersion;
  private String sourceControlPath;
  private String documentsFolder;
  private String interactionsFolder;
  private String zipUrl;

  private List<DocumentViewModel> descriptionDocuments = new ArrayList<>();
  private List<InteractionDescriptionViewModel> interactionDescriptions = new ArrayList<>();
  private List<ReviewViewModel> reviews = new ArrayList<>();


  public DomainVersionViewModel() {
  }

  public DomainVersionViewModel(DomainVersion domainVersion) {
    this.id = domainVersion.getId();
    this.domainVersionName = domainVersion.getName();
    this.sourceControlPath = domainVersion.getSourceControlPath();
    this.hiddenVersion = domainVersion.isHidden();
    this.documentsFolder = domainVersion.getDocumentsFolder();
    this.sourceControlPath = domainVersion.getSourceControlPath();
    this.interactionsFolder = domainVersion.getInteractionsFolder();
    this.zipUrl = domainVersion.getZipUrl();
  }

  public Integer getId() {
    return id;
  }

  public String getDomainVersionName() {
    return domainVersionName;
  }

  public void setDomainVersionName(String domainVersionName) {
    this.domainVersionName = domainVersionName;
  }

  public String getSourceControlPath() {
    return sourceControlPath;
  }

  public void setSourceControlPath(String sourceControlPath) {
    this.sourceControlPath = sourceControlPath;
  }

  public String getDocumentsFolder() {
    return documentsFolder;
  }

  public void setDocumentsFolder(String documentsFolder) {
    this.documentsFolder = documentsFolder;
  }

  public String getInteractionsFolder() {
    return interactionsFolder;
  }

  public void setInteractionsFolder(String interactionsFolder) {
    this.interactionsFolder = interactionsFolder;
  }

  public boolean isHiddenVersion() {
    return hiddenVersion;
  }

  public void setHiddenVersion(boolean hiddenVersion) {
    this.hiddenVersion = hiddenVersion;
  }

  public String getZipUrl() {
    return zipUrl;
  }

  public void setZipUrl(String zipUrl) {
    this.zipUrl = zipUrl;
  }

  public List<DocumentViewModel> getDescriptionDocuments() {
    return descriptionDocuments;
  }

  public void setDescriptionDocuments(Set<DescriptionDocument> descriptionDocuments) {
    this.descriptionDocuments = descriptionDocuments.stream().
        map(DocumentViewModel::new).sorted().collect(Collectors.toList());
  }

  public List<InteractionDescriptionViewModel> getInteractionDescriptions() {
    return interactionDescriptions;
  }

  public void setInteractionDescriptions(Set<InteractionDescription> interactionDescriptions) {
    this.interactionDescriptions = interactionDescriptions.stream().
        map(InteractionDescriptionViewModel::new).sorted().collect(Collectors.toList());
  }

  public List<ReviewViewModel> getReviews() {
    return reviews;
  }

  public void setReviews(Set<Review> reviews) {
    this.reviews = reviews.stream().map(ReviewViewModel::new).sorted().collect(Collectors.toList());
  }

  @Override
  public int compareTo(DomainVersionViewModel o) {
    return this.id.compareTo(o.id);
  }

  @Override
  public boolean equals(Object o) { // Currently ignores lists to reduce complexity.
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DomainVersionViewModel that = (DomainVersionViewModel) o;
    return isHiddenVersion() == that.isHiddenVersion() && Objects.equals(getId(), that.getId()) && Objects.equals(getDomainVersionName(), that.getDomainVersionName()) && Objects.equals(getSourceControlPath(), that.getSourceControlPath()) && Objects.equals(getDocumentsFolder(), that.getDocumentsFolder()) && Objects.equals(getInteractionsFolder(), that.getInteractionsFolder()) && Objects.equals(getZipUrl(), that.getZipUrl());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getDomainVersionName(), isHiddenVersion(), getSourceControlPath(), getDocumentsFolder(), getInteractionsFolder(), getZipUrl());
  }

  @Override
  public String toString() {
    return "DomainVersionViewModel{" +
        "id=" + id +
        ", domainVersionName='" + domainVersionName + '\'' +
        ", hiddenVersion=" + hiddenVersion +
        ", sourceControlPath='" + sourceControlPath + '\'' +
        ", documentsFolder='" + documentsFolder + '\'' +
        ", interactionsFolder='" + interactionsFolder + '\'' +
        ", zipUrl='" + zipUrl + '\'' +
        ", descriptionDocuments=" + descriptionDocuments +
        ", interactionDescriptions=" + interactionDescriptions +
        ", reviews=" + reviews +
        '}';
  }
}
