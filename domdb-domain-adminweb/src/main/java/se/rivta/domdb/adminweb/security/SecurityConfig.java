package se.rivta.domdb.adminweb.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@SuppressWarnings("unused")
public class SecurityConfig {

  @Autowired
  private CustomOAuth2UserService customOAuth2UserService;

  @Autowired
  private CustomLogoutSuccessHandler logoutSuccessHandler;

  @Bean
  protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http
        .sessionManagement(session -> session
            .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED))
        .headers()
            .frameOptions().sameOrigin()
            .and()
        .authorizeRequests()
            .anyRequest().permitAll()
            .and()
        .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessHandler(logoutSuccessHandler)
            .invalidateHttpSession(true)
            .clearAuthentication(true)
            .deleteCookies("JSESSIONID")
            .and()
        .oauth2Login().
            redirectionEndpoint().
                baseUri("/oauth/bitbucket/callback")
            .and()
            .loginPage("/login")
            .failureUrl("/login?error")
            .userInfoEndpoint()
                .userService(customOAuth2UserService);
    return http.build();
  }
}