package se.rivta.domdb.adminweb.repository.core;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.Interaction;

@Repository
public interface InteractionRepository extends JpaRepository<Interaction, Integer> {
}


