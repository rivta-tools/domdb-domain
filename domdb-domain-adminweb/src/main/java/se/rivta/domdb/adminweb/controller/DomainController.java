package se.rivta.domdb.adminweb.controller;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.rivta.domdb.adminweb.model.viewmodel.DomainTypeViewModel;
import se.rivta.domdb.adminweb.model.viewmodel.DomainViewModel;
import se.rivta.domdb.adminweb.repository.core.DomainRepository;
import se.rivta.domdb.adminweb.repository.core.DomainTypeRepository;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.DomainType;

@Controller
public class DomainController {

  private static final Logger log = LoggerFactory.getLogger(DomainController.class);

  @Autowired
  DomainRepository domainRepository;

  @Autowired
  DomainTypeRepository domainTypeRepository;

  @Autowired
  MessageSource messageSource;

  @RequestMapping("/")
  public String index() {
    return "redirect:/domains";
  }

  @GetMapping(value = "/domains")
  public String domains(Model model) {
    model.addAttribute("domainList", domainRepository.findAllByOrderByName());
    return "domain/list";
  }

  @GetMapping(value = "/domain/{domainName}")
  public String domain(Model model, @PathVariable("domainName") String domainName) {
    log.info("Domain View - GET page render: Incoming Request.");

    Domain domain = domainRepository.findDomainByNameAndFetchVersions(domainName).orElse(null);

    if (domain == null) {
      log.info("Domain View Get: User encountered a problem: Desired Domain could not be found.");

      String message =
          messageSource.getMessage("error.cannot.find.domain.with.name", new String[]{domainName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    DomainViewModel domainViewModel = new DomainViewModel(domain);
    domainViewModel.setVersions(domain.getVersions());
    domainViewModel.setDomainType(domain.getDomainType());
    model.addAttribute("domain", domainViewModel);

    log.debug("Domain View - GET page render: Finished preprocessing.");
    return "domain/domain";
  }

  @GetMapping(value = "/domain/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String createDomain(Model model) {
    log.info("Domain Creation - GET page render: Incoming Request.");

    DomainViewModel domainViewModel = new DomainViewModel();
    model.addAttribute("domainViewModel", domainViewModel);

    List<DomainType> domainTypes = domainTypeRepository.findAll();
    model.addAttribute("domainTypes",
        domainTypes.stream().
            sorted(Comparator.comparingInt(DomainType::getId)).
            map(DomainTypeViewModel::new).collect(Collectors.toList()));

    log.debug("Domain Creation - GET page render: Finished preprocessing.");
    return "domain/create";
  }

  @PostMapping("/domain/create")
  @PreAuthorize("hasRole('EDITOR')")
  @Transactional
  public String createDomain(@ModelAttribute("domainViewModel") DomainViewModel domainViewModel) {
    log.info("Domain Creation - POST endpoint: Incoming Request.");

    Domain domain = new Domain();

    String errorRedirection = processDomainData(domainViewModel, domain);
    if (errorRedirection != null) return errorRedirection;

    log.info("Domain Creation - Domain with id '{}' and name '{}' created.", domain.getId(), domain.getName());
    return "redirect:/domain/" + domain.getName();
  }

  @GetMapping("/domain/{domainName}/edit")
  @PreAuthorize("hasRole('EDITOR')")
  public String editDomain(Model model, @PathVariable String domainName) {
    log.info("Domain Edit - GET page render: Incoming Request.");

    Domain domain = domainRepository.findDomainByNameAndFetchVersions(domainName).orElse(null);

    if (domain == null) {
      log.info("Domain Edit Get: User encountered a problem: Desired Domain could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.with.name", new String[]{domainName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    DomainViewModel domainViewModel = new DomainViewModel(domain);
    domainViewModel.setDomainType(domain.getDomainType());
    model.addAttribute("domain", domainViewModel);

    List<DomainType> domainTypes = domainTypeRepository.findAll();
    model.addAttribute("domainTypes",
        domainTypes.stream().
            sorted(Comparator.comparingInt(DomainType::getId)).
            map(DomainTypeViewModel::new).collect(Collectors.toList()));

    log.debug("Domain Edit - GET page render: Finished preprocessing.");
    return "domain/edit";
  }

  @PostMapping("/domain/{domainName}/edit")
  @PreAuthorize("hasRole('EDITOR')")
  @Transactional
  public String editDomain(@PathVariable String domainName,
      @ModelAttribute DomainViewModel domainViewModel) {
    log.info("Domain Edit - POST endpoint: Incoming Request.");

    Domain domain = domainRepository.findDomainByNameAndFetchVersions(domainName).orElse(null);

    if (domain == null) {
      log.info("Domain Edit Post: User encountered a problem: Desired Domain could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.with.name", new String[]{domainName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    String errorRedirection = processDomainData(domainViewModel, domain);
    if (errorRedirection != null) return errorRedirection;

    log.info("Domain Edit - Domain with id '{}' and name '{}' edited.", domain.getId(), domain.getName());
    return "redirect:/domain/" + domain.getName();
  }

  @DeleteMapping("/domain/{domainId}/delete")
  @PreAuthorize("hasRole('EDITOR')")
  public String deleteDomain(@PathVariable Integer domainId) {
    log.info("Domain Delete - DELETE endpoint: Incoming Request.");

    domainRepository.deleteById(domainId);

    log.info("Domain Delete - Domain with id '{}' was deleted and removed from storage.", domainId);
    return "redirect:/domains";
  }

  private String transformDomainName(@PathVariable String name) {
    return name.replace(".", ":");
  }

  private String processDomainData(DomainViewModel domainViewModel, Domain domain) {
    log.debug("Domain Data Processing: Crunching provided data.");
    domain.setName(transformDomainName(domainViewModel.getDomainName()));
    domain.setSwedishShort(domainViewModel.getSwedishShort());
    domain.setSwedishLong(domainViewModel.getSwedishLong());
    domain.setOwner(domainViewModel.getOwner());
    domain.setDescription(domainViewModel.getDescription());
    domain.setHidden(domainViewModel.isHiddenDomain());
    domain.setInfoPageUrl(domainViewModel.getInfoPageUrl());
    domain.setIssueTrackerUrl(domainViewModel.getIssueTrackerUrl());
    domain.setSourceCodeUrl(domainViewModel.getSourceCodeUrl());

    DomainType domainType = domainTypeRepository.findById(domainViewModel.getDomainType().getId())
        .orElse(null);
    if (domainType == null) {
      log.info("Domain Data Processing: User encountered a problem: Desired Domain Type could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.type.with.id", new Integer[]{domain.getDomainType().getId()},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }
    domain.setDomainType(domainType);

    log.debug("Domain Data Processing: finished data processing. Will attempt save to storage.");
    domainRepository.save(domain);
    return null;
  }
}
