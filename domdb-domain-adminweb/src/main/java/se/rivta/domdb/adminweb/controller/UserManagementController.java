package se.rivta.domdb.adminweb.controller;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import se.rivta.domdb.adminweb.model.auth.User;
import se.rivta.domdb.adminweb.model.auth.UserRole;
import se.rivta.domdb.adminweb.model.viewmodel.UserViewModel;
import se.rivta.domdb.adminweb.repository.auth.RoleRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRoleRepository;
import se.rivta.domdb.adminweb.util.RoleConstants;

@Controller
public class UserManagementController {

  private static final Logger log = LoggerFactory.getLogger(UserManagementController.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  UserRoleRepository userRoleRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  MessageSource messageSource;

  public static final String ADMIN_ROLE = "hasRole('ADMIN')";

  @GetMapping("/users")
  @PreAuthorize(ADMIN_ROLE)
  public String users(Model model) {
    log.info("UserManagement ListView - GET page render: Incoming Request.");

    // Retrieving all users, and preparing list of view representations to be returned.
    List<User> userList = userRepository.findAll();
    List<UserViewModel> adminUserViewModelsList = new ArrayList<>();

    for (User user : userList) {
      UserViewModel userViewModel = generateUserViewModel(user);
      adminUserViewModelsList.add(userViewModel);
    }

    model.addAttribute("userList", adminUserViewModelsList);

    log.debug("UserManagement ListView - GET page render: Finished preprocessing.");
    return "manageUsers/list";
  }

  @GetMapping("/user/edit/{userName}")
  @PreAuthorize(ADMIN_ROLE)
  public String edit(Model model, @PathVariable String userName) {
    log.info("UserManagement UserEdit - GET page render: Incoming Request.");

    return userRepository.findByUsername(userName)
        .map(user -> {
          UserViewModel userViewModel = generateUserViewModel(user);
          model.addAttribute("user", userViewModel);
          log.info("whut?");
          return "manageUsers/edit";
        })
        .orElse("redirect:/error?errorMessage=" +
            URLEncoder.encode(
                messageSource.getMessage("error.cannot.find.user", new String[]{userName},
                    Locale.getDefault()), StandardCharsets.UTF_8));
  }

  private UserViewModel generateUserViewModel(User user) {
    UserViewModel userViewModel = new UserViewModel();
    userViewModel.setUsername(user.getUsername());

    List<UserRole> userRoleList = userRoleRepository.findByUser(user);

    for (UserRole userRole : userRoleList) {

      switch (userRole.getRole().getAuthority()) {
        case RoleConstants.ROLE_USER:
          userViewModel.setUserRole(true);
          break;
        case RoleConstants.ROLE_EDITOR:
          userViewModel.setEditorRole(true);
          break;
        case RoleConstants.ROLE_ADMIN:
          userViewModel.setAdminRole(true);
          break;

        default:
          throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT,
              messageSource.getMessage("error.unknown.role",
                  null, Locale.getDefault()));
      }
    }
    return userViewModel;
  }

  @PostMapping("/user/edit/{userName}")
  @PreAuthorize(ADMIN_ROLE)
  public String edit(@ModelAttribute UserViewModel userViewModel,
      @PathVariable String userName) {
    log.info("UserManagement UserEdit - POST endpoint: Incoming Request.");

    if (userViewModel.allRolesAreUnset()) {
      log.info("UserManagement UserEdit Post: Admin encountered a problem: Edited User had no roles set.");
      String message = messageSource.getMessage("select.role",
          null, Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    Optional<User> optionalUser = userRepository.findByUsername(userName);
    if (optionalUser.isEmpty()) {
      log.info("UserManagement UserEdit Post: Admin encountered a problem: Desired User could not be found.");
      return "redirect:/error?errorMessage=" +
          URLEncoder.encode(
              messageSource.getMessage("error.cannot.find.user", new String[]{userName},
                  Locale.getDefault()), StandardCharsets.UTF_8);

    }

    log.debug("UserManagement UserEdit Post: Analyzing UserRole Changes...");

    User user = optionalUser.get();
    List<UserRole> userRoleList = userRoleRepository.findByUser(user);

    // Check what roles already existed
    boolean existingUserIsRoleUser = false;
    boolean existingUserIsRoleEditor = false;
    boolean existingUserIsRoleAdmin = false;

    for (UserRole role : userRoleList) {
      switch (role.getRole().getAuthority()) {
        case RoleConstants.ROLE_USER:
          existingUserIsRoleUser = true;
          break;
        case RoleConstants.ROLE_EDITOR:
          existingUserIsRoleEditor = true;
          break;
        case RoleConstants.ROLE_ADMIN:
          existingUserIsRoleAdmin = true;
          break;
        default:
          throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
              messageSource.getMessage("error.unhandled.value",
                  null, Locale.getDefault()));
      }
    }

    if (userViewModel.isUserRole() == existingUserIsRoleUser &&
        userViewModel.isEditorRole() == existingUserIsRoleEditor &&
        userViewModel.isAdminRole() == existingUserIsRoleAdmin) {
      log.info("UserManagement UserEdit Post: Admin encountered a problem: User data encompassed no changes.");
      // No Change. Abort.
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
          messageSource.getMessage("request.for.change.contains.no.changes",
              null, Locale.getDefault()));
    }

    //Process changes:
    log.debug("UserManagement UserEdit Post: Processing and Saving UserRoles...");
    // Delete User Roles.
    userRoleRepository.deleteAll(userRoleList);

    // Save user roles as if they're new.
    if (userViewModel.isUserRole()) {
      UserRole roleUser = new UserRole();
      roleUser.setUser(user);
      roleUser.setRole(roleRepository.findRoleByAuthority(RoleConstants.ROLE_USER));
      userRoleRepository.save(roleUser);
    }
    if (userViewModel.isEditorRole()) {
      UserRole roleEditor = new UserRole();
      roleEditor.setUser(user);
      roleEditor.setRole(roleRepository.findRoleByAuthority(RoleConstants.ROLE_EDITOR));
      userRoleRepository.save(roleEditor);
    }
    if (userViewModel.isAdminRole()) {
      UserRole roleAdmin = new UserRole();
      roleAdmin.setUser(user);
      roleAdmin.setRole(roleRepository.findRoleByAuthority(RoleConstants.ROLE_ADMIN));
      userRoleRepository.save(roleAdmin);
    }

    // Save any changes to the user metadata.
    log.debug("UserManagement UserEdit Post: Saving Edited User");
    userRepository.save(user);

    log.info("UserManagement UserEdit Post: User has been edited and saved to storage.");
    return "redirect:/users";
  }

  @DeleteMapping("/user/delete/{userName}")
  @PreAuthorize(ADMIN_ROLE)
  public String deleteUser(@PathVariable String userName) {
    log.info("UserManagement UserDelete - DELETE endpoint: Incoming Request.");

    Optional<User> optionalUser = userRepository.findByUsername(userName);
    if (optionalUser.isEmpty()) {
      log.info("UserManagement UserDelete: Admin encountered a problem: Desired User could not be found.");
      return "redirect:/error?errorMessage=" +
          URLEncoder.encode(
              messageSource.getMessage("error.cannot.find.user", new String[]{userName},
                  Locale.getDefault()), StandardCharsets.UTF_8);
    } else {
      User user = optionalUser.get();
      List<UserRole> userRoleList = userRoleRepository.findByUser(user);

      log.debug("UserManagement UserDelete: Attempting deletion of UserRoles.");
      userRoleRepository.deleteAll(userRoleList);

      log.debug("UserManagement UserDelete: Attempting deletion of User.");
      userRepository.delete(user);
      log.info("UserManagement UserDelete: User has been deleted from storage.");

      return "redirect:/users";
    }
  }
}
