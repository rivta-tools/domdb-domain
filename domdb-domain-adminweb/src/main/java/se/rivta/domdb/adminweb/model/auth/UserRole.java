package se.rivta.domdb.adminweb.model.auth;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user_role")
@IdClass(UserRoleId.class)
public class UserRole implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @Id
  @ManyToOne
  @JoinColumn(name = "role_id", nullable = false)
  private Role role;


  ////
  // EQUAL AND HASH
  ////

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof UserRole)) {
      return false;
    }

    if (this == other)
      return true;

    if (getClass() != other.getClass())
      return false;

    return getUser().getId().equals(((UserRole) other).getUser().getId())
        && getRole().getId().equals(((UserRole) other).getRole().getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getUser().getId(), getRole().getId());
  }


  ////
  // GETTERS AND SETTERS
  ////

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }
}

