package se.rivta.domdb.adminweb.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import se.rivta.domdb.adminweb.model.auth.User;
import se.rivta.domdb.adminweb.model.auth.UserRole;

import java.util.ArrayList;
import se.rivta.domdb.adminweb.model.auth.UserRoleId;

public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleId> {
  ArrayList<UserRole> findByUser(User userId);
}
