package se.rivta.domdb.adminweb.repository.core;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.DescriptionDocumentType;


@Repository
public interface DocumentRepository extends JpaRepository<DescriptionDocument, Integer> {

  boolean existsByDomainVersionIdAndDocumentType(int domainVersionId,
      DescriptionDocumentType documentType);

  DescriptionDocument getByDomainVersionIdAndDocumentType(int domainVersionId,
      DescriptionDocumentType documentType);

  List<DescriptionDocument> getByDomainVersionId(int domainVersionId);
}
