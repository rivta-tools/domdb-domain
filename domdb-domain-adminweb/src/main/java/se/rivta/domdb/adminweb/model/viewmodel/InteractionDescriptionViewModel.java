package se.rivta.domdb.adminweb.model.viewmodel;

import java.util.Date;
import se.rivta.domdb.domain.core.InteractionDescription;

public class InteractionDescriptionViewModel implements Comparable<InteractionDescriptionViewModel> {

  private String interactionName;
  private String rivtaProfile;
  private int major;
  private int minor;
  private Date lastChangedDate;

  public InteractionDescriptionViewModel() {}

  public InteractionDescriptionViewModel(InteractionDescription description) {
    this.interactionName = description.getInteraction().getName();
    this.rivtaProfile = description.getInteraction().getRivtaProfile().name();
    this.major = description.getInteraction().getMajor();
    this.minor = description.getInteraction().getMinor();
    this.lastChangedDate = description.getLastChangedDate();
  }

  public String getInteractionName() {
    return interactionName;
  }

  public void setInteractionName(String interactionName) {
    this.interactionName = interactionName;
  }

  public String getRivtaProfile() {
    return rivtaProfile;
  }

  public void setRivtaProfile(String rivtaProfile) {
    this.rivtaProfile = rivtaProfile;
  }

  public int getMajor() {
    return major;
  }

  public void setMajor(int major) {
    this.major = major;
  }

  public int getMinor() {
    return minor;
  }

  public void setMinor(int minor) {
    this.minor = minor;
  }

  public Date getLastChangedDate() {
    return lastChangedDate;
  }

  public void setLastChangedDate(Date lastChangedDate) {
    this.lastChangedDate = lastChangedDate;
  }

  @Override
  public int compareTo(InteractionDescriptionViewModel o) {
    return this.interactionName.compareTo(o.interactionName);
  }
}
