package se.rivta.domdb.adminweb.model.viewmodel;

public class UserViewModel {
    private String username;
    private Boolean isUserRole;
    private boolean isEditorRole;
    private boolean isAdminRole;
    private boolean isLocked;

    public boolean allRolesAreUnset() {
        return !isUserRole && !isEditorRole && !isAdminRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isUserRole() {
        return isUserRole;
    }

    public void setUserRole(boolean userRole) {
        isUserRole = userRole;
    }

    public boolean isEditorRole() {
        return isEditorRole;
    }

    public void setEditorRole(boolean editorRole) {
        isEditorRole = editorRole;
    }

    public boolean isAdminRole() {
        return isAdminRole;
    }

    public void setAdminRole(boolean adminRole) {
        isAdminRole = adminRole;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }
}
