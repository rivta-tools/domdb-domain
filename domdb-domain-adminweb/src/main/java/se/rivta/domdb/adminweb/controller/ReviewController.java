package se.rivta.domdb.adminweb.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.rivta.domdb.adminweb.model.ftp.FtpFileListPayload;
import se.rivta.domdb.adminweb.service.FtpService;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewOutcomeRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewProtocolRepository;
import se.rivta.domdb.adminweb.repository.core.ReviewRepository;
import se.rivta.domdb.adminweb.model.viewmodel.ReviewViewModel;
import se.rivta.domdb.domain.core.DomainVersion;
import se.rivta.domdb.domain.core.Review;
import se.rivta.domdb.domain.core.ReviewOutcome;
import se.rivta.domdb.domain.core.ReviewProtocol;

@Controller
public class ReviewController {

  private static final Logger log = LoggerFactory.getLogger(ReviewController.class);
  @Autowired
  ReviewProtocolRepository reviewProtocolRepository;

  @Autowired
  ReviewOutcomeRepository reviewOutcomeRepository;

  @Autowired
  DomainVersionRepository domainVersionRepository;

  @Autowired
  ReviewRepository reviewRepository;

  @Autowired
  FtpService ftpService;

  @Autowired
  MessageSource messageSource;

  @GetMapping(value = "/domain/{domainName}/{versionName}/review/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String createReview(Model model,
      @PathVariable String domainName,
      @PathVariable String versionName) throws IOException {
    log.info("Review Create - GET page render: Incoming Request.");

    if (!domainVersionRepository.existsByNameAndDomainName(versionName, domainName)) {
      log.info("Review Create Get: User encountered a problem: Desired DomainVersion could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.version", new String[]{domainName, versionName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    model.addAttribute("review", new ReviewViewModel());
    model.addAttribute("reviewProtocols", reviewProtocolRepository.findAllReviewNames());
    model.addAttribute("reviewOutcomes", reviewOutcomeRepository.findAllReviewOutcomeNames());

    // Retrieve any files associated with the DomVer.
    log.debug("Review Create - Attempting to scan for FTP files associated with relevant DomainVersion.");
    FtpFileListPayload payload = ftpService.getListOfDirectoryContent(domainName, versionName);
    model.addAttribute("ftp_fileScanResults", payload);

    log.debug("Review Create - GET page render: Finished preprocessing.");
    return "review/create";
  }

  @PostMapping(value = "/domain/{domainName}/{versionName}/review/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String createReview(@PathVariable String domainName,
      @PathVariable String versionName,
      @ModelAttribute ReviewViewModel reviewModel) {
    log.info("Review Create - POST endpoint: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(domainName, versionName).orElse(null);

    if (domainVersion == null) {
      String message = messageSource.getMessage("error.cannot.find.domain.version",
          new String[]{domainName, versionName}, Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    if (reviewRepository.existsByDomainVersionIdAndReviewProtocolName(domainVersion.getId(),
        reviewModel.getReviewProtocol())) {
      String errorMessage =
          messageSource.getMessage("error.already.exists.review",
              new String[]{reviewModel.getReviewProtocol(), domainName, versionName}, Locale.getDefault());
      return "redirect:/error?errorMessage=" + errorMessage;
    }

    Review review = new Review();
    review.setDomainVersion(domainVersion);
    review.setReviewProtocol(
        reviewProtocolRepository.findReviewProtocolByName(reviewModel.getReviewProtocol()));
    review.setReviewOutcome(
        reviewOutcomeRepository.findReviewOutcomeByName(reviewModel.getReviewOutcome()));
    review.setReportUrl(reviewModel.getReportUrl());

    log.debug("Review Create POST: Saving Review");
    reviewRepository.save(review);

    log.info( "Review with id '{}' and report '{}' was created for domainVersion '{}', under domain '{}', and saved to storage.",
        review.getId(), review.getReportName(), domainVersion.getName(), domainVersion.getDomainName());

    return "redirect:/domain/" + domainName + "/" + versionName;

  }

  @GetMapping(value = "/domain/{domainName}/{versionName}/review/edit/{reviewId}")
  @PreAuthorize("hasRole('EDITOR')")
  public String editReview(Model model,
      @PathVariable String domainName,
      @PathVariable String versionName,
      @PathVariable Integer reviewId) throws IOException {
    log.info("Review Edit - GET page render: Incoming Request.");

    Review review = reviewRepository.findById(reviewId).orElse(null);
    if (review == null) {
      log.info("Review Edit Get: User encountered a problem: Desired Review could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.review",
              new Integer[]{reviewId}, Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    ReviewViewModel reviewViewModel = new ReviewViewModel(review);
    reviewViewModel.setReviewProtocol(review.getReviewProtocol().getName());
    reviewViewModel.setReviewOutcome(review.getReviewOutcome().getName());

    List<ReviewProtocol> reviewProtocols = reviewProtocolRepository.findAll();
    List<ReviewOutcome> reviewOutcomes = reviewOutcomeRepository.findAll();

    model.addAttribute("review", reviewViewModel);
    model.addAttribute("reviewProtocols", reviewProtocols);
    model.addAttribute("reviewOutcomes", reviewOutcomes);

    // Retrieve any files associated with the DomVer.
    log.debug("Review Edit - Attempting to scan for FTP files associated with relevant DomainVersion.");
    FtpFileListPayload payload = ftpService.getListOfDirectoryContent(domainName, versionName);
    model.addAttribute("ftp_fileScanResults", payload);

    log.debug("Review Edit - GET page render: Finished preprocessing.");
    return "review/edit";
  }

  @PostMapping(value = "/domain/{domainName}/{versionName}/review/edit/{reviewId}")
  @PreAuthorize("hasRole('EDITOR')")
  public String editReview(@PathVariable String domainName,
      @PathVariable String versionName,
      @PathVariable Integer reviewId,
      @ModelAttribute ReviewViewModel reviewModel) {
    log.info("Review Edit - POST endpoint: Incoming Request.");

    Review review = reviewRepository.findById(reviewId).orElse(null);
    if (review == null) {
      log.info("Review Edit Post: User encountered a problem: Desired Review could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.review",
              new Integer[]{reviewId}, Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(domainName, versionName).orElse(null);

    if (domainVersion == null) {
      log.info("Review Edit Post: User encountered a problem: Desired DomainVersion could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.version", new String[]{domainName, versionName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    review.setReviewOutcome(
        reviewOutcomeRepository.findReviewOutcomeByName(reviewModel.getReviewOutcome()));
    review.setReportUrl(reviewModel.getReportUrl());

    log.debug("Review Edit POST: Saving Review");
    reviewRepository.save(review);

    log.info( "Review with id '{}' and report '{}' was edited for domainVersion '{}', under domain '{}', and saved to storage.",
        review.getId(), review.getReportName(), domainVersion.getName(), domainVersion.getDomainName());
    return "redirect:/domain/" + domainName + "/" + versionName;
  }

  @DeleteMapping(value = "/review/{reviewId}/delete")
  @PreAuthorize("hasRole('EDITOR')")
  public String deleteReview(@PathVariable Integer reviewId) {
    log.info("Review Deletion - DELETE endpoint: Incoming Request.");

    Review review = reviewRepository.findById(reviewId).orElse(null);
    if (review == null) {
      log.info("Review Deletion: User encountered a problem: Desired Review could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.review",
              new Integer[]{reviewId}, Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    String domainName = review.getDomainVersion().getDomain().getName();
    String versionName = review.getDomainVersion().getName();

    log.debug("Review Delete: Attempting deletion.");
    reviewRepository.deleteById(reviewId);

    log.info( "Review with id '{}' and report '{}' was removed from domainVersion '{}', under domain '{}', and deleted from storage.",
        review.getId(), review.getReportName(), versionName, domainName);

    return "redirect:/domain/" + domainName + "/" + versionName;
  }

}
