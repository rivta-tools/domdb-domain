package se.rivta.domdb.adminweb.model.viewmodel;

import se.rivta.domdb.domain.core.DomainType;

public class DomainTypeViewModel {

  private Integer id;

  private String name;

  private String description;

  public DomainTypeViewModel(DomainType type) {
    this.id = type.getId();
    this.name = type.getName();
    this.description = type.getDescription();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
