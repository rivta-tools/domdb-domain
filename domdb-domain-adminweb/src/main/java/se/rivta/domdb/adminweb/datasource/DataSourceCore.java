package se.rivta.domdb.adminweb.datasource;

import jakarta.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "coreEntityManagerFactory",
    transactionManagerRef = "coreTransactionManager",
    basePackages = {"se.rivta.domdb.adminweb.repository.core"}
)
public class DataSourceCore {

  @Primary
  @Bean(name = "coreProperties")
  @ConfigurationProperties("domdb.datasource.core")
  public DataSourceProperties dataSourceProperties() {
    return new DataSourceProperties();
  }


  @Primary
  @Bean(name = "coreDataSource")
  @ConfigurationProperties(prefix = "domdb.datasource.core")
  public DataSource dataSource(
      @Qualifier("coreProperties") DataSourceProperties properties) {
    return properties.initializeDataSourceBuilder().build();
  }

  @Primary
  @Bean(name = "coreEntityManagerFactory")
  public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
      EntityManagerFactoryBuilder builder,
      @Qualifier("coreDataSource") DataSource dataSource) {
    return builder.dataSource(dataSource)
        .packages("se.rivta.domdb.domain.core")
        .persistenceUnit("core").build();
  }


  @Bean
  @Profile({"test", "local"})
  public DataSourceInitializer coreDataSourceInitializer(
      @Qualifier("coreDataSource") DataSource dataSource) {
    DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
    dataSourceInitializer.setDataSource(dataSource);
    ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
    databasePopulator.setSqlScriptEncoding("UTF-8");
    databasePopulator.addScript(new ClassPathResource("data_core.sql"));
    dataSourceInitializer.setDatabasePopulator(databasePopulator);
    return dataSourceInitializer;
  }

  @Primary
  @Bean(name = "coreTransactionManager")
  @ConfigurationProperties("spring.jpa")
  public PlatformTransactionManager transactionManager(
      @Qualifier("coreEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
    return new JpaTransactionManager(entityManagerFactory);
  }
}
