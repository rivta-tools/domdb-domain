package se.rivta.domdb.adminweb.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.session.jdbc.config.annotation.SpringSessionDataSource;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@SuppressWarnings("unused")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "authEntityManagerFactory",
    transactionManagerRef = "authTransactionManager",
    basePackages = {"se.rivta.domdb.adminweb.repository.auth"}
)
@EnableJdbcHttpSession(
    cleanupCron = "0 * * * * *" /*top of every minute*/
)
public class DataSourceAuth {

  @Bean(name = "authProperties")
  @ConfigurationProperties("domdb.datasource.auth")
  public DataSourceProperties dataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean(name = "authDatasource")
  @ConfigurationProperties(prefix = "domdb.datasource.auth")
  @SpringSessionDataSource
  public DataSource dataSource(
      @Qualifier("authProperties") DataSourceProperties properties) {
    return properties.initializeDataSourceBuilder().build();
  }

  @Bean(name = "authEntityManagerFactory")
  public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
      EntityManagerFactoryBuilder builder, @Qualifier("authDatasource") DataSource dataSource) {

    return builder.dataSource(dataSource)
        .packages("se.rivta.domdb.adminweb.model.auth")
        .persistenceUnit("auth").build();
  }

  @Bean(name = "authTransactionManager")
  @ConfigurationProperties("spring.jpa")
  public PlatformTransactionManager transactionManager(
      @Qualifier("authEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
    return new JpaTransactionManager(entityManagerFactory);
  }


  @Bean
  @Profile({"test", "local"})
  public DataSourceInitializer authDataSourceInitializer(
      @Qualifier("authDatasource") DataSource dataSource) {
    DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
    dataSourceInitializer.setDataSource(dataSource);
    ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
    databasePopulator.setSqlScriptEncoding("UTF-8");
    databasePopulator.addScript(new ClassPathResource("data_users.sql"));
    databasePopulator.addScript(new ClassPathResource("data_sessions.sql"));
    dataSourceInitializer.setDatabasePopulator(databasePopulator);
    return dataSourceInitializer;
  }
}
