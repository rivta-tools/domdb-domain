package se.rivta.domdb.adminweb.repository.core;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {
  boolean existsByDomainVersionIdAndReviewProtocolName(int domainVersionId,
      String reviewProtocolName);

  Review getByDomainVersionIdAndReviewProtocolName(int domainVersionId,
      String reviewProtocolName);

  List<Review> getByDomainVersionId(int id);
}
