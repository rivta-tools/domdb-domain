package se.rivta.domdb.adminweb.model.auth;

import jakarta.persistence.*;

@Entity
@Table(name = "role")
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  //@Size(min=1)
  @Column(nullable = false, unique = true)
  private String authority;

  public Role() {}

  public Role(String authority) {
    this.authority = authority;
  }

  public Long getId() {
    return id;
  }

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }
}

