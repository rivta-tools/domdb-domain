package se.rivta.domdb.adminweb.repository.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.ReviewProtocol;

import java.util.List;

@Repository
public interface ReviewProtocolRepository extends JpaRepository<ReviewProtocol, Integer> {

    @Query("SELECT r FROM ReviewProtocol r ORDER BY r.id ASC")
    List<ReviewProtocol> findAll();

    @Query("SELECT name FROM ReviewProtocol")
    List<String> findAllReviewNames();

    @Query("SELECT r FROM ReviewProtocol r WHERE r.name = :name")
    ReviewProtocol findReviewProtocolByName(@Param("name") String name);
}
