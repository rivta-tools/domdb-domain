package se.rivta.domdb.adminweb.controller;

import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.rivta.domdb.adminweb.model.viewmodel.DocumentViewModel;
import se.rivta.domdb.adminweb.repository.core.DocumentRepository;
import se.rivta.domdb.adminweb.repository.core.DomainVersionRepository;
import se.rivta.domdb.domain.core.DescriptionDocument;
import se.rivta.domdb.domain.core.DescriptionDocumentType;
import se.rivta.domdb.domain.core.DomainVersion;

@Controller
public class DocumentController {

  private static final Logger log = LoggerFactory.getLogger(DocumentController.class);

  @Autowired
  DocumentRepository documentRepository;

  @Autowired
  DomainVersionRepository domainVersionRepository;

  @Autowired
  MessageSource messageSource;

  @GetMapping(value = "/domain/{domainName}/{versionName}/document/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String createDocument(Model model, @PathVariable String domainName,
      @PathVariable String versionName) {
    log.info("Document Creation - GET page render: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersionWithDetails(domainName, versionName).orElse(null);

    if (domainVersion == null) {
      log.info("Document Creation GET: User encountered a problem: Desired domainVersion could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.version",
              new String[]{domainName, versionName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    String svnPath = domainVersion.getFullDocumentsPath();

    DocumentViewModel documentViewModel = new DocumentViewModel();
    model.addAttribute("document", documentViewModel);
    model.addAttribute("documentTypes", DescriptionDocumentType.values());
    model.addAttribute("svnPath", svnPath);
    log.debug("Document Creation - GET page render: Finished preprocessing.");
    return "document/create";
  }

  @PostMapping(value = "/domain/{domainName}/{versionName}/document/create")
  @PreAuthorize("hasRole('EDITOR')")
  public String createDocument(@PathVariable String domainName,
      @PathVariable String versionName,
      @ModelAttribute DocumentViewModel viewModel) {
    log.info("Document Creation - POST endpoint: Incoming Request.");

    DomainVersion domainVersion = domainVersionRepository.
        findDomainVersion(domainName, versionName).orElse(null);

    if (domainVersion == null) {
      log.info("Document Creation POST: User encountered a problem: Desired domainVersion could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.domain.version",
              new String[]{domainName, versionName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    if (documentRepository.existsByDomainVersionIdAndDocumentType(domainVersion.getId(),
        viewModel.getDocumentType())) {
      log.info("Document Creation POST: User encountered a problem: Document already exists.");
      String message =
          messageSource.getMessage("error.already.exists.document",
              new String[]{viewModel.getDocumentType().toString(), domainName, versionName},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    DescriptionDocument document = new DescriptionDocument();
    document.setDomainVersion(domainVersion);
    document.setFileName(viewModel.getFileName());
    document.setDocumentType(viewModel.getDocumentType());

    log.debug("Document Creation POST: Saving Document");
    documentRepository.save(document);

    log.info("Document with id '{}' and file name '{}' was created and saved to storage'.",
        document.getId(), document.getFileName());

    return "redirect:/domain/" + domainName + "/" + versionName;
  }

  @GetMapping(value = "/domain/{domainName}/{versionName}/document/edit/{documentId}")
  @PreAuthorize("hasRole('EDITOR')")
  public String editDocument(Model model,
      @PathVariable String domainName,
      @PathVariable String versionName,
      @PathVariable Integer documentId) {
    log.info("Document Edit - GET page render: Incoming Request.");

    DescriptionDocument document = documentRepository.findById(documentId).orElse(null);
    if (document == null) {
      log.info("Document Edit GET: User encountered a problem: Desired Document could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.description.document", new Integer[]{documentId},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    String svnPath = document.getDomainVersion().getFullDocumentsPath();
    model.addAttribute("svnPath", svnPath);
    model.addAttribute("document", new DocumentViewModel(document));
    model.addAttribute("documentTypes", DescriptionDocumentType.values());
    log.debug("Document Edit - GET page render: Finished preprocessing.");
    return "document/edit";
  }

  @Transactional
  @PostMapping(value = "/domain/{domainName}/{versionName}/document/edit/{documentId}")
  @PreAuthorize("hasRole('EDITOR')")
  public String editDocument(@PathVariable String domainName,
      @PathVariable String versionName, @PathVariable Integer documentId,
      @ModelAttribute DocumentViewModel viewModel) {
    log.info("Document Edit - POST endpoint: Incoming Request.");

    DescriptionDocument document = documentRepository.findById(documentId).orElse(null);
    if (document == null) {
      log.info("Document Edit POST: User encountered a problem: Desired Document could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.description.document", new Integer[]{documentId},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    document.setDocumentType(viewModel.getDocumentType());
    document.setFileName(viewModel.getFileName());
    log.info("Document Edit POST: Saving Document");
    documentRepository.save(document);

    log.info("Document with id '{}' and file name '{}' was edited and saved to storage'.",
        document.getId(), document.getFileName());

    return "redirect:/domain/" + domainName + "/" + versionName;
  }

  @DeleteMapping("/document/{documentId}/delete")
  @PreAuthorize("hasRole('EDITOR')")
  public String deleteDocument(@PathVariable Integer documentId) {
    log.info("Document Deletion - DELETE endpoint: Incoming Request.");

    DescriptionDocument document = documentRepository.findById(documentId).orElse(null);
    if (document == null) {
      log.info("Document deletion DELETE: User encountered a problem: Desired Document could not be found.");
      String message =
          messageSource.getMessage("error.cannot.find.description.document", new Integer[]{documentId},
              Locale.getDefault());
      return "redirect:/error?errorMessage=" + message;
    }

    String domainName = document.getDomainVersion().getDomain().getName();
    String versionName = document.getDomainVersion().getName();

    log.debug("Document Deletion DELETE: Deleting Document");
    documentRepository.deleteById(documentId);
    log.info("Document with id '{}' and file name '{}' was deleted and removed from storage'.",
        document.getId(), document.getFileName());

    return "redirect:/domain/" + domainName + "/" + versionName;
  }
}
