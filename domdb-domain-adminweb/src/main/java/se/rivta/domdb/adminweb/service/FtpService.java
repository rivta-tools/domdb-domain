package se.rivta.domdb.adminweb.service;

import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.provider.ftp.FtpFileSystemConfigBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import se.rivta.domdb.adminweb.model.ftp.FtpFile;
import se.rivta.domdb.adminweb.model.ftp.FtpFileListPayload;

import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FtpService {

  // Value from properties.
  @Value(value = "${ftp.uploadUrl}")
  String ftpUrlConnectionString;

  /**
   * This can be used to offset the user root directory (e.g. /ftp/domdb_user/)
   * to a directory which the user has permissions for in the ftp file system, (e.g. files/, downloads/ or uploads/)
   * Value overridden with property or ENV-VAL "ftp.uploadBasePath"
   */
  @Value(value = "${ftp.uploadBasePath:#{null}}")
  String directoryOffsetBasePath;

  @Value(value = "${ftp.downloadUrl}")
  String publicDownloadUrlPrefix;

  private static final Logger log = LoggerFactory.getLogger(FtpService.class);
  public static final String SUCCESSFULLY_LOCATED_FOLDER_MSG = "Successfully located: {}";

  /**
   * A function to retrieve a listing of files from within a specified folder.
   *
   * @param domainName QWE
   * @return An array containing gathered file information.
   * @throws IOException If the File handling cannot open or locate the folder specified, an Exception might be raised.
   */
  public FtpFileListPayload getListOfDirectoryContent(String domainName, String versionName) throws IOException {

    String desiredSubPath = FtpService.buildDomainAndVersionSubPath(domainName, versionName);
    List<FtpFile> foundFiles = new ArrayList<>(); // Will populate below, or remain empty on connection failure.

    // Prepare response with hitherto known data.
    FtpFileListPayload response = new FtpFileListPayload();
    response.setFtpFiles(foundFiles);
    response.setDomainName(domainName);
    response.setVersionName(versionName);
    response.setDesiredSubPath(desiredSubPath);
    response.setPublicDownloadUrlPrefix(publicDownloadUrlPrefix);

    String targetDirectoryFullPath = null;
    try {
      // Attempt to connect.
      FileObject desiredFolder = getDesiredFolder(desiredSubPath, false); // Throws on failed connection.
      response.setConnected(true); // If above line did not throw, record that a successful directory connection was established.

      if (desiredFolder.exists()) {
        if (desiredFolder.isFolder()) {

          response.setDirectoryFound(true);
          targetDirectoryFullPath = desiredFolder.getName().getPath();
          log.info(SUCCESSFULLY_LOCATED_FOLDER_MSG, targetDirectoryFullPath);

          FileObject[] children = desiredFolder.getChildren();

          for (FileObject child : children) {
            if (child.isFolder()) continue; // ignore contained folders for now.

            FtpFile file = new FtpFile(
                new java.io.File(
                    child.getName().getBaseName()).getName(),
                child.getContent().getSize(),
                new Date(child.getContent().getLastModifiedTime()),
                child.getName().getPath()
            );
            foundFiles.add(file);
          }
          // We've managed to connect and parse files. Record success.
          response.setSuccess(true);
        }
      } else {
        log.debug("Directory does not seem to exist. Response will be empty.");
      }

    } catch (FileSystemException e) {
      String msg = MessageFormat.format("Problem encountered during FTP File Retrieval and Processing: {0}", e.getMessage());
      response.setErrorMessage(msg);
      log.error(msg);
    }

    log.debug("FTP Found Files count: {}", foundFiles.size());

    response.setFullDirectoryPath(targetDirectoryFullPath);
    return response;
  }

  public void uploadFile(String domainName, String versionName, String fileName, byte[] bytes) throws IOException {
    log.info("Processing file upload.");
    if (fileName == null || fileName.isEmpty()) {
      throw new IllegalArgumentException("No file name provided!");
    }
    if (bytes.length == 0) {
      throw new IllegalArgumentException("No file content provided!");
    }
    log.debug("File upload parameters ok.");

    String desiredSubPath = FtpService.buildDomainAndVersionSubPath(domainName, versionName);
    FileObject desiredFolder = getDesiredFolder(desiredSubPath, true);

    if (!desiredFolder.exists()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not locate or create an upload folder.");
    }

    log.info(SUCCESSFULLY_LOCATED_FOLDER_MSG, desiredFolder.getName().getPath());
    log.debug("Resolving upload file...");
    FileObject remoteFile = desiredFolder.resolveFile(fileName);
    log.debug("Creating file...");
    remoteFile.createFile();
    log.debug("Writing to file...");
    try (OutputStream stream = remoteFile.getContent().getOutputStream()) {
      stream.write(bytes);
      stream.flush();
    }
    log.info("Finished file upload.");
  }

  public void deleteFile(String domainName, String versionName, String fileName) throws FileSystemException {
    log.info("Processing file DELETION!");
    String desiredSubPath = FtpService.buildDomainAndVersionSubPath(domainName, versionName);
    FileObject desiredFolder = getDesiredFolder(desiredSubPath, false);

    if (!desiredFolder.exists()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not locate or create an upload folder.");
    }
    log.info(SUCCESSFULLY_LOCATED_FOLDER_MSG, desiredFolder.getName().getPath());

    log.debug("Resolving file targeted for DELETION...");
    FileObject file = desiredFolder.resolveFile(fileName);

    if (!file.exists()) {
      log.warn("File targeted for deletion was not found.");
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "File targeted for deletion does not seem to exist.");
    }

    log.debug("Deleting file...");
    file.delete();
    log.info("File DELETION done!");
  }

  private static String buildDomainAndVersionSubPath(String domainKey, String versionName) {
    return domainKey.replaceAll("[.:]", "_") + "/" + versionName;
  }

  private FileObject getDesiredFolder(
      String desiredSubPath, boolean createFolderIfNotExist) throws FileSystemException {
    log.info("FTP Service will be attempting to connect to domain subdirectory at {}.", desiredSubPath);

    FileSystemManager ftpManager = VFS.getManager();

    // Configuration
    FileSystemOptions fileSystemOptions = new FileSystemOptions();
    FtpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(fileSystemOptions, true);
    FtpFileSystemConfigBuilder.getInstance().setPassiveMode(fileSystemOptions, true);

    // Attempt to resolve into the FTP using a connection string.
    if (ftpUrlConnectionString == null || ftpUrlConnectionString.isBlank()) {
      log.error("FTP Connection String appears to be null or blank.");
    }
    FileObject connection = ftpManager.resolveFile(ftpUrlConnectionString, fileSystemOptions);
    log.debug("FTP currently resolved into directory: {}", connection.getName().getPath());

    // Offset FTP file pointer if the 'Publish Base Path' variable is set.
    if (connection.exists() && directoryOffsetBasePath != null) {
      log.debug("FTP directory offset property is set. Attempting to resolve into directory: {}", directoryOffsetBasePath);
      connection = connection.resolveFile(directoryOffsetBasePath);
      log.info("FTP currently resolved into offset directory: {}", connection.getName().getPath());
    }

    // Ensure this target FTP Folder exists
    if (!connection.exists()) {
      throw new FileSystemException("Remote root directory at " + connection.getName().getPath() + " does not exist!");
    }

    log.debug("FTP connection root folder set as: {}", connection.getName().getPath());

    if (desiredSubPath == null || desiredSubPath.isEmpty()) {
      log.debug("Desired sub-directory value is null or empty. Using root directory.");
    } else {
      log.debug("FTP connection will attempt to resolve into sub-directory: {}", desiredSubPath);
      connection = connection.resolveFile(desiredSubPath, NameScope.DESCENDENT_OR_SELF);
    }

    if (!connection.exists() && createFolderIfNotExist) {
      log.debug("Desired directory non-existent. Creation of directory permitted. Creating directory...");
      connection.createFolder();
    }

    log.debug("FTP currently resolved into sub-directory: {}", connection.getName().getPath());
    return connection;
  }
}
