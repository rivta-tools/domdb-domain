package se.rivta.domdb.adminweb.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.rivta.domdb.adminweb.model.auth.User;
import se.rivta.domdb.adminweb.model.auth.UserRole;
import se.rivta.domdb.adminweb.repository.auth.UserRepository;
import se.rivta.domdb.adminweb.repository.auth.UserRoleRepository;


@Controller
@Profile("local")
public class LocalAuthController {

  private static final Logger log = LoggerFactory.getLogger(LocalAuthController.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserRoleRepository userRoleRepository;


  @GetMapping(value = "/localLogin/{userName}")
  public String login(@PathVariable String userName) {
    log.info("LocalAuth Controller - Login page GET: Incoming Login Attempt.");

    Optional<User> user = userRepository.findByUsername(userName);
    if (user.isPresent()) {
      Authentication auth = new UsernamePasswordAuthenticationToken(userName, null,
          getSimpleGrantedAuthorities(user.get()));

      SecurityContextHolder.getContext().setAuthentication(auth);
    }
    return "redirect:/domains";
  }

  private ArrayList<SimpleGrantedAuthority> getSimpleGrantedAuthorities(User user) {
    ArrayList<UserRole> userRoles = userRoleRepository.findByUser(user);
    ArrayList<SimpleGrantedAuthority> userAuthorities = new ArrayList<>();
    for (UserRole ur : userRoles) {
      userAuthorities.add(new SimpleGrantedAuthority(ur.getRole().getAuthority()));
    }
    return userAuthorities;
  }
}

