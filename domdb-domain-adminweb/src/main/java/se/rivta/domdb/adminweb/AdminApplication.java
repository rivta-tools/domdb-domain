package se.rivta.domdb.adminweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import se.rivta.domdb.adminweb.service.ConfigurationService;

@SpringBootApplication
// Below property override needed for deploys on Basefarm/Orange VM Environments. Not used in Kubernetes deploys.
@PropertySource(value = "file:${domdb_config_dir}/domdbdomain-config-override.properties", ignoreResourceNotFound = true)
public class AdminApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdminApplication.class, args);
	}

	@Autowired
	ConfigurationService configurationService;
}
