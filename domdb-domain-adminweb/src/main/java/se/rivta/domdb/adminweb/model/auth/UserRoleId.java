package se.rivta.domdb.adminweb.model.auth;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Embeddable;

@Embeddable
public class UserRoleId implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long user;
  private Long role;

  public Long getUser() {
    return user;
  }

  public void setUser(Long user) {
    this.user = user;
  }

  public Long getRole() {
    return role;
  }

  public void setRole(Long role) {
    this.role = role;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof UserRoleId)) {
      return false;
    }
    UserRoleId that = (UserRoleId) o;
    return user.equals(that.user) && role.equals(that.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, role);
  }
}
