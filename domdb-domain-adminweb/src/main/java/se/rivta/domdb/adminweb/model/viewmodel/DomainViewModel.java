package se.rivta.domdb.adminweb.model.viewmodel;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.DomainType;
import se.rivta.domdb.domain.core.DomainVersion;

public class DomainViewModel {

  private Integer id;
  private String domainName;
  private String owner;
  private String swedishLong;
  private String swedishShort;
  private String description;
  private String infoPageUrl;
  private String issueTrackerUrl;
  private String sourceCodeUrl;
  private boolean hiddenDomain;
  private DomainTypeViewModel domainType;
  private DomainVersionViewModel domainVersion;
  private List<DomainVersionViewModel> versions;

  public DomainViewModel() {
  }

  public DomainViewModel(Domain domain) {
    this.id = domain.getId();
    this.domainName = domain.getName();
    this.swedishLong = domain.getSwedishLong();
    this.swedishShort = domain.getSwedishShort();
    this.description = domain.getDescription();
    this.infoPageUrl = domain.getInfoPageUrl();
    this.issueTrackerUrl = domain.getIssueTrackerUrl();
    this.sourceCodeUrl = domain.getSourceCodeUrl();
    this.hiddenDomain = domain.isHidden();
    this.owner = domain.getOwner();
  }

  public Integer getId() {
    return id;
  }

  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public String getSwedishLong() {
    return swedishLong;
  }

  public void setSwedishLong(String swedishLong) {
    this.swedishLong = swedishLong;
  }

  public String getSwedishShort() {
    return swedishShort;
  }

  public void setSwedishShort(String swedishShort) {
    this.swedishShort = swedishShort;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getInfoPageUrl() {
    return infoPageUrl;
  }

  public void setInfoPageUrl(String infoPageUrl) {
    this.infoPageUrl = infoPageUrl;
  }

  public String getIssueTrackerUrl() {
    return issueTrackerUrl;
  }

  public void setIssueTrackerUrl(String issueTrackerUrl) {
    this.issueTrackerUrl = issueTrackerUrl;
  }

  public String getSourceCodeUrl() {
    return sourceCodeUrl;
  }

  public void setSourceCodeUrl(String sourceCodeUrl) {
    this.sourceCodeUrl = sourceCodeUrl;
  }

  public boolean isHiddenDomain() {
    return hiddenDomain;
  }

  public void setHiddenDomain(boolean hiddenDomain) {
    this.hiddenDomain = hiddenDomain;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public DomainVersionViewModel getDomainVersion() {
    return domainVersion;
  }

  public void setDomainVersion(DomainVersionViewModel domainVersion) {
    this.domainVersion = domainVersion;
  }

  public List<DomainVersionViewModel> getVersions() {
    return versions;
  }

  public void setVersions(Set<DomainVersion> versions) {
    this.versions = versions.stream().
        map(DomainVersionViewModel::new).sorted().collect(Collectors.toList());
  }

  public void setDomainType(DomainType domainType) {
    this.domainType = new DomainTypeViewModel(domainType);
  }

  public DomainTypeViewModel getDomainType() {
    return domainType;
  }
}
