package se.rivta.domdb.adminweb.repository.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.rivta.domdb.domain.core.InteractionDescription;
import se.rivta.domdb.domain.core.InteractionDescriptionId;

@Repository
public interface InteractionDescriptionRepository
    extends JpaRepository<InteractionDescription, InteractionDescriptionId> {
}
