INSERT INTO role (`id`,`authority`) VALUES (1,'ROLE_USER');
INSERT INTO role (`id`,`authority`) VALUES (2,'ROLE_EDITOR');
INSERT INTO role (`id`,`authority`) VALUES (3,'ROLE_ADMIN');

INSERT INTO user (`id`,`account_expired`,`account_locked`,`enabled`,`password`,`password_expired`,`username`) VALUES (501,'0','0','1','user','0','user');
INSERT INTO user (`id`,`account_expired`,`account_locked`,`enabled`,`password`,`password_expired`,`username`) VALUES (502,'0','0','1','editor','0','editor');
INSERT INTO user (`id`,`account_expired`,`account_locked`,`enabled`,`password`,`password_expired`,`username`) VALUES (503,'0','0','1','admin','0','admin');

INSERT INTO user_role (`role_id`,`user_id`) VALUES (1,501);
INSERT INTO user_role (`role_id`,`user_id`) VALUES (1,502);
INSERT INTO user_role (`role_id`,`user_id`) VALUES (2,502);
INSERT INTO user_role (`role_id`,`user_id`) VALUES (1,503);
INSERT INTO user_role (`role_id`,`user_id`) VALUES (2,503);
INSERT INTO user_role (`role_id`,`user_id`) VALUES (3,503);
