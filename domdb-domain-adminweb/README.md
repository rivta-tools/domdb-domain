# DomDB - Domändatabasen
<i>v2.0.0 - Spring MVC Version</i><br>

This version of the web administration frontend has been rebuild from the ground up to do away with the old
Grails-based solution used in versions below 2.0.0, and instead make use of Spring Boot, and Spring Web MVC.

## For Developers
### Local Development
Set the Spring Boot profile to 'local' to make use of an in-memory H2-database,
as well allow for connection to a locally hosted FTP server.
#### FTP Testing
If you wish to test the FTP functionality locally, you can use Docker to launch an FTP image. 
<br>
Suggested docker run below. (Bash script presumed)<br>
Set username and password to suitable values, 

````
docker run -d \
--name DomDB_FTP \
-p 21:21 \
-p 21000-21010:21000-21010 \
-e USERS="domdb|Secret123" \
-e ADDRESS=localhost \
delfer/alpine-ftp-server
````
Thereafter, use them in the spring boot local properties file as below pattern.
```
# Example pattern
ftp.uploadUrl=ftp://USER:SECRET@HOST:PORT/FOLDER
# Examples with values
ftp.uploadUrl=ftp://domdb:Secret123@localhost:21/uploads
ftp.uploadUrl=ftp://willywonka:SecretIngredient123@chocolatefactory.org:1337/chocolatestash
```
The property <i>ftp.uploadBasePath</i> sets a base path offset for which folder to use as default instead of the user root folder. Applies across all FTP connections, for all upload or download purposes
```
# Base folder offset examples:
ftp.uploadBasePath=file_folder/
ftp.uploadBasePath=uploads/
ftp.uploadBasePath=downloads/
```
The property <i>ftp.downloadUrl</i> is used as a path prefix when presenting links to rivta.se for file downloads; Prefixed before files and folders when listing FTP files.
```
ftp.downloadUrl=http://localhost/downloads
ftp.downloadUrl=https://rivta.se/downloads
```