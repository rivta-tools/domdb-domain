FROM maven:3-eclipse-temurin-17-alpine AS maven

WORKDIR /opt/build
RUN --mount=type=bind,source=.,target=proj \
    --mount=type=cache,target=/root/.m2 \
    cp -r proj builddir \
    && cd builddir \
    && mvn clean install -PdockerizedJar -DskipTests=true \
    && mkdir -p /opt/app \
    && cp /opt/build/builddir/domdb-domain-adminweb/target/*.jar /opt/app/app.jar


FROM eclipse-temurin:17-jre-alpine AS app
ENV DOMDB_LOG_METHOD=EcsConsole
RUN adduser -D -u 1000 app && mkdir /opt/app
COPY --from=maven /opt/app/ /opt/app/
USER ind-app
CMD java -XX:MaxRAMPercentage=75 ${JAVA_OPTS} -jar /opt/app/app.jar
