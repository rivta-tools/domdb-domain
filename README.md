Domändatabasen innehåller information om RIVTA tjänstedomäner.

Det finns 3 komponenter till som är relaterad till domdb-domain.

  * domdb-connections (https://bitbucket.org/rivta-tools/domdb-connection/)
  * domdb-public (https://bitbucket.org/rivta-tools/domdb-public/)
  * domdb-api (https://bitbucket.org/rivta-tools/domdb-api/)

![Domains_web.png](docs/images/Domains_web.png)

För varje **domän** lagras:

  * Versioner (t.ex. trunk eller 1.0\_RC4)
  * Interaktioner, med dess major och minor version (utan koppling till någon fysisk källkodsfil)
  * Tjänstekontrakt, med dess major och minor version (utan koppling till någon fysisk källkodsfil)

För varje **domänversion** lagras:

  * Vilka interaktioner som förekommer, samt dess beskrivning och länk till wsdl-fil (interaction\_description)
  * Vilka beskrivningsdokument som tagits fram (IS, TKB och AB)
  * Eventuella granskningar som gjorts av den aktuella versionen (review)
  * Länk till ev. releasepaket (zip\_url)

Varje **granskning** (review) hänvisar till

  * en specifik domänversion
  * vilken granskning som har gjorts (review\_protocol), t.ex. "AoR Teknik"
  * Granskningsresultatet (review\_outcome), t.ex. "Godkänd" eller "Delvis godkänd"
  * länk till granskningsrapporten

### Installation
För installationsinstruktioner av ovanstående:

  * [SystemRequirements](docs/SystemRequirements.md).
  * [DeveloperInstructions.md](docs/DeveloperInstructions.md)

Uppgradering:

  * [DomainUpgrade](docs/DomainUpgrade.md)
