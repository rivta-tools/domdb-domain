#!/bin/sh

maven_install ()
{
    # $1 means first argument passed to function

    jar=$(ls -c $1/*.jar)
    pom=$(ls -c $1/pom.xml)

    mvn -N install:install-file -Dfile=$jar -DpomFile=$pom
}

mvn -N install # install parent pom.xml (non-recursively)

maven_install domdb-domain-core
maven_install domdb-domain-integration
