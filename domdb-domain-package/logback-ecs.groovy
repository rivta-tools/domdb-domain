import co.elastic.logging.logback.EcsEncoder

appender("STDOUT", ConsoleAppender) {
    encoder(EcsEncoder) {}
}

root(DEBUG, ["STDOUT"])
