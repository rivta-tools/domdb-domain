## Systemkrav
Domdb-komponenterna har följande systemkrav:

Programvara       | Driftsmiljö | Utvecklingsmiljö
------ | ----------- | -----------------
Java   | JRE 7 eller 8 | JDK 7 eller 8
Groovy | 2.4+        | 2.4+
Grails | Behövs ej   | 2.5+
MySQL  | 5.5+        | 5.5+
Maven  | Behövs ej   | 3+
Tomcat | 7           | Behövs ej

Webbprojekten har dessutom följande krav:

  * en servlet container, t.ex. Tomcat

## Testad konfiguration
Verktygen har testats med följande versioner:

  * Ubuntu 14.04/Windows 8/Linux 
  * Java OpenJDK 7
  * Groovy 2.4
  * MySQL 5.5.29 och 5.5.41
  * Maven 3.0.5

## Installation
Installera följande paket:

  * mysql-server-5.5 (kan förstås även ligga på en separat server - i så fall installera istället mysql-client-5.5 eller -5.6)
  * maven (installerar även java openjdk-7-jdk)

Ladda ner och installera Groovy manuellt, enligt instruktionerna på http://groovy.codehaus.org/Installing+Groovy

Sätt Groovy environment-variablerna i filen ~/.profile

## Fortsättning
Du kan nu fortsätta till [DeveloperInstructions](DeveloperInstructions.md).