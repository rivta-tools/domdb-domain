Administrationswebben används för att hantera innehållet i domändatabasen.

Detta kräver att du tidigare har konfigurerat domändatabasen, se [DomainConfiguration](DomainConfiguration.md).

Det kräver också att en **servlet container** finns installerad på din server.

## Databas
För domain-adminweb (administrationswebben) behövs en ytterligare tom mysql-databas för användarkonton, förslagsvis kallad **Users**.

### Skapa tabeller
På grund av problem med Grails automatiska skapande av databaser (Grails vill skapa tabeller från alla datakällor i alla databaser), så behöver tabellerna i Users-databasen skapas manuellt. Bland den uppackade filerna från filen domdb-domain-x.y.z.zip finns filen userdb.sql som används för detta.

Filen kan exekveras i valfritt MySQL-gränssnitt.

När det är klart ska databasen ha fyra st tabeller:

  * user
  * role
  * user\_role
  * oauthid

### Konfigurationsfil

I den konfigurationsmapp som skapades i steget [CommonConfiguration](CommonConfiguration.md) (t.ex. **/etc/domdb**), skapa filen **adminweb-config.groovy**. Lägg till följande innehåll i filen:

```
dataSource_domains {
	url = 'jdbc:mysql://<servernamn>:3306/<domän-databasnamn>'
	username='<användare>'
	password='<lösenord>'
}

dataSource {
	url = 'jdbc:mysql://<servernamn>:3306/<user-databasnamn>'
	username = '<användare>'
	password = '<lösenord>'
}
```

Konfigurera parametrarna så att de pekar mot databaserna **Domains** och **Users**.

#### FTP-integration
Följande parametrar behöver konfigureras för att ftp-integrationen ska fungera, dvs möjligheten att ladda upp zip-filer och granskningsrapporter via GUI:t:

```
ftp {
    uploadUrl = 'protocol://username:password@host/baseFolder/'
    downloadUrl = 'http://host/baseFolder/'
}
```

**uploadUrl** användas av komponenten Apache Virtual File System för att koppla upp. Apache stöder ftp, sftp m.fl. Anpassa delarna protocol, username, password osv till aktuella värden. T.ex. ftp://sven:pwd1@ftp.rivta.se/downloads/. 

**downloadUrl** används enbart för att generera korrekta nedladdningslänkar till filer som valts i ftp-dialogen. Därför behöver "baseFolder" peka på samma mapp för både downloadUrl och uploadUrl. T.ex. http://rivta.se/downloads/

### Konfigurera miljövariabler
Vissa miljövariabler behöver finnas tillgängliga för din servlet container. I Tomcat kan det göras via filen /usr/share/tomcat7/bin/setenv.sh

#### domdb\_config\_dir
**domdb\_config\_dir** behöver peka på den mapp som innehåller de konfigurationsfiler du har skapat, t.ex. /etc/domdb

```
export domdb_config_dir="/etc/domdb"
```

#### Java-parametrar
För att undvika minnesproblem har följande Java-parametrar använts. I Tomcat kan java-parametrar anges via miljövariabeln **CATALINA\_OPTS**.

```
export CATALINA_OPTS="$CATALINA_OPTS -Xms128m -Xmx1024m -XX:PermSize=64m -XX:MaxPermSize=256m"
```

### Installera war-paket
Bland de uppackade filerna från domdb-domain-x.y.z.zip finns en fil med namn domdb-domain-adminweb-x.y.z.war. Installera denna i din servlet container. Om du har möjlighet, välj context path "admin", t.ex. genom att döpa om filen till **admin.war**. Det ger webbadressen http://servernamn:8080/admin.

För Tomcat-instruktioner, se [TomcatCheatSheet](TomcatCheatSheet.md).

### Kontrollera
Nu ska du kunna surfa till applikationen, via den adress som är konfigurerad i din servlet container, t.ex. http://servernamn:8080/admin eller http://servernamn:8080/domdb-domain-adminweb-x.y.z

## Konfigurera single sign on
För att slippa ge användare separata användarkonton för en så liten applikation som denna, så används Google Accounts och Bitbucket för att logga in till redigeringsläge i administrationswebben. Denna funktionalitet erbjuds konstnadsfritt av respektive leverantör.

### Konfiguration hos Google
All användning av Googles tjänster utgår från ett "project". Därför behöver du skapa ett projekt för domdb.

  * Gå till adressen https://console.developers.google.com. Logga in med ditt Google-konto om du inte redan är inloggad.
  * Klicka på **Create project**
  * Ange ett namn på projektet, t.ex. "DomDB test" och klicka på **Create**
  * När projektet är skapat väljer du **APIs & auth** i vänstermenyn
  * Därefter klickar du på **Credentials**
  * Klicka på **Create new Client ID**
  * Välj Application type = **Web application**
  * Fyll i email address och Product name

I dialogen **Create Client ID**, ange följande
  * Application type = **Web application**
  * Authorized Redirect URI:s = **http://dinserver:8080/contextPath/oauth/google/callback*, t.ex. http://localhost:8080/admin/oauth/google/callback
  * Klicka på Create Client ID**

### Konfiguration hos Bitbucket
I Bitbucket behöver applikationen registreras. Det kan göras under ett personligt Bitbucket-konto, eller under ett "Bitbucket team". Det senare är att föredra för att inte låsa konfigurationen till dig som person. Det kräver dock att du har admin-behörighet till ett Team.

För att lägga upp en applikation under ett Team, gör så här:

  * Gå till startsidan för ett team, t.ex. https://bitbucket.org/rivta-domains/
  * Klicka på **Manage team** uppe till höger
  * Klicka på Access management -> OAuth i vänstermenyn
  * Klicka på **Add consumer**

I dialogen **Add OAuth consumer** fyller du i följande:

  * Name: T.ex. "Inera domändatabas". Användare kommer att se detta när de loggar in hos Bitbucket.
  * Callback URL: Ange **http://dinserver:8080/contextPath/oauth/bitbucket/callback*, t.ex. http://localhost:8080/admin/oauth/bitbucket/callback
  * Permissions: Bocka för **Email** och **Read** under **Account**
  * Klicka på **Save**

Därefter kommer din key och secret att visas. Dessa ska användas i nästa steg.

### Ange Client secrets i konfigurationsfil
Konfigurationsfilen **adminweb-config.groovy** (lokalt på servern) kan nu uppdateras med de uppgifter som genererats hos Google resp Bitbucket.

```
oauth {
    providers {
        google {
            key = '`<Client ID från Google>`'
            secret = '`<Client Secret från Google>`'
            callback = '`<Redirect URI från Google>`'
        }
        bitbucket {
            key = '`<Client key från Bitbucket>`'
            secret = '`<Client Secret från Bitbucket>`'
            callback = '`<Redirect URI från Bitbucket>`'
        }
    }
}
```

### Testa
Starta nu om adminweb-applikationen eller hela din servlet container. Surfa därefter till adminweb-gränssnittet och klicka på **Logga in** uppe till höger. På efterföljande sida klickar du på antingen **Google Accounts** eller **Bitbucket**.

Efter att du har loggat in och godkänt att applikationen DomDB får läsa din kontoinformation ska du komma tillbaka till administrationswebben.

#### Registreringssteget
När du kommer tillbaka till administrationswebben med ett för applikationen nytt användarkonto kommer en dialog om du vill registrera dig som användare.

Genom att välja "Registrera" skapas en rad i tabellen "user" i databasen Users. Kontot ges dock enbart rollen **User**, vilket för närvarande ger samma behörigheter som en anonym användare.

#### Inloggad
Om hela inloggningssteget lyckas så ska du komma tillbaka till administrationswebbens startsida och du ska se ditt användarnamn uppe till höger.

## Tilldelning av roller
För att kunna redigera information via administrationswebben så måste användarkontot tilldelas rollen Editor. 

### Skapa rolltilldelning i grafiskt gränssnitt
AdminWeb använder en plugin kallad spring-security-ui, som skapar administrationssidor under sökvägen admin/users. Dessa sidor är behörighetsstyrda så att enbart användare med rollen ROLE_ADMIN kan använda detta gränssnitt. Dessa användare ser "ROLE_ADMIN" uppe till höger när de är inloggade, tillsammans med en länk som leder till dessa administrationssidor.

Med hjälp av dessa sidor kan du söka upp användare och tilldela roller.

### Skapa rolltilldelning i MySQL
En reservlösning är att skapa rolltildelningen direkt i MySQL. 

Logga in till MySQL på valfritt sätt och gör följande:

  * Ta reda på id-numret för användarkontot, genom att titta i tabellen **user**
  * Ta reda på id-numret för rollen **ROLE\_EDITOR**, genom att titta i tabellen **role**
  * Skapa en ny post i tabellen **user\_role**, som kopplar användaren till rollen.

### Testa
Genom att stänga webbläsaren och logga in på nytt ska användaren ha fått rollen tilldelad. Det kan bekräftas genom att aktuella roller visas under användarens e-postadress uppe till höger i inloggat läge.