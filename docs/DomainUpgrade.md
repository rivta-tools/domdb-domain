## Zip-fil
Till att börja med behövs en zip-fil för den release du vill installera. 

```
mvn clean package
```
Ladda upp zip-filen till din domdb-mapp på servern.


## Installera binärfiler
Innehåller följande artifakt: 
  * /bin - war filer, *.jar filer som behövs för att köra *.groovy skript filer
  * /scripts - groovy skript filer
  * /db_setup_script - databas skript för grundläggande installation, behövs ej för uppgradering.

## Uppdatera databas
Kör följande script för att säkerställa att databasen har alla tabeller och kolumner som behövs:

```
groovy verifyDomainDB.groovy
```

## Uppgradera AdminWeb
Om du använder administrations-webben behöver även den uppgraderas. Det görs genom att:

  * Avinstallera den tidigare versionen av AdminWeb i din servlet container
  * Installera den nya war-filen som ligger i mappen domdb-domain-adminweb.

För Tomcat-instruktioner, se Inera ICC Confluence.

## Uppgradera scripts
Ta en backup på gamla scripts och sen kopiera nya scripts