Denna instruktion beskriver hur Domain-komponenterna installeras. För information om vad detta innehåller, se [Databas diagram](../README.md).

## Grundläggande konfiguration
Viss grundläggande konfiguration behöver göras. Se [CommonConfiguration](CommonConfiguration.md)

## Databaser
För domain-core (domändatabasen) behövs en tom mysql-databas, förslagsvis kallad **Domains**.

För domain-adminweb (administrationswebben) behövs en ytterligare tom mysql-databas för användarkonton, förslagsvis kallad **Users**.

### Databaskonfiguration

I den konfigurationsmapp som skapades i steget [CommonConfiguration](CommonConfiguration.md) (t.ex. **/etc/domdb**), skapa filen **domains-jpa.properties**. Lägg till följande innehåll i filen:

```
javax.persistence.jdbc.url=jdbc:mysql://<servernamn>:3306/<databasnamn>
javax.persistence.jdbc.user=<användare>
javax.persistence.jdbc.password=<lösenord>
```

## Installation av programvara
Kopiera och packa upp den senaste domdb-domain-x.y.z-bin.zip i den programmapp som skapades i steget [CommonConfiguration](CommonConfiguration.md), t.ex. **~/domdb**.

`unzip domdb-domain-x.y.z-bin.zip`

## Skapa eller uppdatera databas
När ovanstående programvara installerats, och konfigurationsfilen **datasource-domains.properties** har skapats kan tabellerna i Domain-databasen skapas.

Kontrollera först att miljövariabeln **domdb\_config\_dir** är tillgänglig.

`echo $domdb_config_dir`

Sökvägen till konfigurationsmappen ska skrivas ut på skärmen, om inte kontrollera [CommonConfiguration](CommonConfiguration.md).

När allt ovanstående är gjort, ställ dig i den uppackade mappen domdb-domain-x.y.x och kör följande script:

`groovy verifyDomainDB.groovy`

Scriptet laddar först ner en del programfiler från Maven Central (i tysthet). Därefter skapas tabeller i databasen, med hjälp av Hibernate. Scriptet kan köras även om databasen har innehåll, t.ex. vid uppgraderingar, och skapar då de objekt som ev. saknas.

## Nästa steg
Om du vill använda det webbaserade administrationsgränssnittet kan du nu fortsätta till steget [AdminWebConfiguration](AdminWebConfiguration.md)