## Konfigurationsfiler
Verktygen behöver ett antal konfigurationsfiler. Dessa kan skapas var som helst i filsystemet, så länge miljövariabeln **domdb\_config\_dir** pekar ut sökvägen.

Om webbkomponenten (adminweb) ska användas så bör konfigurationsfilerna läggas på en "allmän" plats i filsystemet, så att även t.ex. tomcat-användare kommer åt dem. Ett förslag är att skapa mappen **/etc/domdb** i linux.

`sudo mkdir /etc/domdb`

Skapa sedan miljövariabeln **domdb\_config\_dir**. Så länge inga webbkomponenter används behöver den bara vara synlig för det inloggade användarkontot. 

Obs: För produktionsmiljö/QAmiljö specifik inställningar se interna ICC Confluence

### Exportera domdb_config_dir i Linux miljö
`export domdb_config_dir="/etc/domdb"`

## Katalog för programfiler
För produktionsmiljö specifik inställningar se interna ICC Confluence

## Fortsätta
Nu kan du fortsätta till [DeveloperInstructions](DeveloperInstructions.md)