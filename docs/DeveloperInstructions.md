# Utvecklarinstruktioner

Detta är ett s.k. Maven "multi module project", med ett "parent project" (domdb-domain) och följande "sub-modules":

* domdb-domain-core - innehåller persistens-klasser för att representera domäninsformation. Innehåller även script för att skapa och uppdatera databastabeller.
* domdb-domain-integration - kod och script för att uppdatera domändatabasen utifrån dess källkodsfiler.
* domdb-domain-adminweb - webbapplikation för att administrera domändatabasen, byggd med webbramverket Grails.
* domdb-domain-package - innehåller Maven-konfiguration för att skapa en release-zip med jar- och war-filer, groovy-script m.m.

För att underlätta så anges versionen av de flesta dependencies under "dependencyManagement" i parent pom.

## Programvaror
Du behöver följande programvaror installerade för att kunna köra koden i "utvecklarläge", samt för att paketera releaser:

* Java Development Kit (version 1.7+) - behöver vara installerad och miljövariabeln JAVA_HOME satt till JDK-installationens rotkatalog (ett steg ovanför javas bin-katalog)
* Groovy (version 2.4+) - behöver vara uppackad och dess bin-katalog finnas med i PATH
* Grails (version 2.5) - behöver vara uppackad och dess bin-katalog finnas med i PATH
* Maven (version 3+) - behöver vara uppackas och dess bin-katalog finnas med i PATH
* MySQL (version 5.5+) - behöver vara installerad och du ha ett konto med hög behörighet

## Konfigurera utvecklingsmiljön
### Klona Git-repository
Klona detta git-repository via en Git-klient, alternativt ladda ned källkoden som en zip.

### Bygga med Maven
mvn clean install

Om man har problem att bygga på grunda av PermGen sätta MAVEN_OPTS
set MAVEN_OPTS=-Xmx512m -XX:MaxPermSize=128m

Permanentlösning:
skapa fil mavenrc_pre.bat under %HOME% lägga MAVEN_OPTS

När man har byggt får man ett paket under ../domdb-domain-package/target/ med namn <applikation>-x.x.x.bin.zip. Det behövs vi för följa senare steg.

### Skapa miljövariabel
Skapa en katalog för domdb konfigurationsfiler någonstans, och peka ut sökvägen via miljövariabeln **domdb_config_dir**. Se [CommonConfiguration](CommonConfiguration.md)

### Skapa databaser
Skapa följande databaser i MySQL. Databasernas namn är inte viktigt, det anges via konfigurationsfiler sedan. 

#### Domains
Denna databas kommer att innehålla information om tjänstedomänerna. I utvecklingsmiljöer skapar du den enklast genom att läsa in en backup från prod. Eller köra verifyDomain.groovy
Förslag på namn 'domdb_domains'

#### Users
Denna databas kommer att innehålla användarkonton, roller och externa inloggningsidentiteter. Skapa den genom att köra sql-scriptet **userdb.sql** som ligger i katalogen "scripts" i adminweb-kodprojektet.

### Skapa inställningsfiler
**domains-jpa.properties**. Se [DomainConfiguration](DomainConfiguration.md)

**adminweb-config.groovy**. Se [AdminWebConfiguration](AdminWebConfiguration.md)

### Konfigurera OAuth
Adminweb använder OAuth mot Bitbucket för inloggning. Detta för att slippa separata användarkonton till en så liten applikation. Gör så här för att konfigurera:

#### adminweb-config.groovy
Klistra in följande innehåll i slutet av filen **adminweb-config.groovy**:

```
oauth {
    providers {
        google {
            key = 'key'
            secret = 'secret'
            callback = "http://localhost:8080/domdb-domain-adminweb/oauth/google/callback"
        }
        bitbucket {
            key = 'key'
            secret = 'secret'
            callback = 'http://localhost:8080/domdb-domain-adminweb/oauth/bitbucket/callback'
        }
    }
}
```
Se mer detailj kofiguration och hjälp på [AdminWebConfiguration](AdminWebConfiguration.md)
#### Bitbucket
Sedan behöver din lokala adminweb-instans registreras som en oauth-applikation på Bitbucket. Gör så här:

1. Logga in på Bitbucket
2. Klicka på ditt användarnamn uppe till höger och sedan på Settings
3. Välj Oauth i vänstermenyn
4. Välj Add Consumer
5. Fyll i följande:
  * Name: Vad du vill, t.ex. Lokal Adminweb
  * Callback URL: http://localhost:8080/domdb-domain-adminweb/oauth/bitbucket/callback
  * Permissions: Account/Email och Read
6. Tryck på Save

När du sparat ska du se din oauth **key** och **secret** i Bitbucket. Klistra in dessa under bitbucket/key och secret i konfigfilen **adminweb-config.groovy**.

## Köra programvaran
Innan någon av komponenterna kan exekveras så behöver jar-filerna installeras i din lokala Maven-cache. Vissa utvecklingsverktyg kan göra att detta steg inte behövs.

1. Starta en kommandoprompt och gå till domdb-domain (rotkatalogen i det klonade git-repositoriet)
2. Installera jar-filerna så att de hittas av scriptet: mvn install


### Integration-projektet
Integration-projektet körs främst via scriptet **updateDomainsFromSource.groovy**.

1. Starta en kommandoprompt och gå till domdb-domain-integration/src/main/scripts
2. Kör kommandot groovy updateDomainsFromSource.groovy

### Adminweb-projektet
Adminweb-projektet körs i en utvecklingsmiljö så här:

1. Starta en kommandoprompt och gå till katalogen domdb-domain-adminweb i källkodsträdet
2. Kör kommandot mvn grails:run-app

#### Skapa ett adminkonto
Admingränssnittet är read-only tills man loggat in. Gör så här för att skapa ett konto med full behörighet:

1. Surfa till http://localhost:8080/domdb-domain-adminweb och klicka på Logga in
2. Klicka på Bitbucket
3. Logga in på Bitbucket och godkänn att din applikation får läsa din kontoinformation
4. När du kommer tillbaka till din lokala adminweb, klicka på "Registrera".

Ett användarkonto skapas nu i din lokala users-databas och du loggas in. Det kan du se genom att "Logga in"-länken uppe till höger byts ut mot ditt Bitbucket-användarnamn.

Du har dock fortfarande inga behöighetsroller. Det finns ett GUI för att sätta roller, men det krävs att man har en admin-roll för att kunna komma åt det. Därför behöver du manuellt lägga editera tabellen **user_role** i Users-databasen. Lägg till poster så att ditt användarkonto kollas till alla de tre rollerna i roles-tabellen.

Stäng sedan din webbläsare och logga in igen. Nu ska du se dina roller under ditt användarnamn uppe till höger i adminwebben.


## Skapa en release

### Uppdatera versionsnummer
Versionsnumret behöver uppdateras i alla pom-filer. Det görs enklast med följande kommando:

```
cd <projektets rotkatalog>
mvn versions:set -DgenerateBackupPoms=False -DnewVersion=x.y.z
```

pom.xml i rotkatalogen ska nu ha fått det nya versionsnumret (under "version") och under-modulerna ska ha fått samma versionsnummer (under "parent/version").

Om det är viktigt att något script använder exakt denna nya version, uppdatera versionsnumret manuellt i Grapes/Grab-avsnittet längst upp i respektive script.

### Bygga release
Nu kan du bygga en release-zip med följande kommando:

```
mvn package
```

zip-filen skapas i katalogen domdb-domain-package/target/domdb-domain-x.x.x.bin.zip
Innehåller följande artifakt:
 
* /bin - war filer, *.jar filer som behövs för att köra *.groovy skript filer
* /scripts - groovy skript filer

### Checka in ändringar
Om allt ser bra ut:

* committa dina ändringar till Git, 
* skapa en tag med samma namn som versionsnumret
* pusha alltsammans till Bitbucket (dubbelkolla att taggen syns på Bitbucket).
