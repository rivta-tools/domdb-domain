package se.rivta.domdb.domain.core

import spock.lang.Specification


class DomainVersionSpec extends Specification {

    void "getDefaultSourceControlPath: trunk version"(){
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "trunk", domain: domain)

        then:
        version.getDefaultSourceControlPath() == "crm/scheduling/trunk"
    }

    void "getDefaultSourceControlPath: release version"(){
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "1.0", domain: domain)

        then:
        assert version.getDefaultSourceControlPath() == "crm/scheduling/tags/crm_scheduling_1.0"
    }

    void getFullInteractionsPath(){
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "1.0", domain: domain)
        version.sourceControlPath = "http://svn/crm/scheduling/tags/release"
        version.interactionsFolder = "kontrakt"

        then:
        version.getFullInteractionsPath() == "http://svn/crm/scheduling/tags/release/kontrakt"
    }

    void getFullDocumentsPath(){
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "1.0", domain: domain)
        version.sourceControlPath = "http://svn/crm/scheduling/tags/release"
        version.documentsFolder = "dokument"

        then:
        version.getFullDocumentsPath() == "http://svn/crm/scheduling/tags/release/dokument"
    }

    void "GetDomainName: Default domain name returned if no differentDomainName exists"() {
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "1.0", domain: domain)

        then:
        version.getDomainName() == "crm:scheduling"

    }

    void "GetDomainName: Different domain name returned if exists"() {
        when:
        def domain = new Domain(name: "crm:scheduling")
        def version = new DomainVersion(name: "1.0", domain: domain)
        version.differentDomainName = "crm:timetable"

        then:
        version.getDomainName() == "crm:timetable"

    }
}
