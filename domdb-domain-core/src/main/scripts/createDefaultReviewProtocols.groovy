package groovy

import se.rivta.domdb.domain.core.ReviewOutcome
import se.rivta.domdb.domain.core.ReviewProtocol

@Grapes([
		@Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-core', version = '1.9.0'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab(group='org.hibernate.javax.persistence', module='hibernate-jpa-2.1-api', version='1.0.0.Final'),
        @Grab(group='org.hibernate', module='hibernate-ehcache', version='4.3.7.Final'),
        @Grab('mysql:mysql-connector-java:8.0.29'),
        @GrabConfig(systemClassLoader = true)
])

import jakarta.persistence.EntityManager
import jakarta.persistence.EntityManagerFactory
import jakarta.persistence.Persistence
import jakarta.persistence.Query

void ensureProtocol(EntityManager manager, ReviewProtocol protocol) {
    Query query = manager.createQuery("select r from ReviewProtocol r where r.code =:code");
    query.setParameter("code", protocol.code)

    def results = query.getResultList()

    if (results.size() == 0) {
        manager.persist(protocol)
    }
}

void ensureOutcome(EntityManager manager, ReviewOutcome outcome) {
    Query query = manager.createQuery("select r from ReviewOutcome r where r.symbol =:symbol");
    query.setParameter("symbol", outcome.symbol)

    def results = query.getResultList()

    if (results.size() == 0) {
        manager.persist(outcome)
    }
}

def configFileName = "${System.getenv("domdb_config_dir")}/domains-jpa.properties"
def configStream = new FileInputStream(configFileName)
def jpaProperties = new Properties()
jpaProperties.load(configStream)

EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-domains", jpaProperties)
EntityManager manager = factory.createEntityManager()

manager.getTransaction().begin()

ensureProtocol(manager, new ReviewProtocol(code: "aor-s", name: "Arkitektur & Regelverk: Säkerhet"))
ensureProtocol(manager, new ReviewProtocol(code: "aor-i", name: "Arkitektur & Regelverk: Informatik"))
ensureProtocol(manager, new ReviewProtocol(code: "aor-t", name: "Arkitektur & Regelverk: Teknik"))
ensureProtocol(manager, new ReviewProtocol(code: "legacy-s", name: "Gammalt: Säkerhet"))
ensureProtocol(manager, new ReviewProtocol(code: "legacy-i", name: "Gammalt: Informatik"))
ensureProtocol(manager, new ReviewProtocol(code: "legacy-t", name: "Gammalt: Teknik"))

ensureOutcome(manager, new ReviewOutcome(name: "Godkänd", symbol: "G"))
ensureOutcome(manager, new ReviewOutcome(name: "Delvis Godkänd", symbol: "(G)"))
ensureOutcome(manager, new ReviewOutcome(name: "Underkänd", symbol: "U"))

manager.getTransaction().commit()
manager.close()
factory.close()

println("Success!")