#!/usr/bin/env groovy

@Grapes([
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-core', version = '1.10.3-SNAPSHOT'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab(group = 'mysql', module = 'mysql-connector-java', version = '8.0.29'),
        @GrabConfig(systemClassLoader = true)
])

import jakarta.persistence.EntityManager
import jakarta.persistence.EntityManagerFactory
import jakarta.persistence.Persistence

def configFileName = "${System.getenv("domdb_config_dir")}/domains-jpa.properties"
def configStream = new FileInputStream(configFileName)
def jpaProperties = new Properties()
jpaProperties.load(configStream)
jpaProperties["hibernate.hbm2ddl.auto"] = "update"

EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-domains", jpaProperties)
EntityManager manager = factory.createEntityManager()
manager.close()
factory.close()

println("Success!")