package se.rivta.domdb.domain.core

import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="name", unique = true)
)
class Domain {
    def Domain() {
        interactions = new HashSet<Interaction>()
        serviceContracts = new HashSet<ServiceContract>()
        versions = new HashSet<DomainVersion>()
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generate
    int id;

    @Column(nullable = false, length = 100)
    String name;

    @Column(columnDefinition = "text")
    String description;

    @Column(nullable = true)
    String swedishLong;

    @Column(nullable = true, length = 100)
    String swedishShort;

    @ManyToOne(optional = true, cascade = CascadeType.PERSIST)
    @NotFound(action = NotFoundAction.IGNORE)
    DomainType domainType;

    @Column(nullable = true, length = 100)
    String owner;

    @Column(columnDefinition = "bit default 0")
    boolean hidden

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)
    Set<Interaction> interactions;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)
    Set<ServiceContract> serviceContracts;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<DomainVersion> versions;

    @Column(nullable = true, length = 255)
    String issueTrackerUrl;

    @Column(nullable = true, length = 255)
    String sourceCodeUrl;

    @Column(nullable = true, length = 255)
    String infoPageUrl;
}
