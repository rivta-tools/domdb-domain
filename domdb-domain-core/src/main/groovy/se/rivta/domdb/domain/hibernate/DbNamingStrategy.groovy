package se.rivta.domdb.domain.hibernate

import org.hibernate.cfg.ImprovedNamingStrategy


class DbNamingStrategy extends ImprovedNamingStrategy {

    // Appends _id suffix to foreign key columns
    String foreignKeyColumnName(String propertyName, String propertyEntityName, String propertyTableName, String referencedColumnName) {
        String name = super.foreignKeyColumnName(propertyName, propertyEntityName, propertyTableName, referencedColumnName);
        return "${name}_${referencedColumnName}"
    }
}