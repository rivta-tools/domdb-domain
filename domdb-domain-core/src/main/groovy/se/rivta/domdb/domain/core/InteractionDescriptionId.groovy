package se.rivta.domdb.domain.core


import jakarta.persistence.Embeddable

@Embeddable
class InteractionDescriptionId implements Serializable {
    private Integer domainVersion;
    private Integer interaction;

    public InteractionDescriptionId() {}

    public InteractionDescriptionId(DomainVersion domainVersion, Interaction interaction) {
        this.domainVersion = domainVersion.getId()
        this.interaction = interaction.getId()
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof InteractionDescriptionId)) return false

        InteractionDescriptionId that = (InteractionDescriptionId) o

        if (domainVersion != that.domainVersion) return false
        if (interaction != that.interaction) return false

        return true
    }

    int hashCode() {
        int result
        result = domainVersion.hashCode()
        result = 31 * result + interaction.hashCode()
        return result
    }
}
