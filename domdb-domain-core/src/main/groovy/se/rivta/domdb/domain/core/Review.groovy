package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList = "domain_version_id, review_protocol_id", unique = true)
)
class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // db generated id
    int id;

    @ManyToOne(optional = false)
    DomainVersion domainVersion;

    @ManyToOne(optional = false)
    ReviewProtocol reviewProtocol

    @ManyToOne(optional = false)
    ReviewOutcome reviewOutcome

    @Column
    String reportUrl

    String getReportName() {
        return reportUrl != null ? reportUrl.substring(reportUrl.lastIndexOf("/") + 1) : null
    }
}
