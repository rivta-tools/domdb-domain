package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="namespace, major, minor", unique = true)
)
class ServiceContract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generate
    int id

    @Column(nullable = false, length = 100)
    String name

    @Column
    short major

    @Column
    short minor

    @Column(nullable = false)
    String namespace

    @ManyToOne(optional = false)
    Domain domain
}
