package se.rivta.domdb.domain.core

import jakarta.persistence.*

@Entity
@Table(
        indexes = @Index(columnList="code", unique = true)
)
class ReviewProtocol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id;

    @Column(nullable = false)
    String name;

    @Column(length = 25, nullable = false)
    String code;
}
