package se.rivta.domdb.domain.core


enum DescriptionDocumentType {
    Unknown,
    TKB,
    AB,
    IS
}
