package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Table(
        indexes = @Index(columnList="symbol", unique = true)
)
@Entity
class ReviewOutcome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id;

    @Column(length = 25, nullable = false)
    String name

    @Column(length = 10, nullable = false)
    String symbol


}
