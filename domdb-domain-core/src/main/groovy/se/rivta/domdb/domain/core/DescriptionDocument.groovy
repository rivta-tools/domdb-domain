package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="domain_version_id, fileName", unique = true)
)
class DescriptionDocument {
    private static final long serialVersionUID = 1l

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id;

    @ManyToOne(optional = false)
    DomainVersion domainVersion

    @Column(nullable = false)
    String fileName

    @Column(nullable = true)
    Date lastChangedDate

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    DescriptionDocumentType documentType

    String getFullPath(){
        return domainVersion.getFullDocumentsPath() + "/" + fileName
    }
}
