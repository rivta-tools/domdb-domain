package se.rivta.domdb.domain.core

import jakarta.persistence.*

@Entity
@Table(
        indexes = @Index(columnList="name", unique = true)
)
class DomainType {
    def DomainType() {
        domains = new HashSet<Domain>()
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generate
    int id;

    @Column(nullable = false, length = 100)
    String name;

    @Column(name = "plural_name", nullable = false, length = 100)
    String pluralName;

    @Column(columnDefinition = "text")
    String description;

    @OneToMany(mappedBy = "domainType", cascade = CascadeType.ALL)
    Set<Domain> domains;
}
