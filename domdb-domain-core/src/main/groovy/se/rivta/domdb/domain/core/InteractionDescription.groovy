package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@IdClass(InteractionDescriptionId.class)
@Table(
        indexes = @Index(columnList = "domain_version_id, interaction_id", unique = true)
)
class InteractionDescription implements Serializable {
    private static final long serialVersionUID = 1l

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "domain_version_id", nullable = false)
    DomainVersion domainVersion

    @Id
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "interaction_id", nullable = false)
    Interaction interaction

    @Column(columnDefinition = "text")
    String description

    @Column(nullable = true)
    Date lastChangedDate

    @Column(nullable = true, length = 100)
    String folderName

    @Column(nullable = true, length = 100)
    String wsdlFileName

    String getRelativePath() {
        if (folderName) {
            return "${folderName}/${wsdlFileName}"
        } else {
            return wsdlFileName
        }
    }

    String getFullPath() {
        return domainVersion.getFullInteractionsPath() + "/" + getRelativePath()
    }
}
