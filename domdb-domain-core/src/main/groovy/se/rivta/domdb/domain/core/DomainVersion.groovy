package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="name, domain_id", unique = true)
)
class DomainVersion {
    def DomainVersion(){
        interactionDescriptions = new HashSet<InteractionDescription>()
        reviews = new HashSet<ServiceContract>()
        descriptionDocuments = new HashSet<DescriptionDocument>()
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generate
    int id;

    @Column(nullable = false, length = 25)
    String name

    // Used when the (english) name of the parent domain has been renamed and
    // no longer match the contract namespaces in this domain version
    @Column(nullable = true, length = 100)
    String differentDomainName

    @Column(nullable = true)
    String sourceControlPath

    @Column(nullable = true)
    String documentsFolder

    @Column(nullable = true)
    String interactionsFolder

    @Column(nullable = true)
    String zipUrl

    @Column(columnDefinition = "bit default 0 not null")
    boolean hidden

    @OneToMany(mappedBy = "domainVersion", cascade = CascadeType.ALL)
    Set<DescriptionDocument> descriptionDocuments

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    Domain domain

    @OneToMany(mappedBy = "domainVersion", cascade = CascadeType.ALL)
    Set<InteractionDescription> interactionDescriptions

    @OneToMany(mappedBy = "domainVersion", cascade = CascadeType.ALL)
    Set<Review> reviews

    String getDefaultSourceControlPath(){
        StringBuilder path = new StringBuilder()
        path.append(domain.name.replace(':', '/'))

        if (name == 'trunk') {
            path.append('/trunk')
        } else {
            path.append('/tags/')
            path.append(domain.name.replace(':', '_'))
            path.append('_')
            path.append(name)
        }

        return path.toString()
    }

    String getDomainName() {
        if (differentDomainName) {
            return differentDomainName
        } else {
            return domain?.name
        }
    }

    String getDefaultTkbFileName() {
        // Example: TKB_clinicalprocess_activityprescription_actoutcome.docx
        return "TKB_${getDomainName().replace(':','_')}.docx"
    }

    String getDefaultAbFileName() {
        // Example: AB_clinicalprocess_activityprescription_actoutcome.docx
        return "AB_${getDomainName().replace(':','_')}.docx"
    }

    String getDefaultZipFileName() {
        // Example: ServiceContracts_<tjänstedomän>_<major_version>.<minor_version>.zip
        return "ServiceContracts_${getDomainName().replace(':','_')}_${name}.zip"
    }

    String getDefaultDocumentsFolder() {
        return "docs"
    }

    String getDefaultInteractionsFolder() {
        return "schemas/interactions"
    }

    String getFullInteractionsPath(){
        return "${sourceControlPath}/${interactionsFolder}"
    }

    String getFullDocumentsPath(){
        return "${sourceControlPath}/${documentsFolder}"
    }
}
