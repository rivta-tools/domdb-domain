package se.rivta.domdb.domain.core

import jakarta.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="namespace, major, minor", unique = true)
)
class Interaction {
    def Interaction(){
        interactionDescriptions = new HashSet<InteractionDescription>()
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generate
    int id

    @Column(nullable = false, length = 100)
    String name

    @Column(nullable = false)
    String namespace

    @Column(length = 25, nullable = false)
    @Enumerated(value = EnumType.STRING)
    RivtaProfile rivtaProfile

    @Column
    int major

    @Column
    int minor

    @ManyToOne(optional = true, cascade = CascadeType.PERSIST)
    ServiceContract initiatorContract

    @ManyToOne(optional = true, cascade = CascadeType.PERSIST)
    ServiceContract responderContract

    @OneToMany(mappedBy = "interaction", cascade = CascadeType.ALL)
    Set<InteractionDescription> interactionDescriptions

    @ManyToOne(optional = false)
    Domain domain

    String getDefaultFolderName() {
        // Example: GetCareContactsInteraction
        return name
    }

    String getDefaultWsdlFileName() {
        // Example: GetCareContactsInteraction_3.0_RIVTABP21.wsdl
        return "${name}_${major}.${minor}_${rivtaProfile.name().toUpperCase()}.wsdl"
    }
}
