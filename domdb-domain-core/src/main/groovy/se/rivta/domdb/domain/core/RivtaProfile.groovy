package se.rivta.domdb.domain.core

/**
 * Enum to avoid lots of case insensitive string matching
 */
enum RivtaProfile {
    Unknown,
    rivtabp20,
    rivtabp21
}
