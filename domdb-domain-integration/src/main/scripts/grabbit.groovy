#!/usr/bin/env groovy

/**
 *  The purpose of this scripts is to grab dependencies before running individual scripts.
 */

@Grapes([
        @GrabConfig(systemClassLoader = true),
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-core', version = '[1.10.2,)'),
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-integration', version = '[1.10.2,)'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab(group = 'mysql', module = 'mysql-connector-java', version = '8.0.29'),
        @Grab(group = 'org.apache.httpcomponents', module = 'httpclient', version = '4.3.6'),
        @Grab(group = 'commons-httpclient', module = 'commons-httpclient', version = '3.1'),
        @Grab(group = 'ch.qos.logback', module = 'logback-classic', version = '1.2.3'),
        @Grab(group = 'net.logstash.logback', module = 'logstash-logback-encoder', version = '6.4'),
        @Grab(group = 'co.elastic.logging', module = 'logback-ecs-encoder', version='1.5.0'),
        @Grab(group = 'org.slf4j', module = 'jul-to-slf4j', version = '1.7.22'),
        @Grab(group = 'org.apache.commons', module = 'commons-vfs2', version = '2.0'),
        @Grab(group = 'org.apache.commons', module = 'commons-configuration2', version = '2.8.0'),
        @Grab(group = 'commons-beanutils', module = 'commons-beanutils', version = '1.9.4')
])

import groovy.sql.Sql

println 'Done grabbing'