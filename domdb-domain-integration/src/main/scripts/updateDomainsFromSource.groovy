#!/usr/bin/env groovy

package groovy

@Grapes([
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-core', version = '[1.10.2,)'),
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-integration', version = '[1.10.2,)'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab(group = 'mysql', module = 'mysql-connector-java', version = '8.0.29'),
        @GrabConfig(systemClassLoader = true),
        @Grab(group = 'org.apache.httpcomponents', module = 'httpclient', version = '4.3.6'),
        @Grab(group = 'commons-httpclient', module = 'commons-httpclient', version = '3.1'),
        @Grab(group = 'ch.qos.logback', module = 'logback-classic', version = '1.2.3'),
        @Grab(group = 'net.logstash.logback', module = 'logstash-logback-encoder', version = '6.4'),
        @Grab(group = 'co.elastic.logging', module = 'logback-ecs-encoder', version='1.5.0'),
        @Grab(group = 'org.slf4j', module = 'jul-to-slf4j', version = '1.7.22'),
        @Grab(group = 'org.apache.commons', module = 'commons-vfs2', version = '2.0'),
        @Grab(group = 'org.apache.commons', module = 'commons-configuration2', version = '2.8.0'),
        @Grab(group = 'commons-beanutils', module = 'commons-beanutils', version = '1.9.4')
])

import org.apache.commons.vfs2.impl.StandardFileSystemManager
import org.apache.commons.configuration2.Configuration
import org.apache.commons.configuration2.builder.fluent.Configurations
import org.apache.commons.configuration2.ConfigurationConverter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import se.rivta.domdb.domain.core.Domain
import se.rivta.domdb.domain.core.DomainVersion
import se.rivta.domdb.domain.integration.BitbucketReaderFactory
import se.rivta.domdb.domain.integration.DocumentUpdater
import se.rivta.domdb.domain.integration.InteractionUpdater

import jakarta.persistence.EntityManager
import jakarta.persistence.EntityManagerFactory
import jakarta.persistence.Persistence
import jakarta.persistence.Query

SLF4JBridgeHandler.removeHandlersForRootLogger();
SLF4JBridgeHandler.install();

def Logger logger = LoggerFactory.getLogger("scriptLogger")

void cleanParents(EntityManager manager) {
    manager.transaction.begin()

    Query queryDeleteInteractions = manager.createNativeQuery("DELETE FROM interaction where id not in (SELECT interaction_id FROM interaction_description)")
    Query queryDeleteServiceContracts = manager.createNativeQuery("DELETE FROM service_contract where id not in (SELECT responder_contract_id FROM interaction where responder_contract_id is not null) and id not in (SELECT initiator_contract_id FROM interaction where initiator_contract_id is not null)")

    queryDeleteInteractions.executeUpdate()
    queryDeleteServiceContracts.executeUpdate()

    manager.transaction.commit()
}

try {
    logger.info("UpdateDomainsFromSource: Start")

    def configFileName = "${System.getenv("domdb_config_dir")}/domains-jpa.properties"
    Configuration config = new Configurations().properties(new File(configFileName))
    Properties jpaProperties = ConfigurationConverter.getProperties(config)

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-domains", jpaProperties)
    EntityManager manager = factory.createEntityManager()

    cleanParents(manager)

    Query query = manager.createQuery("select d from Domain d")
    List<Domain> domains = query.getResultList()

    EntityManager manager2 = null

    def apacheVfs = new StandardFileSystemManager()
    apacheVfs.init()

    for (domain in domains) {

        for (dv in domain.versions) {
            logger.info("Scanning domain: ${domain.name} version: ${dv.name}")

            try {
                manager2 = factory.createEntityManager()
                manager2.getTransaction().begin()

                // Need to re-fetch domainVersion to be in the right transaction scope...
                DomainVersion domainVersion = manager2.find(DomainVersion.class, dv.id)

                def readerFactory = new BitbucketReaderFactory(domainVersion, apacheVfs)

                if (domainVersion.interactionsFolder) {
                    def interactionReader = readerFactory.createInteractionReader()
                    def interactionUpdater = new InteractionUpdater({ entity -> manager2.remove(entity) })
                    interactionUpdater.updateInteractions(domainVersion, interactionReader, false)
                }

                if (domainVersion.documentsFolder) {
                    def documentReader = readerFactory.createDocumentReader()
                    def documentUpdater = new DocumentUpdater({ entity -> manager2.remove(entity) })
                    documentUpdater.updateDocuments(domainVersion, documentReader)
                }

                manager2.getTransaction().commit()
            } catch (Exception ex) {
                logger.info("Exception on domain: ${domain.name} version: ${dv.name}: ${ex}")

            } finally {
                manager2.close()
            }
        }
    }

    apacheVfs.close() // Deletes temp files on disk ($TMPDIR/vfs_cache)
    manager.close()
    factory.close()

    logger.info("UpdateDomainsFromSource: Success")
} catch (Exception e) {
    logger.error("Exception in updateDomainsFromSource.groovy", e)
}