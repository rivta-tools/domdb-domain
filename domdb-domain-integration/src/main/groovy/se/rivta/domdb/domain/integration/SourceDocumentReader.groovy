package se.rivta.domdb.domain.integration

import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileType
import se.rivta.domdb.domain.core.DescriptionDocumentType


class SourceDocumentReader {
    private fileObject

    def SourceDocumentReader(fileObject) {
        this.fileObject = fileObject
    }

    SourceDocument[] getSourceDocuments() {
        def documents = new ArrayList<SourceDocument>()

        for (FileObject childObject in fileObject.getChildren()) {
            if (childObject.getType() != FileType.FILE) {
                continue
            }

            def document = new SourceDocument(
                    name: childObject.getName().getBaseName(),
                    lastUpdated: new Date(childObject.getContent().getLastModifiedTime())
            )

            switch (document.name) {
                case ~/^TKB.*(doc|.*docx)$/:
                    document.type = DescriptionDocumentType.TKB
                    break
                case ~/^AB.*(doc|.*docx)$/:
                    document.type = DescriptionDocumentType.AB
                    break
                case ~/^(IS|VIS).*(doc|.*docx)$/:
                    document.type = DescriptionDocumentType.IS
                    break
                default:
                    document.type = DescriptionDocumentType.Unknown
            }

            documents.add(document)
        }

        // Return as immutable array
        return documents.toArray()
    }
}
