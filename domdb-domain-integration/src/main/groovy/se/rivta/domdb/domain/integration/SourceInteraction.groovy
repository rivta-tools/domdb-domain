package se.rivta.domdb.domain.integration

import se.rivta.domdb.domain.core.RivtaProfile


class SourceInteraction {
    String name
    String namespace
    RivtaProfile rivtaProfile
    String fileName
    String folderName
    int major
    int minor
    String description
    Date lastChanged
    SourceServiceContract initiatorContract
    SourceServiceContract responderContract
}
