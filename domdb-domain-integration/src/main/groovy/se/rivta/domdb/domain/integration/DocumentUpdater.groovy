package se.rivta.domdb.domain.integration

import groovy.util.logging.Log
import se.rivta.domdb.domain.core.DescriptionDocument
import se.rivta.domdb.domain.core.DescriptionDocumentType
import se.rivta.domdb.domain.core.DomainVersion


@Log
class DocumentUpdater {
    private Closure dbDeleteFunction

    /**
     * Constructor
     *
     * @param dbDelete closure to delete an object from database
     *          JPA (Script) Example: { entity -> entityManager.remove(entity) }
     *          Hibernate (Grails) example: { entity -> session.delete(entity) }
     */
    def DocumentUpdater(Closure dbDelete) {
        this.dbDeleteFunction = dbDelete
    }

    public void updateDocuments(DomainVersion domainVersion, SourceDocumentReader sourceReader) {
        def existingDocuments = domainVersion.descriptionDocuments.toList()
        // Clear (and later rebuild array
        domainVersion.descriptionDocuments.clear()

        SourceDocument[] sourceDocuments

        try {
            sourceDocuments = sourceReader.getSourceDocuments()
        } catch (Exception ex) {
            log.warning("Exception when retrieving documents: ${ex}")
            return
        }

        sourceDocuments.each { SourceDocument sourceDocument ->
            DescriptionDocument document = existingDocuments.find { it.fileName == sourceDocument.name }

            try {
                if (document) {
                    document.lastChangedDate = sourceDocument.lastUpdated
                    existingDocuments.remove(document)
                    domainVersion.descriptionDocuments.add(document)
                } else if (sourceDocument.type != DescriptionDocumentType.Unknown) {
                    log.info("Adding document: ${sourceDocument.name}")

                    document = new DescriptionDocument(fileName: sourceDocument.name)
                    document.documentType = sourceDocument.type
                    document.lastChangedDate = sourceDocument.lastUpdated
                    document.domainVersion = domainVersion

                    domainVersion.descriptionDocuments.add(document)
                }
            } catch (Exception ex) {
                log.warning("Exception on document ${sourceDocument.name}: ${ex}")
            }
        }

        existingDocuments.each { orphanDocument ->
            dbDeleteFunction(orphanDocument)
        }
    }
}
