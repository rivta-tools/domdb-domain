package se.rivta.domdb.domain.integration

import se.rivta.domdb.domain.core.DescriptionDocumentType


class SourceDocument {
    String name
    Date lastUpdated
    DescriptionDocumentType type
}
