package se.rivta.domdb.domain.integration

import groovy.util.logging.Log
import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileType
import org.apache.commons.vfs2.FileTypeSelector
import se.rivta.domdb.domain.core.RivtaProfile
import se.rivta.domdb.domain.integration.wsdl.Schema
import se.rivta.domdb.domain.integration.wsdl.WsdlContents
import se.rivta.domdb.domain.integration.wsdl.WsdlParser

import java.nio.charset.Charset

@Log
class SourceInteractionReader {
    private interactionsFolder // In runtime this is org.apache.commons.vfs2.FileObject

    // Example file name: GetAlertInformationInteraction_2.0_RIVTABP21.wsdl
    private wsdlFileNameRegex = /^[a-zA-Z0-9]+Interaction_\d\.\d_(?i)rivtabp(20|21).wsdl$/

    def SourceInteractionReader(fileObject) {
        this.interactionsFolder = fileObject
    }

    // Could be replaced by unit tests...
    public WsdlParser wsdlParser = new WsdlParser()


    public SourceInteraction[] getSourceInteractions() {
        def interactions = new ArrayList<SourceInteraction>()

        FileObject[] files = interactionsFolder.findFiles(new FileTypeSelector(FileType.FILE))

        for (FileObject file in files) {
            String fileName = file.getName().getBaseName()

            if (!(fileName ==~ wsdlFileNameRegex)) {
                log.fine("Skipping ${fileName}. Not named like a RIVTA interaction")
                continue
            }

            def wsdlContents = wsdlParser.parse(read(file))
            def interaction = createInteraction(fileName, wsdlContents)

            def parentFolder = file.getParent()

            if (parentFolder != interactionsFolder) {
                interaction.folderName = parentFolder.getName().getBaseName()
            }

            interaction.lastChanged = new Date(parentFolder.getContent().lastModifiedTime)

            Schema[] schemas = wsdlContents.serviceContracts

            schemas.each { schema ->
                def serviceContract = createServiceContract(schema)

                switch (schema.namespace) {
                    case ~/.*Responder:\d.*/:
                        interaction.responderContract = serviceContract
                        break
                    case ~/.*Initiator:\d.*/:
                        interaction.initiatorContract = serviceContract
                        break
                    default:
                        log.warning("Neither initiator or responder contract. Skipping: ${schema.namespace}")
                }
            }

            interactions.add(interaction)
        }

        return interactions
    }

    private String read(FileObject file) {
        BufferedInputStream inputStream = new BufferedInputStream(file.getContent().getInputStream());
        Charset charset = Charset.forName("UTF-8");
        InputStreamReader reader = new InputStreamReader(inputStream, charset);
        StringBuilder stringBuilder = new StringBuilder();

        int bytesRead;
        char[] buffer = new char[1024];

        while ((bytesRead = reader.read(buffer, 0, buffer.length)) != -1) {
            stringBuilder.append(buffer, 0, bytesRead);
        }

        reader.close();
        inputStream.close();
        String content = stringBuilder.toString();
        content
    }

    private SourceInteraction createInteraction(String fileName, WsdlContents wsdlContents) {
        def interaction = new SourceInteraction(fileName: fileName)

        // Example fileName: GetAlertInformationInteraction_2.0_RIVTABP21.wsdl
        String[] parts = interaction.fileName.split("_")
        String[] versionParts = parts[1].split("\\.")
        interaction.major = versionParts[0].toInteger()
        interaction.minor = versionParts[1].toInteger()

        String rivtaProfileString = parts[2].replaceAll(/(?i)(.wSDl)/, "")
        // remove .wsdl suffix with case insensitive regex match
        interaction.rivtaProfile = RivtaProfile.valueOf(rivtaProfileString.toLowerCase())

        interaction.name = wsdlContents.name
        interaction.namespace = wsdlContents.targetNamespace
        interaction.description = wsdlContents.serviceDescription

        return interaction
    }

    private SourceServiceContract createServiceContract(Schema schema) {
        def serviceContract = new SourceServiceContract(namespace: schema.namespace)

        def digitFinder = schema.schemaLocation =~ /\d/
        serviceContract.major = digitFinder[0].toString().toShort()
        serviceContract.minor = digitFinder[1].toString().toShort()

        String[] parts = schema.namespace.split(":")
        serviceContract.name = parts[parts.size() - 2]

        return serviceContract
    }
}
