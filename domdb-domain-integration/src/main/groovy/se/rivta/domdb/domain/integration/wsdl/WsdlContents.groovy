package se.rivta.domdb.domain.integration.wsdl


public class WsdlContents {
    def WsdlContents() {
        serviceContracts = []
    }

    String name
    String serviceDescription
    String targetNamespace
    Schema[] serviceContracts
}
