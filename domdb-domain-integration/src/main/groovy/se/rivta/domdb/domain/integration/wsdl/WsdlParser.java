package se.rivta.domdb.domain.integration.wsdl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WsdlParser {

  private static final String DESCRIPTION_KEYWORD = "Beskrivning:";
  private static final String REVISIONS_KEYWORD = "Revisioner:";

  public WsdlContents parse(String wsdlContent) throws IOException, SAXException, ParserConfigurationException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    DocumentBuilder builder = factory.newDocumentBuilder();

    WsdlContents contents = new WsdlContents();
    Document document = builder.parse(new ByteArrayInputStream(wsdlContent.getBytes(
        StandardCharsets.UTF_8)));

    populateBaseData(document, contents);
    populateDocumentationData(document, contents);
    populateSchemas(document, contents);

    return contents;
  }

  public void populateBaseData(Document document, WsdlContents contents) {
    Node rootNode = document.getElementsByTagName("wsdl:definitions").item(0);

    Node nameAttribute = rootNode.getAttributes().getNamedItem("name");
    if (nameAttribute != null) {
      contents.setName(nameAttribute.getNodeValue());
    }

    Node targetNamespaceAttribute = rootNode.getAttributes().getNamedItem("targetNamespace");
    if (targetNamespaceAttribute != null) {
      contents.setTargetNamespace(targetNamespaceAttribute.getNodeValue());
    }
  }

  public void populateDocumentationData(Document document, WsdlContents contents) {
    NodeList documentationNodes;
    if (document.getElementsByTagName("xs:annotation").item(0) != null) {
      documentationNodes = document.getElementsByTagName("xs:documentation");
    } else {
      documentationNodes = document.getElementsByTagName("wsdl:documentation");
    }

    Element documentationElement = (Element) documentationNodes.item(0);
    String documentation = "";
    if (documentationElement != null) {
      documentation = documentationElement.getTextContent().trim();
    }

    int descriptionStartPos = documentation.indexOf(DESCRIPTION_KEYWORD);
    if (descriptionStartPos < 0) {
      return;
    }

    int descriptionEndPos = descriptionStartPos + DESCRIPTION_KEYWORD.length();
    int revisionsStartPos = documentation.indexOf(REVISIONS_KEYWORD);

    String descriptionParagraph = documentation.substring(descriptionEndPos, revisionsStartPos)
        .trim();
    String description = Arrays.stream(descriptionParagraph.split("\n"))
        .map(String::trim)
        .collect(Collectors.joining(" "));

    contents.setServiceDescription(description);
  }

  public void populateSchemas(Document document, WsdlContents contents) {
    NodeList nodes = document.getElementsByTagName("xs:import");
    List<Schema> results = new ArrayList<>();

    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (!node.getAttributes().getNamedItem("schemaLocation").getNodeValue().startsWith("..")) {
        String schemaLocation = node.getAttributes().getNamedItem("schemaLocation").getNodeValue();
        String namespace = node.getAttributes().getNamedItem("namespace").getNodeValue();

        Schema schema = new Schema();
        schema.setSchemaLocation(schemaLocation);
        schema.setNamespace(namespace);
        results.add(schema);
      }
    }
    contents.setServiceContracts(results.toArray(new Schema[0]));
  }
}
