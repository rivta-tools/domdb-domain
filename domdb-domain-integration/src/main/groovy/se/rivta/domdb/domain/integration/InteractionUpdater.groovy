package se.rivta.domdb.domain.integration

import groovy.util.logging.Log
import se.rivta.domdb.domain.core.Domain
import se.rivta.domdb.domain.core.DomainVersion
import se.rivta.domdb.domain.core.Interaction
import se.rivta.domdb.domain.core.InteractionDescription
import se.rivta.domdb.domain.core.ServiceContract

@Log
class InteractionUpdater {
    private Closure dbDeleteFunction

    /**
     * Constructor
     *
     * @param dbDelete closure to delete an object from database
     *          JPA Example: { entity -> entityManager.remove(entity) }
     *          Hibernate example: { entity -> session.delete(entity) }
     */
    def InteractionUpdater(Closure dbDelete ) {
        this.dbDeleteFunction = dbDelete
    }


    public void updateInteractions(DomainVersion domainVersion, SourceInteractionReader sourceReader, boolean force) {
        SourceInteraction[] sourceInteractions

        try {
            sourceInteractions = sourceReader.getSourceInteractions()
        } catch (Exception ex) {
            log.warning("Exception while retrieving interactions: ${ex}")
            return
        }

        sourceInteractions.each { sourceInteraction ->
            try {
                InteractionDescription description = domainVersion.interactionDescriptions.find { desc ->
                    desc.folderName == sourceInteraction.folderName && desc.wsdlFileName == sourceInteraction.fileName
                }

                if (!force && description != null && description.lastChangedDate >= sourceInteraction.lastChanged) {
                    log.info("Skipping ${description.interaction.name}. Nothing updated in source folder.")
                    return
                }

                // wsdl file could have been renamed...
                if (!description) {
                    description = domainVersion.interactionDescriptions.find {
                        it.interaction?.namespace == sourceInteraction.namespace
                    }
                }

                if (!description) {
                    if (!sourceInteraction.namespace.contains(domainVersion.getDomainName())) {
                        log.warning("Interaction does not belong to the domain:  ${sourceInteraction.namespace}")
                        return
                    }

                    log.info("Adding interaction description: ${sourceInteraction.fileName}")
                    description = new InteractionDescription(domainVersion: domainVersion)
                    domainVersion.interactionDescriptions.add(description)

                    description.interaction = findOrCreateDomainInteraction(sourceInteraction, domainVersion.domain)
                    description.interaction.interactionDescriptions.add(description)
                }

                description.folderName = sourceInteraction.folderName
                description.wsdlFileName = sourceInteraction.fileName

                def interaction = description.interaction

                if (sourceInteraction.initiatorContract) {
                    interaction.initiatorContract = findOrCreateServiceContract(sourceInteraction.initiatorContract, interaction.domain)
                }

                if (sourceInteraction.responderContract) {
                    interaction.responderContract = findOrCreateServiceContract(sourceInteraction.responderContract, interaction.domain)
                }

                description.description = sourceInteraction.description
                description.lastChangedDate = sourceInteraction.lastChanged
            } catch (Exception ex) {
                log.warning("Exception on interaction ${sourceInteraction.name}: ${ex}")
            }

        }

        // Remove non-present interactionDescriptions
        for (int i = domainVersion.interactionDescriptions.size() - 1; i >= 0; i--) {
            def desc = domainVersion.interactionDescriptions[i]

            if (!sourceInteractions.any { it.fileName == desc.wsdlFileName && it.folderName == desc.folderName }) {
                log.info("Removing interaction description: ${desc.getRelativePath()}")

                domainVersion.interactionDescriptions.remove(desc)
                desc.interaction.interactionDescriptions.remove(desc)

                // Invoke provided persistance delete method, e.g. entityManager.remove(obj)
                dbDeleteFunction(desc)
            }

        }
    }

    private Interaction findOrCreateDomainInteraction(SourceInteraction sourceInteraction, Domain domain) {

        Interaction interaction = domain.interactions.find {
            it.namespace == sourceInteraction.namespace &&
                    it.major == sourceInteraction.major &&
                    it.minor == sourceInteraction.minor &&
                    it.rivtaProfile == sourceInteraction.rivtaProfile
        }

        if (interaction == null) {
            log.info("Creating new interaction: ${sourceInteraction.namespace}")

            interaction = new Interaction()
            interaction.name = sourceInteraction.name
            interaction.namespace = sourceInteraction.namespace
            interaction.major = sourceInteraction.major
            interaction.minor = sourceInteraction.minor
            interaction.domain = domain
            interaction.rivtaProfile = sourceInteraction.rivtaProfile

            domain.interactions.add(interaction)
        }

        return interaction
    }

    private ServiceContract findOrCreateServiceContract(SourceServiceContract sourceServiceContract, Domain domain) {
        ServiceContract contract = domain.serviceContracts.find {
            it.namespace.equalsIgnoreCase(sourceServiceContract.namespace) &&
                    it.major == sourceServiceContract.major &&
                    it.minor == sourceServiceContract.minor
        }

        if (contract == null) {
            log.info("Creating new service contract: ${sourceServiceContract.namespace}")
            contract = new ServiceContract()
            contract.namespace = sourceServiceContract.namespace

            contract.name = sourceServiceContract.name
            contract.major = sourceServiceContract.major
            contract.minor = sourceServiceContract.minor
            contract.domain = domain

            domain.serviceContracts.add(contract)
        }

        return contract
    }
}
