package se.rivta.domdb.domain.integration.wsdl

import groovy.xml.XmlSlurper
import groovy.xml.slurpersupport.GPathResult



/**
 * Extracts data from wsdl files
 */
class WsdlParserOld {

    final String beskrivningKeyword = "Beskrivning:"
    final String revisionerKeyword = "Revisioner:"

    public WsdlContents parse(String wsdlContent){
        def rootNode = new XmlSlurper().parseText(wsdlContent)

        return parseInternal(rootNode)
    }

    public WsdlContents parse(InputStream stream) {
        def rootNode = new XmlSlurper().parse(stream)

        return parseInternal(rootNode)
    }

    private WsdlContents parseInternal(GPathResult rootNode) {
        def contents = new WsdlContents()
        contents.name = rootNode.@name
        contents.targetNamespace = rootNode.@targetNamespace

        String documentationText = rootNode.annotation.documentation.text()

        if (!documentationText) {
            documentationText = rootNode.documentation.text()
        }

        if (documentationText) {
            contents.serviceDescription = parseDescriptionFromDocumentation(documentationText)
        }

        contents.serviceContracts = getSchemas(rootNode.types.schema.import)

        return contents
    }

    private String parseDescriptionFromDocumentation(String documentation){
        int beskrivningStartPos = documentation.indexOf(beskrivningKeyword)
        if (beskrivningStartPos < 0){
            return null
        }

        int beskrivningEndPos = beskrivningStartPos + beskrivningKeyword.length()
        int revisionerStartPos = documentation.indexOf(revisionerKeyword)

        String descriptionParagraph = documentation.substring(beskrivningEndPos, revisionerStartPos).trim()

        String[] descriptionParts = descriptionParagraph.split('\n')
        String[] trimmedDescriptionParts = descriptionParts.collect { it.trim() }
        String description = trimmedDescriptionParts.join(' ')

        return description
    }

    private Schema[] getSchemas(nodes){
        def results = []

        nodes.each { node ->
            if (!node.@schemaLocation.toString().startsWith("..")) {
                results.add(new Schema(
                        schemaLocation: node.@schemaLocation.toString(),
                        namespace: node.@namespace.toString())
                )
            }
        }

        return results
    }
}
