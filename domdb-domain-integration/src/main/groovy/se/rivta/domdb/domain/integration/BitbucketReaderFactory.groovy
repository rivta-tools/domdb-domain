package se.rivta.domdb.domain.integration

import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileSystemManager
import org.apache.commons.vfs2.FileSystemOptions
import se.rivta.domdb.domain.core.DomainVersion


/**
 * Creates SourceDocumentReaders and SourceInteractionReaders backed by correct
 * Apache VFS fileObjects
 */
class BitbucketReaderFactory {
    private DomainVersion domainVersion
    private FileSystemManager fileSystemManager
    private FileObject fileObject

    def BitbucketReaderFactory(DomainVersion domainVersion, FileSystemManager fileSystemManager) {
        if (!domainVersion.sourceControlPath) {
            throw new Exception("DomainVersion has no source control path!")
        }

        this.domainVersion = domainVersion
        this.fileSystemManager = fileSystemManager
        this.fileObject = getFileObject()
    }

    public SourceDocumentReader createDocumentReader() {
        if (!domainVersion.documentsFolder) {
            throw new Exception("DomainVersion has no document folder path! Current value: " + domainVersion.documentsFolder)
        }

        def documentFolder = fileObject.resolveFile(domainVersion.documentsFolder)
        def documentReader = new SourceDocumentReader(documentFolder)

        return documentReader
    }

    public SourceInteractionReader createInteractionReader() {
        if (!domainVersion.interactionsFolder) {
            throw new Exception("DomainVersion has no interaction folder path! Current value: " + domainVersion.interactionsFolder)
        }

        def interactionFolder = fileObject.resolveFile(domainVersion.interactionsFolder)
        def interactionReader = new SourceInteractionReader(interactionFolder)

        return interactionReader
    }

    private FileObject getFileObject() {
        def zipUrl = getBitbucketZipUrl(domainVersion.sourceControlPath)

        // See https://commons.apache.org/proper/commons-vfs/filesystems.html for correct URL formats
        // Examples: ftp://[ username[: password]@] hostname[: port][ relative-path]
        //              http://[ username[: password]@] hostname[: port][ absolute-path]
        // Here we use zip and http in combination: zip:http://somehost/downloads/somefile.zip
        def fileObject = fileSystemManager.resolveFile("zip:" + zipUrl, new FileSystemOptions());

        if (!fileObject.exists()) {
            throw new Exception("Zip URL does not exist!")
        }

        // zip begins with a root folder, named for example: rivta-domains-riv.clinicalprocess.healthcond.description-743d865ce840
        // We jump down one level...
        def firstChild = fileObject.getChildren().first()
        return firstChild
    }

    /**
     * Translates the url for a tag into a zip download url
     * @param url   Example: https://bitbucket.org/rivta-domains/riv.clinicalprocess.activity.request/src/master
     * @return      Example: https://bitbucket.org/rivta-domains/riv.clinicalprocess.activity.request/get/master.zip
     */
    private String getBitbucketZipUrl(String url) {
        int pos = url.indexOf('?')

        if (pos > 0) {
            url = url.substring(0, pos)
        }

        if (url.endsWith('/')) {
            url = url.substring(0, url.length() - 1)
        }

        url = url.replace("/src/", "/get/") + ".zip"

        return url
    }
}
