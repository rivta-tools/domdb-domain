package se.rivta.domdb.domain.integration

import org.apache.commons.vfs2.FileContent
import org.apache.commons.vfs2.FileName
import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileType
import se.rivta.domdb.domain.core.DescriptionDocumentType
import se.rivta.domdb.domain.core.RivtaProfile
import se.rivta.domdb.domain.integration.wsdl.Schema
import se.rivta.domdb.domain.integration.wsdl.WsdlContents
import se.rivta.domdb.domain.integration.wsdl.WsdlParser
import spock.lang.Specification

import java.nio.file.Path


class SourceInteractionReaderSpec extends Specification {

    void "Get source interactions: Normal interaction is parsed correctly"() {
        setup:
        def childObjects = [
                createDummyDocument("GetAlertInformationInteraction_2.0_RIVTABP21.wsdl", createDummyFolder("GetAlertInformationInteraction", new Date(2014,07,01)))
        ]

        def fileObject = [findFiles : { return childObjects }]

        def wsdlParser = Mock(WsdlParser)

        def responderSchema = new Schema(
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2",
                schemaLocation: "GetAlertInformationResponder_2.0.xsd")

        def wsdlContents = new WsdlContents(
                name: "GetAlertInformationInteraction",
                targetNamespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                serviceDescription: "Request alert information for a subject of care",
                serviceContracts: [ responderSchema ])

        wsdlParser.parse(_) >> wsdlContents

        def repository = new SourceInteractionReader(fileObject)
        repository.wsdlParser = wsdlParser

        when:
        SourceInteraction[] interactions = repository.getSourceInteractions()

        then:
        interactions.length == 1

        SourceInteraction interaction = interactions.first()
        interaction.namespace == wsdlContents.targetNamespace
        interaction.name == wsdlContents.name
        interaction.description == wsdlContents.serviceDescription
        interaction.fileName == "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl"
        interaction.folderName == "GetAlertInformationInteraction"
        interaction.rivtaProfile == RivtaProfile.rivtabp21
        interaction.lastChanged == new Date(2014,07,01)
        interaction.major == 2
        interaction.minor == 0

        interaction.initiatorContract == null

        interaction.responderContract.name == "GetAlertInformationResponder"
        interaction.responderContract.namespace == responderSchema.namespace
        interaction.responderContract.major == 2
        interaction.responderContract.minor == 0
    }

    void "Get source interactions: Interaction with two service contracts is parsed correctly"() {
        setup:
        def childObjects = [
                createDummyDocument("GetAlertInformationInteraction_2.0_RIVTABP21.wsdl", createDummyFolder("GetAlertInformationInteraction", new Date(2014,07,01)))
        ]

        def fileObject = [findFiles : { return childObjects }]

        def wsdlParser = Mock(WsdlParser)

        def responderSchema = new Schema(
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2",
                schemaLocation: "GetAlertInformationResponder_2.0.xsd")

        def initiatorSchema = new Schema(
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationInitiator:2",
                schemaLocation: "GetAlertInformationInitiator_2.1.xsd")

        def wsdlContents = new WsdlContents(
                name: "GetAlertInformationInteraction",
                targetNamespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                serviceDescription: "Request alert information for a subject of care",
                serviceContracts: [ responderSchema, initiatorSchema ])

        wsdlParser.parse(_) >> wsdlContents

        def repository = new SourceInteractionReader(fileObject)
        repository.wsdlParser = wsdlParser

        when:
        SourceInteraction[] interactions = repository.getSourceInteractions()

        then:
        interactions.length == 1

        SourceInteraction interaction = interactions.first()

        interaction.initiatorContract.name == "GetAlertInformationInitiator"
        interaction.initiatorContract.namespace == initiatorSchema.namespace
        interaction.initiatorContract.major == 2
        interaction.initiatorContract.minor == 1

        interaction.responderContract.name == "GetAlertInformationResponder"
        interaction.responderContract.namespace == responderSchema.namespace
        interaction.responderContract.major == 2
        interaction.responderContract.minor == 0
    }

    void "Additional xml schemas are ignored"() {
        setup:
        def childObjects = [
                createDummyDocument("HamtaApoteksInfoInteraction_1.0_RIVTABP20.wsdl", createDummyFolder("HamtaApoteksInfoInteraction", new Date(2014,07,01)))
        ]

        def fileObject = [findFiles : { return childObjects }]

        def wsdlParser = Mock(WsdlParser)

        def responderSchema = new Schema(
                namespace: "urn:riv:se.apotekensservice:expo:HamtaApoteksInfoResponder:1",
                schemaLocation: "HamtaApoteksInfoResponder_1.0.xsd")

        // Rivta BP 2.0 interactions import the ws-addressing schema. This schema should be ignored.
        def wsAddressingSchema = new Schema(
                namespace: "http://www.w3.org/2005/08/addressing",
                schemaLocation: "../../core_components/ws-addressing-1.0.xsd")

        def wsdlContents = new WsdlContents(
                name: "HamtaApoteksInfoInteraction",
                targetNamespace: "urn:riv:inera:se.apotekensservice:expo:HamtaApoteksInfo:1",
                serviceDescription: "Tjänstekontraktet som avser att konkretisera tillämpningen av RIV.",
                serviceContracts: [ responderSchema, wsAddressingSchema ])

        wsdlParser.parse(_) >> wsdlContents

        def repository = new SourceInteractionReader(fileObject)
        repository.wsdlParser = wsdlParser

        when:
        SourceInteraction[] interactions = repository.getSourceInteractions()

        then:
        interactions.length == 1

        SourceInteraction interaction = interactions.first()
        interaction.rivtaProfile == RivtaProfile.rivtabp20

        interaction.initiatorContract == null

        interaction.responderContract.name == "HamtaApoteksInfoResponder"
        interaction.responderContract.namespace == responderSchema.namespace
        interaction.responderContract.major == 1
        interaction.responderContract.minor == 0
    }

    void "Get source interactions: Non-wsdl files are skipped"() {
        setup:
        def childObjects = [
                createDummyDocument("GetAlertInformationResponder_2.0.xsd", createDummyFolder("GetAlertInformationInteraction", new Date(2014,07,01)))
        ]

        def fileObject = [findFiles : { return childObjects }]

        def repository = new SourceInteractionReader(fileObject)

        when:
        SourceInteraction[] interactions = repository.getSourceInteractions()

        then:
        interactions.length == 0
    }



    void "Get source interactions: wsdl files with wrong name are skipped"() {
        setup:
        def childObjects = [
                createDummyDocument("GetAlertInformation.wsdl", createDummyFolder("GetAlertInformationInteraction", new Date(2014,07,01)))
        ]

        def fileObject = [findFiles : { return childObjects }]

        def repository = new SourceInteractionReader(fileObject)

        when:
        SourceInteraction[] interactions = repository.getSourceInteractions()

        then:
        interactions.length == 0
    }

    /**
     * returns fake FileObjects
     */
    private FileObject createDummyDocument(String name, FileObject parent) {
        return [
                getName: { return [ getBaseName: { name }, getExtension: { name.substring(name.lastIndexOf('.') + 1) } ] as FileName },
                getType: { return FileType.FILE },
                getContent: { return [ getLastModifiedTime: { return date.getTime() }, getInputStream: { return new ByteArrayInputStream(new byte[0]) } ] as FileContent },
                getParent: { return parent }
        ] as FileObject
    }


    private FileObject createDummyFolder(String name, Date lastModified) {
        return [
                getName: { return [ getBaseName: { name } ] as FileName },
                getContent: { return [ getLastModifiedTime: { return lastModified.getTime() } ] as FileContent }
        ] as FileObject
    }
}
