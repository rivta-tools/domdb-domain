package se.rivta.domdb.domain.integration.wsdl

import spock.lang.Specification


class WsdlParserSpec extends Specification {
    void testGetServiceDescription_doesNotExistInWsdl(){
        setup:
        String wsdlContents = createWsdlWithDocumentation("Namn: CancelBookingInteraction")
        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.serviceDescription == null
    }

    void testGetServiceDescription_oneLineDescriptionExistInWsdl(){
        setup:
        String wsdlContents = createWsdlWithDocumentation(
                """Namn: CancelBookingInteraction
                    Beskrivning:
                      Detta är beskrivningen.
                    Revisioner:
                """
        )
        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.serviceDescription == "Detta är beskrivningen."
    }

    void testGetServiceDescription_AnnotationDescriptionExistInWsdl(){
        setup:
        String wsdlContents = createWsdlWithAnnotationDocumentation(
                """Namn: CancelBookingInteraction
                    Beskrivning:
                      Detta är beskrivningen.
                    Revisioner:
                """
        )
        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.serviceDescription == "Detta är beskrivningen."
    }

    void testGetServiceDescription_multiLineDescriptionExistInWsdl(){
        setup:
        String wsdlContents = createWsdlWithDocumentation(
                """Namn: CancelBookingInteraction
                    Beskrivning:
                      Detta är beskrivningen.
                      Den fortsätter här.
                    Revisioner:
                """
        )
        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.serviceDescription == "Detta är beskrivningen. Den fortsätter här."
    }

    void testGetTargetNamespace(){
        setup:
        def targetNamespace = "urn:riv:clinicalprocess:healthcond:actoutcome:GetImagingOutcome:1:rivtabp21"
        String wsdlContents = createWsdlWithTargetNamespace(targetNamespace)
        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.targetNamespace == targetNamespace
    }

    void testGetSchemaImports(){
        setup:
        def namespace = "urn:riv:clinicalprocess:healthcond:actoutcome:GetImagingOutcomeResponder"
        def schemaLocation = "abc.xsd"

        def wsdlContents = """<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <wsdl:definitions name='CancelBookingInteraction' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>
            <wsdl:types>
            <xs:schema>
              <xs:import schemaLocation='${schemaLocation}' namespace='${namespace}'/>" }}
              <xs:import schemaLocation='../../core_components/itintegration_registry_1.0.xsd' namespace='urn:riv:itintegration:registry:1' />
            </xs:schema>
            </wsdl:types>
            </wsdl:definitions>"""

        def parser = new WsdlParser()

        when:
        def contents = parser.parse(wsdlContents)

        then:
        contents.serviceContracts.size() == 1
        contents.serviceContracts.any {
            it.namespace == namespace &&
            it.schemaLocation == schemaLocation
        }
    }

    private String createWsdlWithDocumentation(String documentation){
        return """<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <wsdl:definitions name='CancelBookingInteraction' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>
            <wsdl:documentation>${documentation}</wsdl:documentation>
            </wsdl:definitions>"""
    }

    private String createWsdlWithAnnotationDocumentation(String documentation){
        return """<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <wsdl:definitions name='CancelBookingInteraction' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>
            <xs:annotation><xs:documentation>${documentation}</xs:documentation></xs:annotation>
            </wsdl:definitions>"""
    }

    private String createWsdlWithTargetNamespace(String targetNamespace){
        return """<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <wsdl:definitions name='CancelBookingInteraction' targetNamespace='${targetNamespace}' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>
            </wsdl:definitions>"""
    }

    private String createWsdlWithSchemaImports(List namespaces){
        return """<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
            <wsdl:definitions name='CancelBookingInteraction' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>
            <wsdl:types>
            <xs:schema>
              ${namespaces.collect { return "<xs:import schemaLocation='abc.xsd' namespace='" + it + "'/>" }}
            <xs:import schemaLocation='../../core_components/itintegration_registry_1.0.xsd' namespace='urn:riv:itintegration:registry:1' />
            </xs:schema>
            </wsdl:types>
            </wsdl:definitions>"""
    }
}
