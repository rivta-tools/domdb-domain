package se.rivta.domdb.domain.integration

import se.rivta.domdb.domain.core.DescriptionDocument
import se.rivta.domdb.domain.core.DescriptionDocumentType
import se.rivta.domdb.domain.core.Domain
import se.rivta.domdb.domain.core.DomainVersion
import se.rivta.domdb.domain.core.Interaction
import se.rivta.domdb.domain.core.InteractionDescription
import se.rivta.domdb.domain.core.RivtaProfile
import se.rivta.domdb.domain.core.ServiceContract
import spock.lang.Specification

import javax.persistence.EntityManager


class InteractionUpdaterSpec extends Specification {

    void "new interaction in source is created in domain"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def deleteFunction = Mock(Closure)
        def updater = new InteractionUpdater(deleteFunction)

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")


        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1

        def interactionDescription = domainVersion.interactionDescriptions.first()
        interactionDescription.wsdlFileName == sourceInteraction.fileName
        interactionDescription.folderName == sourceInteraction.folderName
        interactionDescription.domainVersion == domainVersion
        interactionDescription.description == sourceInteraction.description
        interactionDescription.lastChangedDate == sourceInteraction.lastChanged

        def interaction = interactionDescription.interaction
        interaction.name == sourceInteraction.name
        interaction.namespace == sourceInteraction.namespace
        interaction.domain == domain
        interaction.major == sourceInteraction.major
        interaction.minor == sourceInteraction.minor
        interaction.rivtaProfile == sourceInteraction.rivtaProfile
        domain.interactions.contains(interaction)

        interaction.initiatorContract == null

        def serviceContract = interaction.responderContract
        serviceContract.name == sourceInteraction.responderContract.name
        serviceContract.namespace == sourceInteraction.responderContract.namespace
        serviceContract.major == sourceInteraction.responderContract.major
        serviceContract.minor == sourceInteraction.responderContract.minor
        serviceContract.domain == domain
        domain.serviceContracts.contains(serviceContract)

        0 * deleteFunction(_)
    }

    void "existing interaction is updated if lastChanged date is newer"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: new Date(2012, 1,1))
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1

        interactionDescription.wsdlFileName == sourceInteraction.fileName
        interactionDescription.folderName == sourceInteraction.folderName
        interactionDescription.description == sourceInteraction.description
        interactionDescription.domainVersion == domainVersion
        interactionDescription.lastChangedDate == sourceInteraction.lastChanged

        interactionDescription.interaction == interaction
        interaction.domain == domain
        interaction.name == sourceInteraction.name
        interaction.namespace == sourceInteraction.namespace
        interaction.major == sourceInteraction.major
        interaction.minor == sourceInteraction.minor
        interaction.rivtaProfile == sourceInteraction.rivtaProfile

        interaction.initiatorContract == null

        def serviceContract = interaction.responderContract
        serviceContract.name == sourceInteraction.responderContract.name
        serviceContract.namespace == sourceInteraction.responderContract.namespace
        serviceContract.major == sourceInteraction.responderContract.major
        serviceContract.minor == sourceInteraction.responderContract.minor

        0 * entityManager.remove(_)
    }

    void "existing interaction is not updated if lastChanged date is unchanged and force is false"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: sourceInteraction.lastChanged)
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1

        // interactionDescription should not be updated, we use the decription field to check
        // Matches both null and ""
        !interactionDescription.description
    }

    void "existing interaction is updated if lastChanged date is unchanged but force is true"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: sourceInteraction.lastChanged)
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        // last argument is force: true/false
        updater.updateInteractions(domainVersion, reader, true)

        then:
        domainVersion.interactionDescriptions.size() == 1

        // interactionDescription should be updated, we use the decription field to check
        interactionDescription.description == sourceInteraction.description
    }

    void "moved interaction description is found and updated"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformation",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                // Notice: folderName is empty
                folderName: "",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: new Date(2012, 1,1))
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1

        interactionDescription.wsdlFileName == sourceInteraction.fileName
        interactionDescription.folderName == sourceInteraction.folderName

        interactionDescription.interaction == interaction

        0 * entityManager.remove(_)
    }

    void "existing sevice contract is picked up by new interaction"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def serviceContract10 = new ServiceContract(
                        name: "GetAlertInformationResponder", major: 1, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")

        domain.serviceContracts.add(serviceContract10)

        def serviceContract20 = new ServiceContract(
                name: "GetAlertInformationResponder", major: 2, minor: 0,
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")

        domain.serviceContracts.add(serviceContract20)

        def interaction10 = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 1, minor: 0, domain: domain,
                responderContract: serviceContract10
        )
        domain.interactions.add(interaction10)

        def interaction20 = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp20, major: 2, minor: 0, domain: domain,
                responderContract: serviceContract20
        )
        domain.interactions.add(interaction20)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        // new interaction description
        domainVersion.interactionDescriptions.size() == 1

        // one new and two old interactions
        domain.interactions.size() == 3

        // existing service contract
        domain.serviceContracts.size() == 2
        domain.serviceContracts.contains(serviceContract10)
        domain.serviceContracts.contains(serviceContract20)

        0 * entityManager.remove(_)
    }

    void "existing interaction is picked up by new interaction description"() {
        setup:

        def sourceInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ sourceInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1

        def interactionDescription = domainVersion.interactionDescriptions.first()
        interactionDescription.wsdlFileName == sourceInteraction.fileName
        interactionDescription.folderName == sourceInteraction.folderName
        interactionDescription.description == sourceInteraction.description
        interactionDescription.domainVersion == domainVersion
        interactionDescription.lastChangedDate == sourceInteraction.lastChanged

        domain.interactions.size() == 1
        interactionDescription.interaction == interaction

        0 * entityManager.remove(_)
    }

    void "foreign interaction is not added"() {
        setup:

        def localInteraction = new SourceInteraction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                fileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                description: "Request alert information for a subject of care",
                lastChanged: new Date(2014, 03, 14, 13, 02),
                major: 2, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )

        // Some domains include a copy of PingForConfiguration in schemas/interactions
        def foreignInteraction = new SourceInteraction(
                name: "PingForConfigurationInteraction",
                namespace: "urn:riv:itintegration:monitoring:PingForConfiguration:1:rivtabp21",
                fileName: "PingForConfigurationInteraction_1.0_RIVTABP21.wsdl",
                folderName: "PingForConfigurationInteraction",
                description: "A service interaction for monitoring and querying the configuration of service endpoints.",
                lastChanged: new Date(),
                major: 1, minor: 0, rivtaProfile: RivtaProfile.rivtabp21,
                responderContract: new SourceServiceContract(
                        name: "PingForConfigurationResponder", major: 1, minor: 0,
                        namespace: "urn:riv:itintegration:monitoring:PingForConfigurationResponder:1")
        )

        def reader = Mock(SourceInteractionReader)
        reader.getSourceInteractions() >> [ localInteraction, foreignInteraction ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: new Date(2012, 1,1))
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 1
        domain.serviceContracts.size() == 1
        domain.interactions.size() == 1
    }

    void "removed interaction description is deleted from domain version"() {
        setup:
        def reader = Mock(SourceInteractionReader)
        // getSourceInteractions returns nothing
        reader.getSourceInteractions() >> [ ]

        def entityManager = Mock(EntityManager)
        def updater = new InteractionUpdater({ obj -> entityManager.remove(obj)})

        def domain = new Domain(name: "clinicalprocess:healthcond:description")
        def domainVersion = new DomainVersion(domain: domain, interactionsFolder: "schemas/interactions")
        def interaction = new Interaction(
                name: "GetAlertInformationInteraction",
                namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformation:2:rivtabp21",
                rivtaProfile: RivtaProfile.rivtabp21, major: 2, minor: 0, domain: domain,
                responderContract: new ServiceContract(
                        name: "GetAlertInformationResponder", major: 2, minor: 0,
                        namespace: "urn:riv:clinicalprocess:healthcond:description:GetAlertInformationResponder:2")
        )
        domain.interactions.add(interaction)

        def interactionDescription = new InteractionDescription(
                wsdlFileName: "GetAlertInformationInteraction_2.0_RIVTABP21.wsdl",
                folderName: "GetAlertInformationInteraction",
                interaction: interaction,
                domainVersion: domainVersion,
                lastChangedDate: new Date(2012, 1,1))
        domainVersion.interactionDescriptions.add(interactionDescription)

        when:
        updater.updateInteractions(domainVersion, reader, false)

        then:
        domainVersion.interactionDescriptions.size() == 0

        1 * entityManager.remove(interactionDescription)
    }
}
