package se.rivta.domdb.domain.integration

import org.apache.commons.vfs2.FileContent
import org.apache.commons.vfs2.FileName
import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileType
import se.rivta.domdb.domain.core.DescriptionDocumentType
import spock.lang.Specification


class SourceDocumentReaderSpec extends Specification {

    void "Get source documents: TKB documents are parsed correctly"() {
        setup:
        def childObjects = [
                createDummyDocument("TKB_clinicalprocess_healthcond_description.docx", new Date(2014,07,14,12,13), FileType.FILE)
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 1

        documents[0].name == "TKB_clinicalprocess_healthcond_description.docx"
        documents[0].lastUpdated == new Date(2014,07,14,12,13)
        documents[0].type == DescriptionDocumentType.TKB
    }

    void "Get source documents: AB documents are parsed correctly"() {
        setup:
        def childObjects = [
                createDummyDocument("AB_clinicalprocess_healthcond_description.docx", new Date(2014,07,19,14,22), FileType.FILE),
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 1

        documents[0].name == "AB_clinicalprocess_healthcond_description.docx"
        documents[0].lastUpdated == new Date(2014,07,19,14,22)
        documents[0].type == DescriptionDocumentType.AB
    }



    void "Get source documents: IS documents are parsed correctly"() {
        setup:
        def childObjects = [
                createDummyDocument("IS_clinicalprocess_healthcond_description.docx", new Date(2014,07,19,14,22), FileType.FILE),
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 1

        documents[0].name == "IS_clinicalprocess_healthcond_description.docx"
        documents[0].lastUpdated == new Date(2014,07,19,14,22)
        documents[0].type == DescriptionDocumentType.IS
    }

    void "Get source documents: Unknown documents are returned without documentType"() {
        setup:
        def childObjects = [
                createDummyDocument("Bilaga Mappningar_GetDiagnosis.xlsx", new Date(2014,07,19,14,22), FileType.FILE),
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 1

        documents[0].name == "Bilaga Mappningar_GetDiagnosis.xlsx"
        documents[0].lastUpdated == new Date(2014,07,19,14,22)
        documents[0].type == DescriptionDocumentType.Unknown
    }

    void "Get source documents: Directories are ignored"() {
        setup:
        def childObjects = [
                createDummyDocument("subdir", new Date(), FileType.FOLDER),
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 0
    }

    void "Get source documents: Correct number of documents are returned"() {
        setup:
        def childObjects = [
                createDummyDocument("TKB_clinicalprocess_healthcond_description.docx", new Date(2014,07,14,12,13), FileType.FILE),
                createDummyDocument("AB_clinicalprocess_healthcond_description.docx", new Date(2014,07,19,14,22), FileType.FILE),
                createDummyDocument("Bilaga Mappningar_GetDiagnosis.xlsx", new Date(2014,07,19,14,22), FileType.FILE),
                createDummyDocument("subdir", new Date(), FileType.FOLDER)
        ]

        def fileObject = [getChildren : { return childObjects }]

        def repository = new SourceDocumentReader(fileObject)

        when:
        SourceDocument[] documents = repository.getSourceDocuments()

        then:
        documents.length == 3
    }

    /**
     * returns fake FileObjects
     */
    private FileObject createDummyDocument(String name, Date date, FileType fileType) {
        return [
                getName: { return [ getBaseName: { return name } ] as FileName },
                getType: { return fileType },
                getContent: { return [ getLastModifiedTime: { return date.getTime() } ] as FileContent }
        ] as FileObject
    }
}
