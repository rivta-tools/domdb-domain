package se.rivta.domdb.domain.integration

import se.rivta.domdb.domain.core.DescriptionDocument
import se.rivta.domdb.domain.core.DescriptionDocumentType
import se.rivta.domdb.domain.core.DomainVersion
import spock.lang.Specification

import javax.persistence.EntityManager


class DocumentUpdaterSpec extends Specification {

    void "new document in source is created"() {
        setup:

        def sourceDocument1 = new SourceDocument(name: "TKB_crm_scheduling.docx", type: DescriptionDocumentType.TKB, lastUpdated: new Date(2014,7,14,13,15))

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> [ sourceDocument1 ]

        def deleteFunction = Mock(Closure)
        def updater = new DocumentUpdater(deleteFunction)

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")


        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        domainVersion.descriptionDocuments.size() == 1

        def document = domainVersion.descriptionDocuments.first()
        document.fileName == sourceDocument1.name
        document.lastChangedDate == sourceDocument1.lastUpdated
        document.documentType == sourceDocument1.type
        document.domainVersion == domainVersion

        0 * deleteFunction(_)
    }

    void "unknown document in source is not added"() {
        setup:

        def sourceDocument1 = new SourceDocument(name: "Förteckning.docx", type: DescriptionDocumentType.Unknown, lastUpdated: new Date(2014,7,14,13,15))

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> [ sourceDocument1 ]

        def deleteFunction = Mock(Closure)
        def updater = new DocumentUpdater(deleteFunction)

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")


        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        domainVersion.descriptionDocuments.size() == 0

        0 * deleteFunction(_)
    }

    void "existing document is updated"() {
        setup:

        def sourceDocument1 = new SourceDocument(name: "TKB_crm_scheduling.docx", type: DescriptionDocumentType.TKB, lastUpdated: new Date(2014,7,14,13,15))

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> [ sourceDocument1 ]

        def entityManager = Mock(EntityManager)
        def updater = new DocumentUpdater({ obj -> entityManager.remove(obj)})

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")
        def document = new DescriptionDocument(fileName: "TKB_crm_scheduling.docx", documentType: DescriptionDocumentType.TKB, lastChangedDate: new Date(2012, 1,1))
        domainVersion.descriptionDocuments.add(document)

        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        domainVersion.descriptionDocuments.size() == 1

        document.fileName == "TKB_crm_scheduling.docx"
        document.lastChangedDate == sourceDocument1.lastUpdated
        document.documentType == DescriptionDocumentType.TKB

        0 * entityManager.remove(_)
    }

    void "manually set documentType is kept after update"() {
        setup:

        // type is unknown here
        def sourceDocument1 = new SourceDocument(name: "Tjänstekontraktsbeskrivning.docx", type: DescriptionDocumentType.Unknown, lastUpdated: new Date(2014,7,14,13,15))

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> [ sourceDocument1 ]

        def deleteFunction = Mock(Closure)
        def updater = new DocumentUpdater(deleteFunction)

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")
        def document = new DescriptionDocument(fileName: "Tjänstekontraktsbeskrivning.docx", lastChangedDate: new Date(2012, 1,1))
        // documentType could be set manually in adminweb
        document.documentType = DescriptionDocumentType.TKB
        domainVersion.descriptionDocuments.add(document)

        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        // documentType must not be reverted to Unknown
        document.documentType == DescriptionDocumentType.TKB
    }

    void "document is deleted if no longer present in source"() {
        setup:

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> []

        def entityManager = Mock(EntityManager)
        def updater = new DocumentUpdater({ obj -> entityManager.remove(obj)})

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")
        def document = new DescriptionDocument(fileName: "TKB_crm_scheduling.docx", documentType: DescriptionDocumentType.TKB, lastChangedDate: new Date(2012, 1,1))
        domainVersion.descriptionDocuments.add(document)

        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        domainVersion.descriptionDocuments.size() == 0

        1 * entityManager.remove(document)
    }

    void "all together now!"() {
        setup:
        def sourceDocumentTkb = new SourceDocument(name: "TKB_crm_scheduling.docx", type: DescriptionDocumentType.TKB, lastUpdated: new Date(2014,7,14,13,15))
        def sourceDocumentAb = new SourceDocument(name: "AB_crm_scheduling.docx", type: DescriptionDocumentType.AB, lastUpdated: new Date(2014,7,14,13,15))

        def reader = Mock(SourceDocumentReader)
        reader.getSourceDocuments() >> [sourceDocumentTkb, sourceDocumentAb]

        def entityManager = Mock(EntityManager)
        def updater = new DocumentUpdater({ obj -> entityManager.remove(obj)})

        def domainVersion = new DomainVersion(sourceControlPath: "http://source", documentsFolder: "docs")

        domainVersion.descriptionDocuments.add(
                new DescriptionDocument(fileName: "TKB_crm_scheduling.docx", documentType: DescriptionDocumentType.TKB, lastChangedDate: new Date(2012, 1,1)))

        domainVersion.descriptionDocuments.add(
                new DescriptionDocument(fileName: "IS_crm_scheduling.docx", documentType: DescriptionDocumentType.IS, lastChangedDate: new Date(2012, 1,1)))

        when:
        updater.updateDocuments(domainVersion, reader)

        then:
        domainVersion.descriptionDocuments.size() == 2

        def documentTkb = domainVersion.descriptionDocuments.find { it.fileName == sourceDocumentTkb.name }
        documentTkb.lastChangedDate == sourceDocumentTkb.lastUpdated
        documentTkb.documentType == DescriptionDocumentType.TKB

        def documentAb = domainVersion.descriptionDocuments.find { it.fileName == sourceDocumentAb.name }
        documentAb.lastChangedDate == sourceDocumentAb.lastUpdated
        documentAb.documentType == DescriptionDocumentType.AB

        1 * entityManager.remove({ it.fileName == "IS_crm_scheduling.docx"})
    }
}
